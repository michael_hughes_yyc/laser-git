<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Laser Equation - Industrial Cutting Solutions</title>
<link href="includes/styles.css" rel="stylesheet" type="text/css" />
<meta name="description" content="Laser Equation Ltd. provides material cutting services for a variety of industries." />
<meta name="keywords" content="Laser cutting, precision cutting, carbon steel, stainless steel, wood, plastics, water jet, waterjet, speed cutting, clean cut edges" />
<meta name="robots" content="index, follow" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script type="text/javascript">
function unewWindow(url) {
	if (arguments.length > 1) {
		var width = String(arguments[1]);
		var height = String(arguments[2]);
		if ((loc = width.indexOf("%")) != -1) {
			width = width.slice(0, loc);
			width = Number(width);
			width = screen.width * (width / 100);
		}
		if ((loc = height.indexOf("%")) != -1) {
			height = height.slice(0, loc);
			height = Number(height);
			height = screen.height * (height / 100);
		}
	} else {
		var width = 640;
		var height = 480;
	}
	var attributeString = 'height=' + height + ',width=' + width + ',menubar=yes,scrollbars=yes,resizable=yes,screenX=10,screenY=10,top=10,left=10';
	//alert(attributeString);
	window.open(url, 'NewWin', attributeString);
}

</script>
</head>

<body>
<div id="header">

<ul>
		<li><a href="#" class="active">Home</a></li>
		<li><a href="about-us.asp">About Us</a></li>
		<li><a href="laser-cutting.asp">Laser Cutting</a></li>
		<li><a href="water-cutting.asp">Water Cutting</a></li>
		<li><a href="gallery.asp">Gallery</a></li>
		<li><a href="contact-us.asp">Contact Us</a></li>
	</ul>

<!-- <p id="layoutdims">Measure columns in: <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-pixels.htm">Pixel widths</a> | <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-ems.htm">Em widths</a> | <strong>Percentage widths</strong></p> -->

</div>
<div class="colmask fullpage">
	<div class="col1">
	
	<p>Laser Equation Ltd. provides material cutting services for a variety of industries. We're sure we will be able to work with you to make your product in a more cost effective manner. </p>

<p>Check out our laser and watercutting options. Or, if you have any questions, simply contact us by phone or email. </p>

<p>Laser Equation Ltd is proud to be <a href="pdf/LASEQ9-2010-Oct-27-RA1Cer.pdf"  onclick="unewWindow('pdf/LASEQ9-2010-Oct-27-RA1Cer.pdf', 700, 500); return false;" >ISO 9001:2008 certified</a>.</p>
</div>
</div>
<div id="footer">

<p>For a custom quote contact us at 403.250.2603 or toll free 1.888.534.1141</p>
</div>

<!--#include file="includes/logo.asp"-->
</body>
</html>
