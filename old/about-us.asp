<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Laser Equation - Industrial Cutting Solutions</title>
<link href="includes/styles.css" rel="stylesheet" type="text/css" />
<meta name="description" content="Laser Equation Ltd. provides material cutting services for a variety of industries." />
<meta name="keywords" content="Laser cutting, precision cutting, carbon steel, stainless steel, wood, plastics, water jet, waterjet, speed cutting, clean cut edges" />
<meta name="robots" content="index, follow" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>

   <body>
<div id="header">

<ul>
		<li><a href="index.asp" >Home</a></li>
		<li><a href="#" class="active">About Us</a></li>
		<li><a href="laser-cutting.asp">Laser Cutting</a></li>
		<li><a href="water-cutting.asp">Water Cutting</a></li>
        <li><a href="gallery.asp" >Gallery</a></li>
		<li><a href="contact-us.asp">Contact Us</a></li>
	</ul>

<!-- <p id="layoutdims">Measure columns in: <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-pixels.htm">Pixel widths</a> | <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-ems.htm">Em widths</a> | <strong>Percentage widths</strong></p> -->

</div>
<div class="colmask fullpage">
	<div class="col1">
	
	<p>LEL is a premier company with highly qualified personnel, doing quality industrial cutting, utilizing industrial lasers and waterjets. LEL does strictly laser and waterjet cutting on a contract basis, in a timely manner. </p>

<p>The company owns large industrial lasers and waterjets. The lasers have cutting areas of 5'x 10' while the waterjets have cutting areas of 6' by 10'. Larger pieces can be cut.  </p>
</div>
</div>
<div id="footer">

<p>For a custom quote contact us at 403.250.2603 or toll free 1.888.534.1141</p>
</div>
<!--#include file="includes/logo.asp"-->
</body>
</html>
