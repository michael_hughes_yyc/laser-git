<!-- Start Pharmacy List -->
<%
if (request.form("pharmacyadd") <> "Add") and (request.form("pharmacysearch") = "Search" or request.form("pharmacy") <> "") then

   search_id = request.form("pharmacy")
   delflag_id = 0
   MySQL = "SELECT * FROM pharmacy WHERE ((pharmacyname like '" & search_id & "%' or pharmacycity like '%" & search_id & "%' or pharmacystate like '%" & search_id & "%') and (delflag = " & delflag_id & ")) ORDER BY pharmacyname"
   ''response.write MySQL
   ''delflag: 0-Active, 1-Deleted
   field_acctno   = true
   field_name     = true
   field_address  = true
   field_location = true
   field_phone    = true
   field_fax      = true

   intPharmacyID = 0
   Set MyRS = Server.CreateObject("ADODB.Recordset") 
   MyRS.Open MySQL, MyConn

   if MyRS.EOF then
     response.write "No Matches"
     response.end
   end if
  
   response.write "<table class='pharmacy'>" & vbCrLf
   response.write "  <tr>" & vbCrLf
   if field_acctno   then response.write "    <td class='smaller'><b>AcctNo</b></td>" & vbCrLf
   if field_name     then response.write "    <td width='100'><b>Name</b></td>" & vbCrLf
   if field_address  then response.write "    <td width='100'><b>Address</b></td>" & vbCrLf
   if field_location then response.write "    <td width='100'><b>Location</b></td>" & vbCrLf
   if field_phone    then response.write "    <td width='100'><b>Phone</b></td>" & vbCrLf
   if field_fax      then response.write "    <td width='100'><b>Fax</b></td>" & vbCrLf
     response.write "  </tr>" & vbCrLf

   Do While Not MyRS.EOF

     ''Start Pharmacy List Database Mapping
     pharmacy_id            = MyRS("pmcid")
     pharmacy_name          = MyRS("pharmacyname")
     pharmacy_address       = MyRS("pharmacyaddress")
     pharmacy_city          = MyRS("pharmacycity")
     pharmacy_state         = MyRS("pharmacystate")
     pharmacy_postalcode    = MyRS("pharmacyzip")
     pharmacy_phonenumber   = MyRS("pharmacyphone")
     pharmacy_phonefax      = MyRS("pharmacyfax")
     ''End Pharmacy List Database Mapping

     intPharmacyID = intPharmacyID + 1
     response.write "  <tr>" & vbCrLf
     if field_acctno   then response.write "    <td  class='smaller'><a href='pharmacy.asp?id=" & pharmacy_id & "' class='textlinks'>" & right("00000" & pharmacy_id, 5) & "</a></td>" & vbCrLf
     if field_name     then response.write "    <td>" & pharmacy_name & "</td>"  & vbCrLf
     if field_address  then response.write "    <td>" & pharmacy_address &"</td>" & vbCrLf
     if field_location then
       if (len(pharmacy_city) > 1) and (len(pharmacy_state) > 1) then
         response.write "    <td>" & pharmacy_city & ", " & pharmacy_state & "</td>" & vbCrLf
       else
         response.write "    <td> </td>"
       end if
     end if
     if field_phone    then response.write "    <td>" & pharmacy_phonenumber & "</td>" & vbCrLf
     if field_fax      then response.write "    <td>" & pharmacy_phonefax &  "</td>" & vbCrLf
   
     response.write "  </tr>" & vbCrLf

   MyRS.MoveNext 
   Loop 

   response.write "  <tr>" & vbCrLf
   response.write "    <td colspan='2'>" & right("00000" & intPharmacyID, 5) & " Pharmacies Found</td>" & vbCrLf
   response.write "  </tr>" & vbCrLf

   response.write "</table>" & vbCrLf

   if intPharmacyID < 1 then
     intPharmacyID = 1
   else
     intPharmacyID = intPharmacyID + 1
   end if

   ''response.write "Current Application #: " & intPharmacyID

   MyRS.Close
   set MyRS = Nothing
end if
%>
<!-- End Pharmacy List -->