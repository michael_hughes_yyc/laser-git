<!--#include file="func_activity.inc"-->

<%
''response.write request.form & "<br /><br />"
''response.end

Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

client_id                = request.form("client_id")
client_lastname          = request.form("client_lastname")
client_firstname         = request.form("client_firstname")
client_middleinitial     = request.form("client_middleinitial")
client_physicianNameRend = request.form("client_physicianNameRend")
client_placeofservice    = request.form("client_placeofservice")
client_physicianNameRef  = request.form("client_physicianNameRef")
client_insuranceName     = request.form("client_insuranceName")
client_varDate           = request.form("client_varDate")
client_preop_diag        = replace(request.form("client_preop_diag"),"%0D","<br />")
client_postop_diag       = replace(request.form("client_postop_diag"),"%0D","<br />")
client_operation         = replace(request.form("client_operation"),"%0D","<br />")
client_anesthesia        = replace(request.form("client_anesthesia"),"%0D","<br />")
client_indications       = replace(request.form("client_indications"),"%0D","<br />")
client_procedure         = replace(request.form("client_procedure"),"%0D","<br />")
client_specimens         = replace(request.form("client_specimens"),"%0D","<br />")
client_complications     = replace(request.form("client_complications"),"%0D","<br />")
client_findings          = replace(request.form("client_findings"),"%0D","<br />")
client_notes             = replace(request.form("client_notes"),"%0D","<br />")
client_templatename      = request.form("client_templatename")

test = mid(request.form,instr(request.form,"&client_procedure"),(instr(request.form,"&client_specimens") - instr(request.form,"&client_procedure")))
test = replace (test,"%0D%0A","<br />")
test = replace (test,"+"," ")
test = replace (test,"&client_procedure=","")
test = replace (test,"%25","%")
test = replace (test,"%27","'")
test = replace (test,"%2C",",")
test = replace (test,"%3F","?")
client_procedure = test


template = ""
'' Start Patient Basics Template -->
template = template + "<font face='arial'>" & vbCrLF
template = template + "<table width='100%' border='0'>" & vbCrLF
template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><strong>Patient Report</strong></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF
template = template + "  <tr>" & vbCrLF
template = template + "    <td width='30%'><font size='3'>Patient: " + client_lastname + ", " + client_firstname + "&nbsp;" + client_middleinitial + "</td>" & vbCrLF
template = template + "  </tr>" & vbCrLF
template = template + "  <tr>" & vbCrLF
template = template + "    <td width='30%'><font size='3'>Provider: " + client_physicianNameRend + "</td>" & vbCrLF
template = template + "  </tr>" & vbCrLF
template = template + "  <tr>" & vbCrLF
template = template + "    <td width='30%'><font size='3'>Primary Insurance: " + client_insuranceName + "</td>" & vbCrLF
template = template + "    <td width='5%'> </td>" & vbCrLF
template = template + "    <td width='35%'><font size='3'>Encounter Date: " + client_varDate & vbCrLF
template = template + "  </tr>" & vbCrLF
template = template + "  <tr>" & vbCrLF
template = template + "    <td width='30%'><font size='3'>Place of Service: Carson Douglas Pain Care</td>" & vbCrLF
template = template + "  </tr>" & vbCrLF
template = template + "  <tr>" & vbCrLF
template = template + "    <td width='30%'><font size='3'>Surgeon: " + client_physicianNameRef + "</td>" & vbCrLF
template = template + "    <td width='5%'> </td>" & vbCrLF
template = template + "    <td width='30%'><font size='3'>Assistant: </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF
template = template + "</table>" & vbCrLF
templatebasics = template
'' End Patient Basics Template

template = ""
template = template + "<table width='100%' border='0'>" & vbCrLF
template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><hr></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><center><h2>OPERATIVE REPORT</h2></center></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Pre-op. Diagnosis:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'><li></li></td>" & vbCrLF
template = template + "    <td>" + client_preop_diag & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Post-op. Diagnosis:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'><li></li></td>" & vbCrLF
template = template + "    <td>" + client_postop_diag & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Operation:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'><li></li></td>" & vbCrLF
template = template + "    <td>" + client_operation & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Anesthesia:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'></td>" & vbCrLF
template = template + "    <td>" + client_anesthesia & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Indications:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'></td>" & vbCrLF
template = template + "    <td>" + client_indications & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Details of Procedure:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'></td>" & vbCrLF
template = template + "    <td>" + client_procedure & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Specimens:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'></td>" & vbCrLF
template = template + "    <td>" + client_specimens & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Complications:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'></td>" & vbCrLF
template = template + "    <td>" + client_complications & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Findings:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'></td>" & vbCrLF
template = template + "    <td>" + client_findings & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "  <tr>" & vbCrLF
template = template + "    <td colspan='3'><b>Notes:</b></td>" & vbCrLF
template = template + "  </tr>" & vbCrLF 
template = template + "  <tr>" & vbCrLF    
template = template + "    <td width='3'></td>" & vbCrLF
template = template + "    <td width='2'></td>" & vbCrLF
template = template + "    <td>" + client_notes & vbCrLF
template = template + "    </td>" & vbCrLF
template = template + "  </tr>" & vbCrLF

template = template + "</table>" & vbCrLF
template = template + "" & vbCrLF
templatereport = template

client_randomID = RandomID()
fileName = client_id & "_" & client_randomID & "_" & client_templatename & ".htm"
filename_full = Server.MapPath("mobiledoc/" & filename)
''response.write filename_full & "<br />"
''response.write templatebasics & templatereport
''response.end

dim fs,fname
set fs=Server.CreateObject("Scripting.FileSystemObject")
set order=fs.OpenTextFile(Server.MapPath("mobiledoc/" & filename), 2, true)
' 8 = Append / 2 = Rewrite
with order
	.write templatebasics + templatereport
end with
order.Close

''Template Add Logging
description = "Template add - client #" & client_id & ": File: " & fileName
test = activity(session("username"),description)
description = ""

%>

<!--#include file="client_upload_template_database_addition.inc"-->
