<!--#include File = "CalendarInclude.asp"-->
<!--#include file="db.asp"-->

<%
' Prevent Caching
Response.Expires = 0
Response.ExpiresAbsolute = now - 1
Response.CacheControl = "no-cache"
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_String

response.write "<head><title>Bulk Events Schedule</title>"
response.write "<script language=javascript>"
  Response.Write "function AddEvents()"
  Response.Write "{"
  Response.Write "	myRegExp = new RegExp(""\n"", ""g"");"
  Response.Write "	mcalNotes = document.ThisForm.scheduleNotes.value.replace(""&"",""and"");"
  Response.Write "	mcalNotes = mcalNotes.replace(myRegExp,""<br />"");"
  Response.Write "window.showModalDialog('ScheduleInsertEventBulk.asp?"
  ''Response.Write "window.showModalDialog('test.asp?"
  Response.Write "scheduleMonth_start=' + document.ThisForm.scheduleMonth_start.value + "
  Response.Write "'&scheduleDay_start=' + document.ThisForm.scheduleDay_start.value + "
  Response.Write "'&scheduleYear_start=' + document.ThisForm.scheduleYear_start.value + "
  Response.Write "'&scheduleMonth_end=' + document.ThisForm.scheduleMonth_end.value + "
  Response.Write "'&scheduleDay_end=' + document.ThisForm.scheduleDay_end.value + "
  Response.Write "'&scheduleYear_end=' + document.ThisForm.scheduleYear_end.value + "

  Response.Write "'&SunStartTime=' + document.ThisForm.SunStartTime.value + "
  Response.Write "'&SunEndTime=' + document.ThisForm.SunEndTime.value + "
  Response.Write "'&MonStartTime=' + document.ThisForm.MonStartTime.value + "
  Response.Write "'&MonEndTime=' + document.ThisForm.MonEndTime.value + "
  Response.Write "'&TueStartTime=' + document.ThisForm.TueStartTime.value + "
  Response.Write "'&TueEndTime=' + document.ThisForm.TueEndTime.value + "
  Response.Write "'&WedStartTime=' + document.ThisForm.WedStartTime.value + "
  Response.Write "'&WedEndTime=' + document.ThisForm.WedEndTime.value + "
  Response.Write "'&ThuStartTime=' + document.ThisForm.ThuStartTime.value + "
  Response.Write "'&ThuEndTime=' + document.ThisForm.ThuEndTime.value + "
  Response.Write "'&FriStartTime=' + document.ThisForm.FriStartTime.value + "
  Response.Write "'&FriEndTime=' + document.ThisForm.FriEndTime.value + "
  Response.Write "'&SatStartTime=' + document.ThisForm.SatStartTime.value + "
  Response.Write "'&SatEndTime=' + document.ThisForm.SatEndTime.value + "

  Response.Write "'&scheduleEmployeeID=' + document.ThisForm.scheduleEmployeeID.value + "
  Response.Write "'&scheduleClientID=' + document.ThisForm.scheduleClientID.value + "
  Response.Write "'&scheduleNotes=' + mcalNotes,"
  Response.Write "'Updating', 'scroll: no help: no; status: no; dialogWidth: 225px; dialogHeight: 200px;');"
  Response.Write "	window.close();"
  Response.Write "}"

  Response.Write "function DeleteEvents()"
  Response.Write "{"
  Response.Write "	myRegExp = new RegExp(""\n"", ""g"");"
  Response.Write "	mcalNotes = document.ThisForm.scheduleNotes.value.replace(""&"",""and"");"
  Response.Write "	mcalNotes = mcalNotes.replace(myRegExp,""<br />"");"
  Response.Write "window.showModalDialog('ScheduleDeleteEventBulk.asp?"
  ''Response.Write "window.showModalDialog('test.asp?"
  Response.Write "scheduleMonth_start=' + document.ThisForm.scheduleMonth_start.value + "
  Response.Write "'&scheduleDay_start=' + document.ThisForm.scheduleDay_start.value + "
  Response.Write "'&scheduleYear_start=' + document.ThisForm.scheduleYear_start.value + "
  Response.Write "'&scheduleMonth_end=' + document.ThisForm.scheduleMonth_end.value + "
  Response.Write "'&scheduleDay_end=' + document.ThisForm.scheduleDay_end.value + "
  Response.Write "'&scheduleYear_end=' + document.ThisForm.scheduleYear_end.value + "

  Response.Write "'&SunStartTime=' + document.ThisForm.SunStartTime.value + "
  Response.Write "'&SunEndTime=' + document.ThisForm.SunEndTime.value + "
  Response.Write "'&MonStartTime=' + document.ThisForm.MonStartTime.value + "
  Response.Write "'&MonEndTime=' + document.ThisForm.MonEndTime.value + "
  Response.Write "'&TueStartTime=' + document.ThisForm.TueStartTime.value + "
  Response.Write "'&TueEndTime=' + document.ThisForm.TueEndTime.value + "
  Response.Write "'&WedStartTime=' + document.ThisForm.WedStartTime.value + "
  Response.Write "'&WedEndTime=' + document.ThisForm.WedEndTime.value + "
  Response.Write "'&ThuStartTime=' + document.ThisForm.ThuStartTime.value + "
  Response.Write "'&ThuEndTime=' + document.ThisForm.ThuEndTime.value + "
  Response.Write "'&FriStartTime=' + document.ThisForm.FriStartTime.value + "
  Response.Write "'&FriEndTime=' + document.ThisForm.FriEndTime.value + "
  Response.Write "'&SatStartTime=' + document.ThisForm.SatStartTime.value + "
  Response.Write "'&SatEndTime=' + document.ThisForm.SatEndTime.value + "

  Response.Write "'&scheduleEmployeeID=' + document.ThisForm.scheduleEmployeeID.value + "
  Response.Write "'&scheduleClientID=' + document.ThisForm.scheduleClientID.value + "
  Response.Write "'&scheduleNotes=' + mcalNotes,"
  Response.Write "'Updating', 'scroll: no help: no; status: no; dialogWidth: 225px; dialogHeight: 200px;');"
  Response.Write "	window.close();"
  Response.Write "}"
response.write "</script>"
response.write "</head>"

response.write "<body>"
mMonth = Request.Item("calMonth")
if mMonth = "" then mMonth = Month(date)
mDay = Request.Item("calDay")
if mDay = "" then mDay = Day(date)
mYear = Request.Item("calYear")
if mYear = "" then mYear = Year(date)
response.write "<form ID=ThisForm Name=ThisForm >"
response.write "<input type=hidden name=calDay value=" & mDay & ">"
response.write "<input type=hidden name=calMonth value=" & mMonth & ">"
response.write "<input type=hidden name=calYear value=" & mYear & ">"
response.write "<table width='100%' height='100%' border='1' bgcolor='lightgrey'>"
response.write "<tr><td height='1%'>"
response.write "<table width='100%' border='0'>"
response.write "<tr><td><b>Bulk Scheduling</b></td><tr>"
response.write "<tr>"

response.write "<td colspan='8'>"
response.write "<table border='0'>"
response.write "<tr>"
response.write "<td>&nbsp;</td>"
response.write "<td>SUN</td>"
response.write "<td>MON</td>"
response.write "<td>TUE</td>"
response.write "<td>WED</td>"
response.write "<td>THU</td>"
response.write "<td>FRI</td>"
response.write "<td>SAT</td>"
response.write "</tr>"
response.write "<tr>"
response.write "<td><b>Start<br />End</b></td>"

'SUN
response.write "<td>"
response.write "<Select name=SunStartTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select><br />"
response.write "<select name=SunEndTime>"
response.write Replace(mDateList, "", "")
response.write "</select>"
response.write "</td>"
'MON
response.write "<td>"
response.write "<Select name=MonStartTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select><br />"
response.write "<select name=MonEndTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select>"
response.write "</td>"
'TUE
response.write "<td>"
response.write "<Select name=TueStartTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select><br />"
response.write "<select name=TueEndTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select>"
response.write "</td>"
'WED
response.write "<td>"
response.write "<Select name=WedStartTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select><br />"
response.write "<select name=WedEndTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select>"
response.write "</td>"
'THU
response.write "<td>"
response.write "<Select name=ThuStartTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select><br />"
response.write "<select name=ThuEndTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select>"
response.write "</td>"
'FRI
response.write "<td>"
response.write "<Select name=FriStartTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select><br />"
response.write "<select name=FriEndTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select>"
response.write "</td>"
'SAT
response.write "<td>"
response.write "<Select name=SatStartTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select><br />"
response.write "<select name=SatEndTime>"
response.write Replace(mDateList, "...", "...")
response.write "</select>"
response.write "</td>"

response.write "</tr>"
response.write "</table>"
''response.write "</td>"
''response.write "</tr>"
''response.write "<tr>"

'START DATE
''response.write "<td colspan='8'>"
response.write "<table border='0'>"
response.write "<tr>"
response.write "<td nowrap><b>Start Date</b></td>"
response.write "<td align='right' width='100%'>"
response.write "<select name='scheduleMonth_start'>"
response.write "  <option value='01'>01 - January</option>"
response.write "  <option value='02'>02 - February</option>"
response.write "  <option value='03'>03 - March</option>"
response.write "  <option value='04'>04 - April</option>"
response.write "  <option value='05'>05 - May</option>"
response.write "  <option value='06'>06 - June</option>"
response.write "  <option value='07'>07 - July</option>"
response.write "  <option value='08'>08 - August</option>"
response.write "  <option value='09'>09 - September</option>"
response.write "  <option value='10'>10 - October</option>"
response.write "  <option value='11'>11 - November</option>"
response.write "  <option value='12'>12 - December</option>"
response.write "</select>"
response.write "<select name='scheduleDay_start'>"
response.write "  <option value='01'>01</option>"
response.write "  <option value='02'>02</option>"
response.write "  <option value='03'>03</option>"
response.write "  <option value='04'>04</option>"
response.write "  <option value='05'>05</option>"
response.write "  <option value='06'>06</option>"
response.write "  <option value='07'>07</option>"
response.write "  <option value='08'>08</option>"
response.write "  <option value='09'>09</option>"
response.write "  <option value='10'>10</option>"
response.write "  <option value='11'>11</option>"
response.write "  <option value='12'>12</option>"
response.write "  <option value='13'>13</option>"
response.write "  <option value='14'>14</option>"
response.write "  <option value='15'>15</option>"
response.write "  <option value='16'>16</option>"
response.write "  <option value='17'>17</option>"
response.write "  <option value='18'>18</option>"
response.write "  <option value='19'>19</option>"
response.write "  <option value='20'>20</option>"
response.write "  <option value='21'>21</option>"
response.write "  <option value='22'>22</option>"
response.write "  <option value='23'>23</option>"
response.write "  <option value='24'>24</option>"
response.write "  <option value='25'>25</option>"
response.write "  <option value='26'>26</option>"
response.write "  <option value='27'>27</option>"
response.write "  <option value='28'>28</option>"
response.write "  <option value='29'>29</option>"
response.write "  <option value='30'>30</option>"
response.write "  <option value='31'>31</option>"
response.write "</select>"
response.write "<select name='scheduleYear_start'>"
response.write "  <option value='2007'>2007</option>"
response.write "  <option value='2008'>2008</option>"
response.write "  <option value='2009'>2009</option>"
response.write "  <option value='2010'>2010</option>"
response.write "</select>"
response.write "</td>"
response.write "</tr>" 

'END DATE
response.write "<td nowrap><b>End Date</b></td>"
response.write "<td align='right'>"
response.write "<select name='scheduleMonth_end'>"
response.write "  <option value='01'>01 - January</option>"
response.write "  <option value='02'>02 - February</option>"
response.write "  <option value='03'>03 - March</option>"
response.write "  <option value='04'>04 - April</option>"
response.write "  <option value='05'>05 - May</option>"
response.write "  <option value='06'>06 - June</option>"
response.write "  <option value='07'>07 - July</option>"
response.write "  <option value='08'>08 - August</option>"
response.write "  <option value='09'>09 - September</option>"
response.write "  <option value='10'>10 - October</option>"
response.write "  <option value='11'>11 - November</option>"
response.write "  <option value='12'>12 - December</option>"
response.write "</select>"
response.write "<select name='scheduleDay_end'>"
response.write "  <option value='01'>01</option>"
response.write "  <option value='02'>02</option>"
response.write "  <option value='03'>03</option>"
response.write "  <option value='04'>04</option>"
response.write "  <option value='05'>05</option>"
response.write "  <option value='06'>06</option>"
response.write "  <option value='07'>07</option>"
response.write "  <option value='08'>08</option>"
response.write "  <option value='09'>09</option>"
response.write "  <option value='10'>10</option>"
response.write "  <option value='11'>11</option>"
response.write "  <option value='12'>12</option>"
response.write "  <option value='13'>13</option>"
response.write "  <option value='14'>14</option>"
response.write "  <option value='15'>15</option>"
response.write "  <option value='16'>16</option>"
response.write "  <option value='17'>17</option>"
response.write "  <option value='18'>18</option>"
response.write "  <option value='19'>19</option>"
response.write "  <option value='20'>20</option>"
response.write "  <option value='21'>21</option>"
response.write "  <option value='22'>22</option>"
response.write "  <option value='23'>23</option>"
response.write "  <option value='24'>24</option>"
response.write "  <option value='25'>25</option>"
response.write "  <option value='26'>26</option>"
response.write "  <option value='27'>27</option>"
response.write "  <option value='28'>28</option>"
response.write "  <option value='29'>29</option>"
response.write "  <option value='30'>30</option>"
response.write "  <option value='31'>31</option>"
response.write "</select>"
response.write "<select name='scheduleYear_end'>"
response.write "  <option value='2007'>2007</option>"
response.write "  <option value='2008'>2008</option>"
response.write "  <option value='2009'>2009</option>"
response.write "  <option value='2010'>2010</option>"
response.write "</select>"
response.write "</td>"
response.write "</tr>"
response.write "</table>"
''response.write "</td>"
''response.write "</tr>"
''response.write "<tr>"

response.write "<table border='0'>"
response.write "<tr>"
response.write "<td><b>Caregiver:</b></td>"
response.write "<td colspan='2'><select name='scheduleEmployeeID'>" & vbCrLF
''response.write "<option value=''>...</option>"  & vbCrLF
MySQL = "SELECT * FROM employees ORDER BY employee_name"
Set MyRS = Server.CreateObject("ADODB.Recordset") 
MyRS.Open MySQL, MyConn
response.write MySQL
if Not MyRS.EOF then
  Do While Not MyRS.EOF
    response.write  "<option value='" & MyRS("employee_ID") & "'>" & MyRS("employee_name") & "</option>" & vbCrLF
    MyRS.MoveNext 
  Loop
end if
MyRS.close
set MyRS = Nothing 
response.write "</select>"
response.write "</td>"

response.write "<tr><td><b>Client:</b></td>"
response.write "<td><select name='scheduleClientID'>" & vbCrLF
''response.write "<option value=''>...</option>"  & vbCrLF
MySQL = "SELECT * FROM clients ORDER BY client_name"
Set MyRS = Server.CreateObject("ADODB.Recordset") 
MyRS.Open MySQL, MyConn
response.write MySQL
if Not MyRS.EOF then
  Do While Not MyRS.EOF
    response.write  "<option value='" & MyRS("clientID") & "'>" & MyRS("client_name") & "</option>" & vbCrLF
    MyRS.MoveNext 
  Loop
end if
MyRS.close
set MyRS = Nothing
response.write "</select>"
response.write "</td>"
response.write "</tr>"
response.write "<tr><td colspan=3 align=left><b>Notes:</b><br />"
response.write "<textarea name=scheduleNotes cols=74 rows=5></textarea></td></tr>"
response.write "</table></td></tr>"
response.write "<tr><td valign=top align=middle><input type=button value='Cancel' onClick='window.close()'> "
response.write " <input type=button value='Add Events' onClick='AddEvents()'>"
response.write " <input type=button value='Delete Events' onClick='DeleteEvents()'></td></tr></table>"
%>