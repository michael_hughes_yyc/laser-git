
      <!-- Start Client Database Query -->
      <%
      MySQL = "SELECT users.uid, users.ufname, users.uminitial, users.ulname, users.upaddress, users.upcity, users.upstate, users.zipcode, "
      MySQL = MySQL & "users.upPhone, users.umobileno, users.dob, users.ssn, users.sex, "
      MySQL = MySQL & " patients.doctorId, patients.pmcID, patients.RefPrID, patients.RendPrID, "
      MySQL = MySQL & "pharmacy.pharmacyname, pharmacy.pharmacyphone, pharmacy.pharmacyfax "
      MySQL = MySQL & "FROM (users LEFT JOIN patients ON users.uid = patients.pid) LEFT JOIN pharmacy ON patients.pmcId = pharmacy.pmcid "
      MySQL = MySQL & "WHERE (((users.uid)=" & request.querystring("pid") & "))"
      ''response.write MySQL
  
      Set MyRS = Server.CreateObject("ADODB.Recordset") 
      MyRS.Open MySQL, MyConn
 
      if MyRS.EOF then
        response.write "No Matches"
        response.end
      else
    
        if MyRS("doctorid") <> "" then
          MySQL = "SELECT * FROM users WHERE uid=" & MyRS("doctorid")
          ''response.write MySQL
  
          Set MyDR = Server.CreateObject("ADODB.Recordset") 
          MyDR.Open MySQL, MyConn

          if MyDR.EOF then
            client_physicianName = ""
          else
            client_physicianID   = MyRS("doctorid")
            client_physicianName = MyDR("ulname") & ", " & MyDR("ufname") & " " & MyDR("uminitial")
          end if
        end if
  
        if MyRS("RefPrID") <> "" then
          MySQL = "SELECT * FROM users WHERE uid=" & MyRS("RefPrID")
          ''response.write MySQL
  
          Set MyDR = Server.CreateObject("ADODB.Recordset") 
          MyDR.Open MySQL, MyConn
 
          if MyDR.EOF then
            client_physicianNameRef = ""
          else
            client_physicianIDRef   = MyRS("RefPrID")
            client_physicianNameRef = MyDR("ulname") & ", " & MyDR("ufname") & " " & MyDR("uminitial")
          end if
        end if

        if MyRS("RendPrID") <> "" then
          MySQL = "SELECT * FROM users WHERE uid=" & MyRS("RendPrID")
          ''response.write MySQL
  
          Set MyDR = Server.CreateObject("ADODB.Recordset") 
          MyDR.Open MySQL, MyConn
 
          if MyDR.EOF then
            client_physicianNameRend = ""
          else
            client_physicianIDRend   = MyRS("RendPrID")
            client_physicianNameRend = MyDR("ulname") & ", " & MyDR("ufname") & " " & MyDR("uminitial")
          end if
        end if

        ''Start Client List Database Mapping
        client_id                    = MyRS("uid")
        client_firstname             = MyRS("ufname")
        client_middleinitial         = MyRS("uminitial")
        client_lastname              = MyRS("ulname")
        client_address               = MyRS("upaddress")
        client_city                  = MyRS("upcity")
        client_state                 = MyRS("upstate")
        client_postalcode            = MyRS("zipcode")
        client_phonenumber           = MyRS("upPhone")
        client_phonecellular         = MyRS("umobileno")
        client_dateofbirth           = MyRS("dob")
        client_gender                = MyRS("sex")
        client_ssn                   = MyRS("ssn")
        client_age                   = datediff("yyyy", client_dateofbirth, date)
        client_pharmacyID            = MyRS("pmcid")
        client_pharmacyName          = MyRS("pharmacyname")
        client_pharmacyPhoneNumber   = MyRS("pharmacyphone")
        client_pharmacyPhoneFax      = MyRS("pharmacyfax")
        ''End Client List Database Mapping

        MySQL = "SELECT DISTINCTROW insurancedetail.insid, insurance.insuranceName, insurancedetail.subscriberno FROM insurancedetail LEFT JOIN insurance ON insurancedetail.insid=insurance.insId WHERE (((insurancedetail.pid)=" & request.querystring("pid") & "))"
        ''response.write MySQL
  
        Set MyINS = Server.CreateObject("ADODB.Recordset") 
        MyINS.Open MySQL, MyConn

        intClientINS = 0   
        Do While Not MyINS.EOF
          intClientINS = intClientINS + 1
          if intClientINS = 1 then
            client_insuranceID = MyINS("insid")
            client_insuranceName = MyINS("insurancename")
            client_insurancePolicyNumber = MyINS("subscriberno")
          end if
          if intClientINS = 2 then
            client_insuranceIDSecond = MyINS("insid")
            client_insuranceNameSecond = MyINS("insurancename")
            client_insurancePolicyNumberSecond = MyINS("subscriberno")
          end if
          if intClientINS = 3 then
            client_insuranceIDThird = MyINS("insid")
            client_insuranceNameThird = MyINS("insurancename")
            client_insurancePolicyNumberThird = MyINS("subscriberno")
          end if
       MyINS.MoveNext 
       Loop
  
      %>
      <!-- End Client Database Query -->
