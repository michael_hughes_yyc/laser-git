
            <!-- Start Patient Information Form -->
            <input type="hidden" name="client_action" value="<%= request.form("clientadd") %>">
            <input type="hidden" name="client_form" value="<%= request.querystring("form") %>">
            <table width="100%">
            <tr>
                <td colspan="2"><h2>Client Information</h2></td>
			</tr>
			<tr>
                <td width="35%">Name: (ID:<%= right("00000" & client_id, 5) %>)</td>
                <td width="65%">
                <input type="text" name="client_firstname" size="25" value="<%= client_firstname %>">
                <input type="text" name="client_middleinitial" size="5" value="<%= client_middleinitial %>">
                <input type="text" name="client_lastname" size="25" value="<%= client_lastname %>">
                </td>
            </tr>
            <tr>
                <td>Address: </td>
                <td>
                <input type="text" name="client_address" size="25" value="<%= client_address %>">
                </td>
            </tr>
            <tr>
                <td>City, State/Province, Zip/Postal Code: </td>
                <td>
                <input type="text" name="client_city" size="25" value="<%= client_city %>">
                <input type="text" name="client_state" size="5" value="<%= client_state %>">
                <input type="text" name="client_postalCode" size="5" value="<%= client_postalCode %>">
                </td>
            </tr>
            <tr>
                <td>Telephone: </td>
                <td>
                <input type="text" name="client_phoneNumber" size="25" value="<%= client_phoneNumber %>">
                </td>
            </tr>
            <tr>
                <td>Cellular Telephone: </td>
                <td>
                <input type="text" name="client_phoneCellular" size="25" value="<%= client_phoneCellular %>">
                </td>
            </tr>
            <tr>
                <td>Date of Birth: </td>
                <td>
                <input type="text" name="client_dateofbirth" size="25" value="<%= client_dateofbirth %>">
                </td>
            </tr>
            <tr>
                <td>Age: </td>
                <td>
                <input type="text" name="client_age" size="25" value="<%= client_age %>">
                </td>
            </tr>
            <tr>
                <td>Social Security Number: </td>
                <td>
                <input type="text" name="client_ssn" size="25" value="<%= client_ssn %>">
                </td>
            </tr>
			<tr>
                <td>Primary Care Physician Name: (ID:<%= right("00000" & client_physicianID, 5) %>)</td>
                <td>
                  <select name="client_physicianID">
                  <option value='<%= client_physicianID %>' selected='selected'><%= client_physicianName %></option>
                  <option value='0'>None</option>
                  <!--#include file="doctor_select.inc"-->
                  </select>
                </td>
            </tr>
			<tr>
                <td>Referring Physician Name: (ID:<%= right("00000" & client_physicianIDRef, 5) %>)</td>
                <td>
                  <select name="client_physicianIDRef">
                  <option value='<%= client_physicianIDRef %>' selected='selected'><%= client_physicianNameRef %></option>
                  <option value='0'>None</option>
                  <!--#include file="doctor_select.inc"-->
                  </select>
                </td>
            </tr>
			<tr>
                <td>Rendering Physician Name: (ID:<%= right("00000" & client_physicianIDRend, 5) %>)</td>
                <td>
                  <select name="client_physicianIDRend">
                  <option value='<%= client_physicianIDRend %>' selected='selected'><%= client_physicianNameRend %></option>
                  <option value='0'>None</option>
                  <!--#include file="doctor_select_rendering.inc"-->
                  </select>
                </td>
            </tr>
			<tr>
                <td>Pharmacy Name: (ID:<%= right("00000" & client_pharmacyID, 5) %>)</td>
                <td>
                  <select name="client_pharmacyID">
                  <option value='<%= client_pharmacyID %>' selected='selected'><%= client_pharmacyName %></option>
                  <option value='0'>None</option>
                  <!--#include file="pharmacy_select.inc"-->
                  </select>
                </td>
            </tr>
            <tr>
                <td>Pharmacy Telephone: </td>
                <td>
                <input type="text" name="client_pharmacyPhoneNumber" size="25" value="<%= client_pharmacyPhoneNumber %>">
                </td>
            </tr>
            <tr>
                <td>Pharmacy Fax: </td>
                <td>
                <input type="text" name="client_pharmacyPhoneFax" size="25" value="<%= client_pharmacyPhoneFax %>">
                </td>
            </tr>
            <tr>
                <td>Gender: </td>
                <td>
                  <table>
                    <tr>
                      <td width="100"><input type="radio" name="client_gender" value="Male" <% if (not isNull(client_gender)) and ucase(client_gender) = ucase("Male") then response.write " checked" %>> Male</td>
                      <td width="100"><input type="radio" name="client_gender" value="Female" <% if (not isNull(client_gender)) and ucase(client_gender) = ucase("Female") then response.write " checked" %>> Female</td>
                    </tr>
                  </table>
                </td>
            </tr>
            </table>
            <!-- End Patient Information Form -->
