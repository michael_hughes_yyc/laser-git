<!--#include file="adovbs.inc"-->
<!--#include file="db.asp"-->
<!--#include file="header.inc"-->

<!-- Start MedLog Add -->
<%
''response.write request.form
''response.end

if request.form("medlogadd") = "Add" then  
Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

'displays header on form field validation pages
function displayHeader()
	Response.Write "<html>"
	Response.Write "<head>"
	Response.Write "<body>"
	Response.Write "<p align='center'>"
	Response.Write "<div align = 'center'>"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.Write "<font size='4' color=red face='arial'><strong>Error</strong><br /></p>"
end function

'Validate user entries in form fields and generate error pages
For Each key in Request.Form
		strName = key
		strValue = Request.Form(key)
		Session(strName) = strValue
	Next

    client_id                          = Request.Form("client_id")
    medlog_id                          = RandomID
    client_firstname                   = Request.Form("client_firstname")
    client_middleinitial               = Request.Form("client_middleinitial")
    client_lastname                    = Request.Form("client_lastname")
    client_dateofbirth                 = Request.Form("client_dateofbirth")
    client_phoneNumber                 = Request.Form("client_phoneNumber")
    client_pharmacyID                  = Request.Form("client_pharmacyID")
    client_pharmacyName                = Request.Form("client_pharmacyName")
    client_pharmacyPhoneNumber         = Request.Form("client_pharmacyPhoneNumber")
    client_pharmacyPhoneFax            = Request.Form("client_pharmacyPhoneFax")
	medlog_rxdate                      = Request.Form("medlog_rxdate")
	if Request.Form("medlog_drugID") <> "0" then medlog_eCWID = left(Request.Form("medlog_drugID"),(instr(Request.Form("medlog_drugID"),",")-1))
	medlog_eCWName                     = mid( Request.Form("medlog_drugID"), (instr(Request.Form("medlog_drugID"),",")+1), len(Request.Form("medlog_drugID")) - instr(Request.Form("medlog_drugID"),",") )
    medlog_dosage                      = Request.Form("medlog_dosage")
    medlog_directions                  = Request.Form("medlog_directions")
    medlog_amount                      = Request.Form("medlog_amount")
    medlog_donotfill                   = Request.Form("medlog_donotfill")
    medlog_numberofrefills             = Request.Form("medlog_numberofrefills")
    medlog_comments                    = Request.Form("medlog_comments")

If medlog_rxdate = "" or len(medlog_rxdate) <10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The prescription date is a required field. Please enter as MM/DD/YYYY." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(medlog_rxdate) > 10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The prescription date is too long. Please enter as MM/DD/YYYY." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If medlog_eCWID = "" or medlog_eCWID = "0" then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The medication is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If medlog_dosage = "" or len(medlog_dosage) <3 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The dosage a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(medlog_dosage) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The dosage is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(medlog_directions) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The directions are too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If medlog_amount = "" then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The amount is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If int(medlog_amount) >1000 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The amount is too large.  Maximum amount is 1000." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(medlog_donotfill) > 10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The do not fill date is too long. Please enter as MM/DD/YYYY." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If medlog_numberofrefills = "" then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The number of refills is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If int(medlog_numberofrefills) >10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The number of refills is too large.  Maximum amount is 10." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(medlog_comments) >250 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The comments are too long.  Maximum length is 250 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if


    MySQL = "SELECT * FROM medlog"

    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

      MyRS.AddNew
      MyRS("pid")             = client_id
      MyRS("ControlNo")       = medlog_id
      MyRS("ufname")          = client_firstname
      MyRS("uminitial")       = client_middleinitial
      MyRS("ulname")          = client_lastname
      MyRS("dob")             = client_dateofbirth
      MyRS("upphone")         = client_phoneNumber
      MyRS("pmcid")           = client_pharmacyID
      MyRS("pharmacyName")    = client_pharmacyName
      MyRS("pharmacyPhone")   = client_pharmacyPhoneNumber
      MyRS("pharmacyFax")     = client_pharmacyPhoneFax
	  MyRS("rxdate")          = medlog_rxdate
      MyRS("eCWID")           = medlog_eCWID
	  MyRS("eCWName")         = medlog_eCWName
      MyRS("dosage")          = medlog_dosage
      MyRS("directions")      = medlog_directions
      MyRS("amount")          = medlog_amount
      MyRS("donotfill")       = medlog_donotfill
      MyRS("numberofrefills") = medlog_numberofrefills
      MyRS("comments")        = medlog_comments
    MyRS.Update

    MyRS.close
    set MyRS = nothing


    response.write "Update Successful<br />"
end if


if  request.form("clientadd") <> "Add" then
    response.write "<form action='client.asp' method=POST>"
    response.write "<div id='medlog'>Pactient Search<br /></div>"
    response.write "<input type='text' name='client' size='30' value=''>&nbsp;&nbsp;"
    response.write "<input type='submit' name='clientsearch' value='Search'><input type='submit' name='clientadd' value='Add'><br /></form>"
end if
%>

<!-- End MedLog Add -->
<!--#include file="footer.inc"-->