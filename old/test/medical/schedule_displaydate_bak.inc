<%

set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_SCHstring

Set MyRS = Server.CreateObject("ADODB.Recordset") 

function getdata()
  test = doctor_id & ":" & displaydate & ":" & displaytime
  displaydate_year  = right(displaydate,4)
  displaydate_month = left(displaydate,2)
  displaydate_day   = mid(displaydate,4,2)
  ''response.write doctor_id & ":" & displaydate_year & ":" & displaydate_month & ":" & displaydate_day & ":" & displaytime
  MySQL = "select * from SchEvents where scheduleEmployeeID = '" & doctor_id & "' and scheduleYear = '" & displaydate_year & "' and scheduleMonth = '" & displaydate_month & "' and scheduleDay = '" & displaydate_day & "' and scheduleStartTime = '" & displaytime & "'"
  ''response.write MySQL
  MyRS.Open MySQL, MyConn

  if MyRS.EOF then
    response.write "&nbsp;"
  else
    intScheduleCount = 0
    Do While Not MyRS.EOF
      ''Start Schedule List Database Mapping
      schedule_id            = MyRS("scheduleid")
      schedule_clientID      = MyRS("scheduleclientID")
      schedule_client        = MyRS("scheduleclient")
      schedule_doctorID      = MyRS("scheduleemployeeID")
      schedule_doctor        = MyRS("scheduleemployee")
      schedule_date          = MyRS("scheduleMonth") & "/" & MyRS("scheduleDay") & "/" & MyRS("scheduleYear")
      schedule_time          = MyRS("scheduleStartTime") & "-" & MyRS("scheduleEndTime")
      schedule_phonenumber   = MyRS("schedulephone")
      schedule_dateofbirth   = MyRS("scheduledob")
      schedule_procedure     = MyRS("schedulenotes")
      schedule_type          = MyRS("scheduletype")
      schedule_status        = MyRS("schedulestatus")
      ''End Schedule List Database Mapping  
 
      if intScheduleCount > 0 then response.write "<br />"
      if field_name      then 
        response.write "<a href='schedule.asp?pid=" & schedule_clientID & "&scheduleid=" & schedule_id 
        response.write "&scheduledate="& Server.URLEncode(dToday) & "&year=" & displaydate_year & "&month=" & displaydate_month & "&day=" & displaydate_day
        response.write "'>" & schedule_client & "</a>: "
      end if
      if field_dob       then response.write "DOB:" & schedule_dateofbirth &  " "
      if field_phone     then response.write "Tel:" & schedule_phonenumber & " "
      if field_procedure then response.write schedule_procedure
      intScheduleCount = intScheduleCount + 1
    MyRS.MoveNext 
    Loop   
  end if
  MyRS.Close
  dbhit = dbhit + 1
end function


if request.querystring("scheduledate") = "" then
  displaydate = right("00" & Month(Now),2) & "/" & right("00" & Day(Now), 2) & "/" & right("0000" & Year(Now), 4)
  displaydate_year  = right("0000" & Year(Now), 4)
  displaydate_month = right("00" & Month(Now),2)
  displaydate_day   = right("00" & Day(Now), 2)
  ''displaydate = "06/08/2010"
else
  displaydate = right("00" & request.querystring("month"),2) & "/" & right("00" & request.querystring("day"),2) & "/" & right("0000" & request.querystring("year"), 4) 
  displaydate_year  = right("0000" & request.querystring("year"), 4)
  displaydate_month = right("00" & request.querystring("month"),2)
  displaydate_day   = right("00" & request.querystring("day"),2)
  ''response.write displaydate 
end if
response.write "<div style='font-family:arial;font-size:0.65em;'>Schedule Date: " & displaydate & "</div>"

doctor_header_1 = "Sullivan, MD"
doctor_id_1     = 18772
doctor_header_2 = "Goode, MD"
doctor_id_2     = 18771
doctor_header_3 = "Scott, PA"
doctor_id_3     = 23245
field_name      = true
field_dob       = true
field_phone     = true
field_procedure = true

response.write "<table cellspacing='0' cellpadding='0' border='1'>"
response.write "<tr>"
response.write "<td style='font-family:arial;font-size:0.65em;font-weight:bold;'>Time</td>"

for doctor = 1 to 3
  response.write"<td width='300' style='font-family:arial;font-size:0.65em;font-weight:bold;'>"
  select case doctor
    case 1
      response.write doctor_header_1
    case 2
      response.write doctor_header_2
    case 3 
      response.write doctor_header_3
  end select
  response.write"</td>"
  response.write "<td style='font-family:arial;font-size:0.65em;font-weight:bold;'>Type</td>"
  response.write "<td style='font-family:arial;font-size:0.65em;font-weight:bold;'>Status</td>"
next
response.write"</tr>"

dbhit = 0
x = 800
Do While x <= 1700
  response.write "<tr>"
  displaytime = right("0000" & x, 4)
  response.write "<td style='font-family:arial;font-size:0.65em;'><a href='schedule.asp?starttime=" & displaytime & "&year=" & displaydate_year & "&month=" & displaydate_month & "&day=" & displaydate_day & "'>"
  if displaytime < 1200 then
    response.write left(displaytime,2) & ":" & right(displaytime,2) & "am"
  else
    if left(displaytime,2) = 12 then
      response.write left(displaytime,2) & ":" & right(displaytime,2) & "pm"
    else
      response.write right("00" & cint(left(displaytime,2)-12),2) & ":" & right(displaytime,2) & "pm"
    end if
  end if 
  response.write "</a></td>"
  x = x + 15
  if right(x,2) >= 60 then x = (left(displaytime,2) + 1) * 100

  for doctor = 1 to 3
    select case doctor
      case 1
        doctor_id = doctor_id_1
      case 2
        doctor_id = doctor_id_2
      case 3
        doctor_id = doctor_id_3
    end select

    response.write "<td style='font-family:arial;font-size:0.65em;'>"
    getdata()
    response.write "</td>"

    '' Type: Office Visit=green, Procedure=pink, New Patient=blue, Pump=purple, Blocked Out=yellow
    if schedule_type = "" then
      response.write "<td>&nbsp;</td>"
    else
      select case schedule_type
      case "Office Visit"
        response.write "<td nowrap>" & schedule_type & "</td>"
      case "Procedure"
        response.write "<td nowrap>" & schedule_type & "</td>"
      case "New Patient"
        response.write "<td nowrap >" & schedule_type & "</td>"
      case "Pump"
        response.write "<td nowrap>" & schedule_type & "</td>"
      case "Blocked Out"
        response.write "<td nowrap>" & schedule_type & "</td>"
      end select
    end if
    schedule_type = ""

    '' Status: Confirmed=dark green, Left Message=yellow, Rescheduled=red, No Show=red, Cancelled=red
    if schedule_status = "" then
      response.write "<td>&nbsp;</td>"
    else
      select case schedule_status
      case "Confirmed"
        response.write "<td nowrap >" & schedule_status & "</td>"
      case "Left Message"
        response.write "<td nowrap>" & schedule_status & "</td>"
      case "Rescheduled"
        response.write "<td nowrap>" & schedule_status & "</td>"
      case "No Show"
        response.write "<td nowrap>" & schedule_status & "</td>"
      case "Cancelled"
        response.write "<td nowrap>" & schedule_status & "</td>"
      end select
    end if
    schedule_status = ""

  next
  response.write "</tr>"
loop

response.write "</table>"
response.write "<div>Database Queries: " & dbhit & "</div>"
%>