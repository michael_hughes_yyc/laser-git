<% 
  if session("login") <> "valid" then 
    session("login") = ""
    response.redirect "login.asp"
  end if

  page = ""
  name = ""
  form = ""
%>

<!--#include file="adovbs.inc"-->
<!--#include file="db.asp"-->
<!--#include file="func_activity.inc"-->
<!--#include file="header.inc"-->

<font face='Verdana' <font size='2'><font size='3'><br /><br />
Help Topics:
<br />
<br />
<b>Problem:</b><br />
Unable to view attachments in patient record<br />
<br />
<b>Resolution:</b><br />
TIFF Viewer is not installed.  Please click on the following link and save ALTTIFF.OCX to C:\Program Files<br />
Download: <a href="https://cdpcweb.com:8443/alttiff.ocx">TIFF Viewer</a>
<br />
<br />
To install ALTTIFF.OCX click Start, Run, paste in the following line and press Enter:<br />
regsvr32 "c:\program files\alttiff.ocx" /s<br />
<br />
If no error message is displayed, then the TIFF Viewer module has been properly registered on the computer.
<br />

<!--#include file="footer.inc"-->
