
            <!-- Start Patient Information Form -->
            <table width="100%" >
            <tr>
                <td colspan="2"><strong>Client Medication</strong></td>
			</tr>
			<tr>
                <td width="35%">Name: (ID:<%= right("00000" & client_id, 5) %>)</td>
                <td width="65%">
                <input type="text" name="client_firstname" size="25" value="<%= client_firstname %>">
                <input type="text" name="client_middleinitial" size="5" value="<%= client_middleinitial %>">
                <input type="text" name="client_lastname" size="25" value="<%= client_lastname %>">
                </td>
            </tr>
            <tr>
                <td>Telephone: </td>
                <td>
                <input type="text" name="client_phoneNumber" size="25" value="<%= client_phoneNumber %>">
                </td>
            </tr>
            <tr>
                <td>Date of Birth: </td>
                <td>
                <input type="text" name="client_dateofbirth" size="25" value="<%= client_dateofbirth %>">
                </td>
            </tr>
			<tr>
                <td>Pharmacy Name: (ID:<%= right("00000" & client_pharmacyID, 5) %>)</td>
                <td>
                  <select name="client_pharmacyID">
                  <option value='<%= client_pharmacyID %>' selected='selected'><%= client_pharmacyName %></option>
                  <option value='0'>None</option>
                  <!--#include file="pharmacy_select.inc"-->
                  </select>
                </td>
            </tr>
            <tr>
                <td>Pharmacy Telephone: </td>
                <td>
                <input type="text" name="client_pharmacyPhoneNumber" size="25" value="<%= client_pharmacyPhoneNumber %>">
                </td>
            </tr>
            <tr>
                <td>Pharmacy Fax: </td>
                <td>
                <input type="text" name="client_pharmacyPhoneFax" size="25" value="<%= client_pharmacyPhoneFax %>">
                </td>
            </tr>
            </table>
            </form>
            <!-- End Patient Information Form -->

            <!-- Start Patient Medication Log -->

            <form action="client_medlog_add.asp" method="post">
            <table width="100%" class="medlog-add">
            <tr>
                <td colspan="8"><strong>Medication Tracking Log</strong></td>
			</tr>
			<tr>
                <td width="10%">RX Date</td>
                <td width="25%">Medication</td>
                <td width="10%">Dosage</td>
                <td width="25%">Directions</td>
                <td width="10%">Amount</td>
                <td width="10%">Do Not Fill</td>
                <td width="10%"># of Refills</td>
                <td width="20%">Comments</td>
            </tr>

              <!--#include file="client_medlog_view.inc"-->
              <% 
                varMonth = right("00" & left(date, inStr(date, "/")-1), 2)
                varDay   = right("00" & left(right(date, (len(date) - inStr(date, "/"))), inStr(right(date, (len(date) - inStr(date, "/"))), "/")-1), 2)
                varYear  = right("0000" & date,4)
                varDate  = varMonth & "/" & varDay & "/" & varYear
               %>

			<tr>
                <td width="10%"><input type="text" name="medlog_rxdate" size="10" value="<%= varDate %>"></td>
                <td width="25%"><select name='medlog_drugid'>
                  <option value='0'>None</option>"
                  <!--#include file="drug_list.inc"-->
                </td>
                <td width="10%"><input type="text" name="medlog_dosage" size="7" value=""></td>
                <td width="15%"><input type="text" name="medlog_directions" size="32" value=""></td>
                <td width="10%"><input type="text" name="medlog_amount" size="7" value=""></td>
                <td width="10%"><input type="text" name="medlog_donotfill" size="10" value=""></td>
                <td width="10%"><input type="text" name="medlog_numberofrefills" size="8" value=""></td>
                <td width="30%"><input type="text" name="medlog_comments" size="20" value=""><br /><input type='submit' name='medlogadd' value='Add'></td>
            </tr>

            </table>
            <input type="hidden" name="client_id"                  value="<%= client_id %>">
            <input type="hidden" name="client_firstname"           value="<%= client_firstname %>">
            <input type="hidden" name="client_middleinitial"       value="<%= client_middleinitial %>">
            <input type="hidden" name="client_lastname"            value="<%= client_lastname %>">
            <input type="hidden" name="client_dateofbirth"         value="<%= client_dateofbirth %>">
            <input type="hidden" name="client_phoneNumber"         value="<%= client_phoneNumber %>">
            <input type="hidden" name="client_pharmacyID"          value="<%= client_pharmacyID %>">
            <input type="hidden" name="client_pharmacyName"        value="<%= client_pharmacyName %>">
            <input type="hidden" name="client_pharmacyPhoneNumber" value="<%= client_pharmacyPhoneNumber %>">
            <input type="hidden" name="client_pharmacyPhoneFax"    value="<%= client_pharmacyPhoneFax %>">
            </form>
            <!-- End Patient Medication Log -->
