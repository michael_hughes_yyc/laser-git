
      <!-- Start Patient Basics -->
      <table width="100%" >
	    <tr>
          <td width="100%">
            
              <b>Patient: </b><%= client_lastname %>, <%= client_firstname %>&nbsp;<%= client_middleinitial %>&nbsp;&nbsp;&nbsp;
              <b>DOB: </b><%= client_dateofbirth %>&nbsp;&nbsp;&nbsp;
              <b>Age: </b><%= client_age %>&nbsp;&nbsp;&nbsp;
              <b>Sex: </b>
              <%
              if (not isNull(client_gender)) then
                if ucase(client_gender) = ucase("Male") then 
                  response.write "Male"
                else
                  if ucase(client_gender) = ucase("Female") then
                    response.write "Female"
                  end if
                end if  
              end if       
              %>
            
          </td>
        </tr>
        <tr>
          <td width="100%">
            
              <b>Phone: </b><%= client_phoneNumber %>&nbsp;&nbsp;&nbsp;
              <b>Primary Insurance: </b><%= client_insuranceName %>&nbsp;&nbsp;&nbsp;
              <b>Payer ID: </b><%= right("00000" & client_insuranceID, 5) %>
            
          </td>
        </tr>
        <tr>
          <td width="100%">
            
              <b>Address: </b><%= client_address %>, <%= client_city %>, <%= client_state %>-<%= client_postalCode %>
            
          </td>
        </tr>
        <tr>
          <td width="100%">
            
              <b>Pcp: </b><%= client_physicianName %>
            
          </td>
        </tr>
        <tr>
          <td width="100%">
            
              <b>Encounter Date: </b>00/00/0000&nbsp;&nbsp;&nbsp;
              <b>Provider: </b><%= client_physicianNameRend %>
            
          </td>
        </tr>
      <%
      if request.querystring("scheduleid") <> "" then
        response.write "<tr><td><b>Appointment: </b>"
        response.write "<a href='schedule.asp?scheduleid=" & request.querystring("scheduleid") & "&task=modify'>Modify</a> / "
        response.write "<a href='schedule.asp?scheduleid=" & request.querystring("scheduleid") & "&task=delete'>Delete</a>"
        response.write "</td></tr>"
      end if
      if request.querystring("reminderid") <> "" then
        response.write "<tr><td><b>Reminder: </b>"
        response.write "<a href='reminders.asp?reminderid=" & request.querystring("reminderid") & "&task=modify'>Modify</a> / "
        response.write "<a href='reminders.asp?reminderid=" & request.querystring("reminderid") & "&task=done'>Done</a>"
        response.write "</td></tr>"
      end if
      %>
      </table>
      <!-- End Patient Basics -->
