
            <!-- Start Patient Payment Form -->
            <input type="hidden" name="inspid" value="<%= client_id %>">
            <table width="100%">
            <tr>
                <td colspan="2"><h2>Payment Information</h2></td>
            </tr>
			<tr>
                <td width="35%">Primary Insurance Company: (ID:<%= right("00000" & client_insuranceID, 5) %>)</td>
                <td width="65%">
                  <select name="client_insuranceID">
                  <option value='<%= client_insuranceID %>' selected='selected'><%= client_insuranceName %></option>
                  <option value='None'>None</option>
                  <!--#include file="insurance_select.inc"-->
                  </select>
                </td>
            </tr>
            <tr>
                <td>Primary Insurance Policy Number: </td>
                <td>
                <input type="text" name="client_insurancePolicyNumber" size="25" value="<%= client_insurancePolicyNumber %>">
                </td>
            </tr>
			<tr>
                <td>Secondary Insurance Company: (ID:<%= right("00000" & client_insuranceIDSecond, 5) %>)</td>
                <td>
                  <select name="client_insuranceIDSecond">
                  <option value='<%= client_insuranceIDSecond %>' selected='selected'><%= client_insuranceNameSecond %></option>
                  <option value='None'>None</option>
                  <!--#include file="insurance_select.inc"-->
                  </select>
                </td>
            </tr>
            <tr>
                <td>Secondary Insurance Policy Number: </td>
                <td>
                <input type="text" name="client_insurancePolicyNumberSecond" size="25" value="<%= client_insurancePolicyNumberSecond %>">
                </td>
            </tr>
			<tr>
                <td>Third Insurance Company: (ID:<%= right("00000" & client_insuranceIDThird, 5) %>)</td>
                <td>
                  <select name="client_insuranceIDThird">
                  <option value='<%= client_insuranceIDThird %>' selected='selected'><%= client_insuranceNameThird %></option>
                  <option value='None'>None</option>
                  <!--#include file="insurance_select.inc"-->
                  </select>
                </td>
            </tr>
            <tr>
                <td>Third Insurance Policy Number: </td>
                <td>
                <input type="text" name="client_insurancePolicyNumberThird" size="25" value="<%= client_insurancePolicyNumberThird %>">
                </td>
            </tr>
            <tr>
              <td colspan="2"><hr color="#CCCCCC" size="1" noshade></td>
            </tr>
          </table>
          <!-- End Patient Payment Form -->
