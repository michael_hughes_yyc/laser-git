<!-- Start Doctor List -->
<%
if (request.form("doctoradd") <> "Add") and (request.form("doctorsearch") = "Search" or request.form("doctor") <> "") then

   search_id = request.form("doctor")
   usertype_id = 5
   delflag_id = 0
   MySQL = "SELECT * FROM users WHERE ((ulname like '" & search_id & "%' or ufname like '%" & search_id & "%' or upcity like '%" & search_id & "%' or upstate like '%" & search_id & "%') and (usertype = " & usertype_id & ") and (delflag = " & delflag_id & ")) ORDER BY ulname"
   ''response.write MySQL
   ''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
   ''delflag: 0-Active, 1-Deleted
   field_acctno   = true
   field_name     = true
   field_dob      = false
   field_phone    = true
   field_location = true

   intDoctorID = 0
   Set MyRS = Server.CreateObject("ADODB.Recordset") 
   MyRS.Open MySQL, MyConn

   if MyRS.EOF then
   
     response.write "No Matches"
     response.end
   end if
  
   response.write "<table class='doc-list'>" & vbCrLf
   response.write "  <tr>" & vbCrLf
   if field_acctno   then response.write "    <td  class='smaller'>AcctNo</td>" & vbCrLf
   if field_name     then response.write "    <td><b>Name</b></td>" & vbCrLf
   if field_dob      then response.write "    <td><b>DOB</b></td>" & vbCrLf
   if field_phone    then response.write "    <td><b>Phone</b></td>" & vbCrLf
   if field_location then response.write "    <td><b>Location</b></td>" & vbCrLf
   response.write "  </tr>" & vbCrLf


   Do While Not MyRS.EOF

     ''Start Doctor List Database Mapping
     doctor_id            = MyRS("uid")
     doctor_firstname     = MyRS("ufname")
     doctor_middleinitial = MyRS("uminitial")
     doctor_lastname      = MyRS("ulname")
     doctor_address       = MyRS("upaddress")
     doctor_city          = MyRS("upcity")
     doctor_state         = MyRS("upstate")
     doctor_postalcode    = MyRS("zipcode")
     doctor_phonenumber   = MyRS("upPhone")
     doctor_dateofbirth   = MyRS("dob")
     doctor_gender        = MyRS("sex")
     doctor_ssn           = MyRS("ssn")
     ''End Doctor List Database Mapping

     intDoctorID = intDoctorID + 1
     response.write "  <tr>" & vbCrLf
     if field_acctno   then response.write "    <td  class='smaller'><a href='doctor.asp?id=" & doctor_id & "' class='textlinks'>" & right("000" & doctor_id, 5) & "</a></td>" & vbCrLf
     if field_name     then response.write "    <td>" & doctor_lastname & ", " & doctor_firstname & " " & doctor_middleinitial & "</td>" & vbCrLf
     if field_dob      then response.write "    <td>" & doctor_dateofbirth &  "</td>" & vbCrLf
     if field_phone    then response.write "    <td>" & doctor_phonenumber & "</td>" & vbCrLf
     if field_location then
       if (len(doctor_city) > 1) and (len(doctor_state) > 1) then
         response.write "    <td>" & doctor_city & ", " & doctor_state & "</td>" & vbCrLf
       else
         response.write "    <td> </td>"
       end if
     end if
       response.write "  </tr>" & vbCrLf

   MyRS.MoveNext 
   Loop 

   response.write "  <tr>" & vbCrLf
   response.write "    <td colspan='2'>" & right("00000" & intDoctorID, 5) & " Doctors Found</td>" & vbCrLf
    response.write "  </tr>" & vbCrLf

   response.write "</table>" & vbCrLf

   if intDoctorID < 1 then
     intDoctorID = 1
   else
     intDoctorID = intDoctorID + 1
   end if

   ''response.write "Current Application #: " & intDoctorID

   MyRS.Close
   set MyRS = Nothing
end if
%>
<!-- End Doctor List -->