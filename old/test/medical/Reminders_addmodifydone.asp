<!--#include file="db.asp"-->

<%
' Prevent Caching
Response.Expires = 0
Response.ExpiresAbsolute = now - 1
Response.CacheControl = "no-cache"
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

Response.Write "<html><head><title>Updating Reminder</title></head><body><center>"

set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_String

usertype_id = 3
delflag_id = 0
MySQL = "SELECT * FROM users WHERE uid = " & Request.Item("reminderClientID") & " and (usertype = " & usertype_id & ") and (delflag = " & delflag_id & ")"
Set MyRS = Server.CreateObject("ADODB.Recordset") 
''response.write MySQL
''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
''delflag: 0-Active, 1-Deleted
MyRS.Open MySQL, MyConn
if Not MyRS.EOF then 
  reminderClient         = MyRS("ulname") & ", " & MyRS("ufname") & " " & MyRS("uminitial")
end if
MyRS.close
set MyRS = Nothing


set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_SCHstring

if Request.Item("remindertask") = "modify" or Request.Item("remindertask") = "done" then
  Response.Write "Modifying Item....."
  Response.Flush

  SQL = "Update reminders set "
  SQL = SQL & " reminderId='" & Replace(Request.Item("reminderID"), "'", "''") & "', "
  SQL = SQL & " reminderMonth='" & Request.Item("reminderMonth") & "',"
  SQL = SQL & " reminderYear='" & Request.Item("reminderYear") & "',"
  SQL = SQL & " reminderDay='" & Request.Item("reminderDay") & "',"
  SQL = SQL & " reminderClient='" & reminderClient & "', "
  SQL = SQL & " reminderClientID='" & Request.Item("reminderClientID") & "',"
  SQL = SQL & " reminderStaff='" & Request.Item("reminderStaff") & "', "
  SQL = SQL & " reminderStatus='" & Request.Item("reminderStatus") & "',"
  SQL = SQL & " reminderNotes='" & Replace(Request.Item("reminderNotes"), "'", "''") & "'"
  SQL = SQL & " Where reminderID = '" & Request.Item("reminderID") & "'"
  
  set CONNECT = server.CreateObject("ADODB.Connection")
  Connect.Open DB_SCHstring
  
  ''response.write SQL
  ''response.end
  set mData = Connect.Execute(SQL)
  set MyConn = Nothing
else
  Response.Write "Saving Item....."
  Response.Flush

  SQL = "Insert Into reminders "
  SQL = SQL & "(reminderID, reminderMonth, reminderYear, reminderDay, reminderClient, reminderClientID, reminderStaff, reminderStatus, reminderNotes) values ("
  SQL = SQL & "'" & RandomID & "', "
  SQL = SQL & "'" & Request.Item("reminderMonth") & "', "
  SQL = SQL & "'" & Request.Item("reminderYear") & "', "
  SQL = SQL & "'" & Request.Item("reminderDay") & "', "
  SQL = SQL & "'" & reminderClient & "', "
  SQL = SQL & "'" & Replace(Replace(Request.Item("reminderClientID"), "'", "''"),"<br />", vbCrLf) & "', "
  SQL = SQL & "'" & Request.Item("reminderStaff") & "', "
  SQL = SQL & "'" & Request.Item("reminderStatus") & "', "
  SQL = SQL & "'" & Replace(Replace(Request.Item("reminderNotes"), "'", "''"),"<br />", vbCrLf) & "'"
  SQL = SQL & ");"

  set CONNECT = server.CreateObject("ADODB.Connection")
  Connect.Open DB_SCHstring
  
  ''response.write SQL
  ''response.end
  set mData = Connect.Execute(SQL)
end if

%>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("window.close()",1500);
</SCRIPT>
</body></html>