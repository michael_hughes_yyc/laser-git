
<!-- Start Client Upload Attachment Database Addition -->
<!--#include file="adovbs.inc"-->
<!--#include file="db.asp"-->
<%
    attachment_patientid         = Upload.Form("pid")
    attachment_randomid          = Upload.Form("rid")
    attachment_scannedby         = session("username")
    attachment_filename          = Upload.UploadedFiles(fileKey).FileName
    attachment_itemtype          = Upload.Form("itemtype")
    attachment_description       = Upload.Form("description")


    Dim MyConn, MyRS, MySQL, DB_String, intAttachmentID, intAttachmentHighRecord
 
    set MyConn = Server.CreateObject("ADODB.Connection")
    MyConn.ConnectionTimeout = 15
    MyConn = DB_String

    MySQL = "SELECT * FROM document"

    intAttachmentID = 0
    intAttachmentHighRecord = 0
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn

    Do While Not MyRS.EOF
      intAttachmentID = intAttachmentID + 1
      if intAttachmentHighRecord < MyRS("docID") then intAttachmentHighRecord = MyRS("docID")

    MyRS.MoveNext 
    Loop 

    if intAttachmentID < 1 then
      intAttachmentID = 1
      intAttachmentHighRecord = 1
    else
      intAttachmentID = intAttachmentID + 1
      intAttachmentHighRecord = intAttachmentHighRecord + 1
    end if

    response.write "Current Attachment #: " & intAttachmentID & " High Record #: " & intAttachmentHighRecord & "<br />"

    response.write "PID: " & attachment_patientid & "<br />"
    response.write "RID: " & attachment_randomid & "<br />"
    response.write "USR: " & attachment_scannedby & "<br />"
    response.write "FIL: " & attachment_filename & "<br />"
    response.write "TYP: " & attachment_itemtype & "<br />"
    response.write "DES: " & attachment_description & "<br />"
    ''response.end

    MyRS.Close
    set MyRS = Nothing

    MySQL = "SELECT * FROM document"

    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

    MyRS.AddNew
    MyRS("docID")        = intAttachmentHighRecord
    MyRS("PatientId")    = attachment_patientid
    MyRS("ScannedBy")    = attachment_scannedby
    MyRS("scanDate")     = Date & " " & Time
    MyRS("filename")     = attachment_patientid & "_" & attachment_randomid & "_" & attachment_filename
    MyRS("doc_Type")     = attachment_itemtype
    MyRS("customName")   = attachment_description
    MyRS("delFlag")      = 0
    '' 1-xray, 2-identification, 3-insurance, 4-voice, 5-transcription
    ''Delflag: 0-Active, 1-Deleted
    MyRS.Update

    MyRS.close
    set MyRS = nothing

    set MyConn = Nothing
%>

<!-- End Client Upload Attachment Database Addition -->