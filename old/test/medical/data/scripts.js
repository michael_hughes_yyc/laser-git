function fixMinWidthForIE(){    
	try{       
		if(!document.body.currentStyle){return} //IE only   
 	}catch(e){return}
 	var elems=document.getElementsByTagName("*");   
 	for(e=0; e<elems.length; e++){       
 		 var eCurStyle = elems[e].currentStyle;       
 		 var l_minWidth = (eCurStyle.minWidth) ? eCurStyle.minWidth : eCurStyle.getAttribute("min-width"); //IE7 : IE6       
		 if(l_minWidth && l_minWidth != 'auto'){          
			 var shim = document.createElement("DIV");          
			 shim.style.cssText = 'margin:0 !important; padding:0 !important; border:0 !important; line-height:0 !important; height:0 !important; BACKGROUND:RED;';          
			 shim.style.width = l_minWidth;          
			 shim.appendChild(document.createElement("&nbsp;"));          
			 if(elems[e].canHaveChildren){
				 elems[e].appendChild(shim);          }
			 else{            
				 //  do nothing          
			}       
		}    
	} 
}

function setSubmits() {
	
	 $('#:submit').each(function(){ 

	   var submitBtnVal = $(this).val(); 
	   if (submitBtnVal == "Search") {
	   	   $(this).addClass("searchbtn");
	   }
	    if (submitBtnVal == "Add") {
	   	   $(this).addClass("addbtn");
	   }
	}); 
}

function setMobileStyles() {

 // ipad i believe is 768 wide
   // if (screen.width <= 800) {
  if (screen.width <= 2800) {
    $('link[href="data/styles.css"]').attr('href','data/mobile.css'); 
    
    $('#footer .menu').addClass("hide");
    
  } else {
   // we don't need to do anything
   //console.warn(screen.width);
  }
	 
	 

} 
