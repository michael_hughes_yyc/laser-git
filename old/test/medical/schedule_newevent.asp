<!--#include File = "CalendarInclude.asp"-->
<!--#include file="db.asp"-->

<%
' Prevent Caching
Response.Expires = 0
Response.ExpiresAbsolute = now - 1
Response.CacheControl = "no-cache"
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_String

response.write "<head><title>Schedule Addition</title>"
response.write "<script language=javascript>"
  Response.Write "function SubmitMe()"
  Response.Write "{"
  Response.Write "	myRegExp = new RegExp(""\n"", ""g"");"
  Response.Write "	mcalNotes = document.ThisForm.scheduleNotes.value.replace(""&"",""and"");"
  Response.Write "	mcalNotes = mcalNotes.replace(myRegExp,""<br />"");"
  Response.Write "window.showModalDialog('schedule_addmodifydelete.asp?"
  Response.Write "scheduleMonth=' + document.ThisForm.scheduleMonth.value + "
  Response.Write "'&scheduleDay=' + document.ThisForm.scheduleDay.value + "
  Response.Write "'&scheduleYear=' + document.ThisForm.scheduleYear.value + "
  Response.Write "'&scheduleStartTime=' + document.ThisForm.scheduleStartTime.value + "
  Response.Write "'&scheduleEndTime=' + document.ThisForm.scheduleEndTime.value + "
  Response.Write "'&scheduleEmployeeID=' + document.ThisForm.client_physicianIDRend.value + "
  Response.Write "'&scheduleClientID=' + document.ThisForm.scheduleClientID.value + "
  Response.Write "'&schedulestatus=' + document.ThisForm.schedulestatus.value + "
  Response.Write "'&scheduletype=' + document.ThisForm.scheduletype.value + "
  Response.Write "'&scheduletask=' + document.ThisForm.scheduletask.value + "
  Response.Write "'&scheduleNotes=' + mcalNotes,"
  Response.Write "'Updating', 'scroll: yes help: yes; status: yes; dialogWidth: 500px; dialogHeight: 250px;');"
  Response.Write "}"
  Response.Write "function UpdateMe()"
  Response.Write "{"
  Response.Write "	myRegExp = new RegExp(""\n"", ""g"");"
  Response.Write "	mcalNotes = document.ThisForm.scheduleNotes.value.replace(""&"",""and"");"
  Response.Write "	mcalNotes = mcalNotes.replace(myRegExp,""<br />"");"
  Response.Write "window.showModalDialog('schedule_addmodifydelete.asp?"
  Response.Write "scheduleMonth=' + document.ThisForm.scheduleMonth.value + "
  Response.Write "'&scheduleDay=' + document.ThisForm.scheduleDay.value + "
  Response.Write "'&scheduleYear=' + document.ThisForm.scheduleYear.value + "
  Response.Write "'&scheduleStartTime=' + document.ThisForm.scheduleStartTime.value + "
  Response.Write "'&scheduleEndTime=' + document.ThisForm.scheduleEndTime.value + "
  Response.Write "'&scheduleEmployeeID=' + document.ThisForm.client_physicianIDRend.value + "
  Response.Write "'&scheduleClientID=' + document.ThisForm.scheduleClientID.value + "
  Response.Write "'&schedulestatus=' + document.ThisForm.schedulestatus.value + "
  Response.Write "'&scheduletype=' + document.ThisForm.scheduletype.value + "
  Response.Write "'&scheduletask=' + document.ThisForm.scheduletask.value + "
  Response.Write "'&scheduleid=' + document.ThisForm.scheduleid.value + "
  Response.Write "'&scheduleNotes=' + mcalNotes,"
  Response.Write "'Updating', 'scroll: yes help: yes; status: yes; dialogWidth: 100px; dialogHeight: 50px;');"
  Response.Write "}"
  Response.Write "function CancelMe()"
  Response.Write "{"
  Response.Write "window.location = " & chr(34) & "schedule.asp" & chr(34) & ";"
  Response.Write "}"
response.write "</script>"
response.write "</head>"
response.write "<body>"

if request.querystring("scheduleid") <> "" then
  MySQL = "SELECT * FROM schEvents WHERE scheduleid = '" & request.querystring("scheduleid") & "'"
  ''response.write MySQL
  MyConn2 = DB_SCHstring
  Set MyRS = Server.CreateObject("ADODB.Recordset") 
  MyRS.Open MySQL, MyConn2

  if MyRS.EOF then
    response.write "No Matches"
    response.end

  else

    ''Start Schedule List Database Mapping
    schedule_id            = MyRS("scheduleid")
    schedule_clientID      = MyRS("scheduleclientID")
    schedule_client        = MyRS("scheduleclient")
    schedule_doctorID      = MyRS("scheduleemployeeID")
    schedule_doctor        = MyRS("scheduleemployee")
    schedule_month         = MyRS("scheduleMonth")
    schedule_day           = MyRS("scheduleDay")
    schedule_year          = MyRS("scheduleYear")
    schedule_starttime     = MyRS("scheduleStartTime")
    schedule_endtime       = MyRS("scheduleEndTime")
    schedule_phonenumber   = MyRS("schedulephone")
    schedule_dateofbirth   = MyRS("scheduledob")
    schedule_procedure     = MyRS("schedulenotes")
    schedule_type          = MyRS("scheduletype")
    schedule_status        = MyRS("schedulestatus")
    ''End Schedule List Database Mapping
  end if

  MyRS.Close
  set MyRS = Nothing

end if


mYear = Request.Item("year")
mMonth = Request.Item("month")
mDay = Request.Item("day")
''if mYear = "" then mYear = Year(date)
''if mMonth = "" then mMonth = Month(date)
''if mDay = "" then mDay = Day(date)

response.write "<form ID=ThisForm Name=ThisForm >"
response.write "<table width='100%' height='100%' >"
response.write "<tr><td height='1%'>"
response.write "<table width='100%'>"
response.write "<tr><td><b>Schedule</b></td><tr>"
response.write "<tr><td><b>Date</b></td>"
response.write "<td>"

function selected_month(data)
  if schedule_month = data then 
    selected_month = " selected='selected'"
  else
    if mMonth = data then 
      selected_month = " selected='selected'"
    end if
  end if
end function
response.write "<select name='scheduleMonth'>"
response.write "  <option value='01'" & selected_month("01") & ">01 - January</option>"
response.write "  <option value='02'" & selected_month("02") & ">02 - February</option>"
response.write "  <option value='03'" & selected_month("03") & ">03 - March</option>"
response.write "  <option value='04'" & selected_month("04") & ">04 - April</option>"
response.write "  <option value='05'" & selected_month("05") & ">05 - May</option>"
response.write "  <option value='06'" & selected_month("06") & ">06 - June</option>"
response.write "  <option value='07'" & selected_month("07") & ">07 - July</option>"
response.write "  <option value='08'" & selected_month("08") & ">08 - August</option>"
response.write "  <option value='09'" & selected_month("09") & ">09 - September</option>"
response.write "  <option value='10'" & selected_month("10") & ">10 - October</option>"
response.write "  <option value='11'" & selected_month("11") & ">11 - November</option>"
response.write "  <option value='12'" & selected_month("12") & ">12 - December</option>"
response.write "</select>"

function selected_day(data)
  if schedule_day = data then 
    selected_day = " selected='selected'"
  else
    if mDay = data then 
      selected_day = " selected='selected'"
    end if
  end if
end function
response.write "<select name='scheduleDay'>" 
response.write "  <option value='01'" & selected_day("01") & ">01</option>"
response.write "  <option value='02'" & selected_day("02") & ">02</option>"
response.write "  <option value='03'" & selected_day("03") & ">03</option>"
response.write "  <option value='04'" & selected_day("04") & ">04</option>"
response.write "  <option value='05'" & selected_day("05") & ">05</option>"
response.write "  <option value='06'" & selected_day("06") & ">06</option>"
response.write "  <option value='07'" & selected_day("07") & ">07</option>"
response.write "  <option value='08'" & selected_day("08") & ">08</option>"
response.write "  <option value='09'" & selected_day("09") & ">09</option>"
response.write "  <option value='10'" & selected_day("10") & ">10</option>"
response.write "  <option value='11'" & selected_day("11") & ">11</option>"
response.write "  <option value='12'" & selected_day("12") & ">12</option>"
response.write "  <option value='13'" & selected_day("13") & ">13</option>"
response.write "  <option value='14'" & selected_day("14") & ">14</option>"
response.write "  <option value='15'" & selected_day("15") & ">15</option>"
response.write "  <option value='16'" & selected_day("16") & ">16</option>"
response.write "  <option value='17'" & selected_day("17") & ">17</option>"
response.write "  <option value='18'" & selected_day("18") & ">18</option>"
response.write "  <option value='19'" & selected_day("19") & ">19</option>"
response.write "  <option value='20'" & selected_day("20") & ">20</option>"
response.write "  <option value='21'" & selected_day("21") & ">21</option>"
response.write "  <option value='22'" & selected_day("22") & ">22</option>"
response.write "  <option value='23'" & selected_day("23") & ">23</option>"
response.write "  <option value='24'" & selected_day("24") & ">24</option>"
response.write "  <option value='25'" & selected_day("25") & ">25</option>"
response.write "  <option value='26'" & selected_day("26") & ">26</option>"
response.write "  <option value='27'" & selected_day("27") & ">27</option>"
response.write "  <option value='28'" & selected_day("28") & ">28</option>"
response.write "  <option value='29'" & selected_day("29") & ">29</option>"
response.write "  <option value='30'" & selected_day("30") & ">30</option>"
response.write "  <option value='31'" & selected_day("31") & ">31</option>"
response.write "</select>"

function selected_year(data)
  if schedule_year = data then 
    selected_year = " selected='selected'"
  else
    if mYear = data then 
      selected_year = " selected='selected'"
    end if
  end if
end function
response.write "<select name='scheduleYear'>"
response.write "  <option value='2010'" & selected_year("2010") & ">2010</option>"
response.write "  <option value='2011'" & selected_year("2011") & ">2011</option>"
response.write "  <option value='2012'" & selected_year("2012") & ">2012</option>"
response.write "</select>"
response.write "</td>"

function selected_type(data)
  if schedule_type = data then 
    selected_type = " selected='selected'"
  end if
end function
response.write "<td align=right NOWRAP><b>Type: </b>"
response.write "<Select name='scheduletype'>"
response.write "  <option value='Office Visit'" & selected_type("Office Visit") & ">Office Visit</option>"
response.write "  <option value='Procedure'" & selected_type("Procedure") & ">Procedure</option>"
response.write "  <option value='New Patient'" & selected_type("New Patient") & ">New Patient</option>"
response.write "  <option value='Pump'" & selected_type("Pump") & ">Pump</option>"
response.write "  <option value='Blocked Out'" & selected_type("Blocked Out") & ">Blocked Out</option>"
response.write "</select>"
response.write "</td>"
response.write "</tr>"           
response.write "<tr><td><b>Provider:</b></td>"
response.write "<td>" & vbCrLF
response.write "<select name='client_physicianIDRend'>" & vbCrLF
response.write "  <option value='0'>None</option>" & vbCrLF
%>
  <!--#include file="doctor_select_rendering.inc"-->
<% 
response.write "</select>"
response.write "</td>"

if request.querystring("task") = "delete" or request.querystring("task") = "modify" then
  function selected_status(data)
    if schedule_status = data then 
      selected_status = " selected='selected'"
    end if
  end function
  response.write "<td align=right NOWRAP><b>Status: </b>"
  response.write "<Select name='schedulestatus'>"
  response.write "  <option value=''>...</option>"
  response.write "  <option value='Confirmed'" & selected_status("Confirmed") & ">Confirmed</option>"
  response.write "  <option value='Left Message'" & selected_status("Left Message") & ">Left Message</option>"
  response.write "  <option value='Rescheduled'" & selected_status("Rescheduled") & ">Rescheduled</option>"
  response.write "  <option value='No Show'" & selected_status("No Show") & ">No Show</option>"
  response.write "  <option value='Cancelled'" & selected_status("Cancelled") & ">Cancelled</option>"
  response.write "</select>"
  response.write "</td>"
end if

response.write "<tr><td><b>Client:</b></td>"
response.write "<td><select name='scheduleClientID'>" & vbCrLF
response.write "<option value='0'>None</option>"

   usertype_id = 3
   delflag_id = 0
   MySQL = "SELECT * FROM users WHERE (usertype = " & usertype_id & ") and (delflag = " & delflag_id & ") ORDER BY ulname, ufname"
   ''response.write MySQL
   ''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
   field_acctno   = true
   field_name     = true
   field_dob      = true
   field_phone    = true
   field_location = true

   intClientID = 0
   Set MyRS = Server.CreateObject("ADODB.Recordset") 
   MyRS.Open MySQL, MyConn

   if MyRS.EOF then
     response.write ""
   end if
  
   Do While Not MyRS.EOF

     ''Start Client List Database Mapping
     client_id            = MyRS("uid")
     client_firstname     = MyRS("ufname")
     client_middleinitial = MyRS("uminitial")
     client_lastname      = MyRS("ulname")
     client_address       = MyRS("upaddress")
     client_city          = MyRS("upcity")
     client_state         = MyRS("upstate")
     client_postalcode    = MyRS("zipcode")
     client_phonenumber   = MyRS("upPhone")
     client_dateofbirth   = MyRS("dob")
     client_gender        = MyRS("sex")
     client_ssn           = MyRS("ssn")
     ''End Client List Database Mapping

     response.write "<option value='" & client_id
     if right("00000" & schedule_clientID, 5) = right("00000" & client_id, 5) then response.write "' selected='selected"
     response.write "'>" & client_lastname & ", " & client_firstname & " " & client_middleinitial & "</option>" & vbCrLf

   MyRS.MoveNext 
   Loop 

   MyRS.Close
   set MyRS = Nothing
response.write "</select>"
response.write "</td>"

response.write "<td align=right NOWRAP><b>Times: </b>"
response.write "<Select name=scheduleStartTime>"
if schedule_starttime = "" then
  response.write Replace(mDateList, "0800", "0800 Selected")
else
  response.write Replace(mDateList, schedule_starttime, schedule_starttime & " Selected")
end if
response.write "</select> to "
response.write "<select name=scheduleEndTime>"
if schedule_endtime = "" then
  response.write Replace(mDateList, "1700", "1700 Selected")
else
  response.write Replace(mDateList, schedule_endtime, schedule_endtime & " Selected")
end if
response.write "</select></td></tr>"
response.write "<tr><td colspan=3 align=left><b>Notes:</b><br />"
response.write "<textarea name=scheduleNotes cols=60 rows=10>"
if schedule_procedure <> "" then response.write schedule_procedure
response.write "</textarea></td></tr>"
response.write "</table></td></tr>"
response.write "<tr><td valign=top align=middle><input type=button value='Cancel' onClick='CancelMe()'> "
if schedule_id = "" then
  response.write "<input type='hidden' name='scheduletask' value='add'>"
  response.write "<input type='hidden' name='schedulestatus' value=''>"
  response.write "<input type='button' value='Add to Schedule' onClick='SubmitMe()'></td></tr></table>"
else
  if request.querystring("task") = "delete" then
    response.write "<input type='hidden' name='scheduletask' value='delete'>"
    response.write "<input type='hidden' name='scheduleid' value='" & schedule_id & "'>"
    response.write "<input type='button' value='Delete' onClick='UpdateMe()'></td></tr></table>"
  else
    if request.querystring("task") = "modify" then  
      response.write "<input type='hidden' name='scheduletask' value='modify'>"
      response.write "<input type='hidden' name='scheduleid' value='" & schedule_id & "'>"
      response.write "<input type='button' value='Save Changes' onClick='UpdateMe()'></td></tr></table>"
    end if
  end if
end if
%>