<!-- Start Schedule List -->
<%
if (request.form("scheduleadd") <> "Add") and (request.form("schedulesearch") = "Search" or request.form("schedule") <> "") then

   search_id = request.form("schedule")
   usertype_id = 5
   MySQL = "SELECT * FROM schEvents WHERE scheduleclient like '%" & search_id & "%' ORDER BY scheduleYear, scheduleMonth, scheduleDay, scheduleStartTime"
   ''response.write MySQL
   field_date      = true
   field_time      = true
   field_acctno    = true
   field_name      = true
   field_dob       = true
   field_phone     = true
   field_doctor    = true
   field_procedure = true

   intScheduleID = 0
   MyConn = DB_SCHstring
   Set MyRS = Server.CreateObject("ADODB.Recordset") 
   MyRS.Open MySQL, MyConn

   if MyRS.EOF then
     response.write "No Matches"
     response.end
   end if
  
   response.write "<table>" & vbCrLf
   response.write "  <tr>" & vbCrLf
   if field_date      then response.write "    <td><b>Date</b></td>" & vbCrLf
   if field_time      then response.write "    <td><b>Time</b></td>" & vbCrLf
   if field_acctno    then response.write "    <td class='smaller'><b>AcctNo</b></td>" & vbCrLf
   if field_name      then response.write "    <td width='100'><b>Name</b></td>" & vbCrLf
   if field_dob       then response.write "    <td width='100'><b>DOB</b></td>" & vbCrLf
   if field_phone     then response.write "    <td width='100'><b>Phone</b></td>" & vbCrLf
   if field_doctor    then response.write "    <td width='100'><b>Doctor</b></td>" & vbCrLf
   if field_procedure then response.write "    <td width='100'><b>Procedure</b></td>" & vbCrLf
   response.write "<td> </td>" & vbCrLF
   response.write "  </tr>" & vbCrLf

   Do While Not MyRS.EOF

     ''Start Schedule List Database Mapping
     schedule_id            = MyRS("scheduleid")
     schedule_clientID      = MyRS("scheduleclientID")
     schedule_client        = MyRS("scheduleclient")
     schedule_doctorID      = MyRS("scheduleemployeeID")
     schedule_doctor        = MyRS("scheduleemployee")
     schedule_date          = MyRS("scheduleMonth") & "/" & MyRS("scheduleDay") & "/" & MyRS("scheduleYear")
     if MyRS("scheduleStartTime") < 1200 then
       scheduleStartTime = left(MyRS("scheduleStartTime"),2) & ":" & right(MyRS("scheduleStartTime"),2) & "am"
     else
       if left(MyRS("scheduleStartTime"),2) = 12 then
         scheduleStartTime = left(MyRS("scheduleStartTime"),2) & ":" & right(MyRS("scheduleStartTime"),2) & "pm"
       else
         scheduleStartTime = right("00" & cint(left(MyRS("scheduleStartTime"),2)-12),2) & ":" & right(MyRS("scheduleStartTime"),2) & "pm"
       end if
     end if
     if MyRS("scheduleEndTime") < 1200 then
       scheduleEndTime = left(MyRS("scheduleEndTime"),2) & ":" & right(MyRS("scheduleEndTime"),2) & "am"
     else
       if left(MyRS("scheduleEndTime"),2) = 12 then
         scheduleEndTime = left(MyRS("scheduleEndTime"),2) & ":" & right(MyRS("scheduleEndTime"),2) & "pm"
       else
         scheduleEndTime = right("00" & cint(left(MyRS("scheduleEndTime"),2)-12),2) & ":" & right(MyRS("scheduleEndTime"),2) & "pm"
       end if
     end if
     schedule_time          = scheduleStartTime & "-" & scheduleEndTime
     schedule_phonenumber   = MyRS("schedulephone")
     schedule_dateofbirth   = MyRS("scheduledob")
     schedule_procedure     = MyRS("schedulenotes")
     ''End Schedule List Database Mapping

     intScheduleID = intScheduleID + 1
     response.write "  <tr>" & vbCrLf
     if field_date      then response.write "    <td>" & schedule_date & "</td>" & vbCrLf
     if field_time      then response.write "    <td>" & schedule_time & "</td>" & vbCrLf
     if field_acctno    then response.write "    <td  class='smaller'><a href='schedule.asp?pid=" & schedule_clientID & "'>" & schedule_clientID & "</a></td>" & vbCrLf
     if field_name      then response.write "    <td>" & schedule_client & "</td>" & vbCrLf
     if field_dob       then response.write "    <td>" & schedule_dateofbirth &  "</td>" & vbCrLf
     if field_phone     then response.write "    <td>" & schedule_phonenumber & "</td>" & vbCrLf
     if field_doctor    then response.write "    <td>" & schedule_doctor & "</td>" & vbCrLf
     if field_procedure then response.write "    <td>" & schedule_procedure & "</td>" & vbCrLf

     response.write "<td> </td>" & vbCrLF
     response.write "  </tr>" & vbCrLf

   MyRS.MoveNext 
   Loop 

   response.write "  <tr>" & vbCrLf
   response.write "    <td>" & right("00000" & intScheduleID, 5) & "</td>" & vbCrLf
   response.write "    <td colspan='2'>Scheduled Items Found</td>" & vbCrLf
   response.write "  </tr>" & vbCrLf
   response.write "</table>" & vbCrLf

   if intScheduleID < 1 then
     intScheduleID = 1
   else
     intScheduleID = intScheduleID + 1
   end if

   ''response.write "Current Application #: " & intScheduleID

   MyRS.Close
   set MyRS = Nothing
end if

if ((request.form("scheduletoday") = "Today") or (request.querystring("scheduledate") <> "")) then
    ReDim doctor_header(3)
    ReDim doctor_id(3)
    doctor_header(1) = "Sullivan, MD"
    doctor_id(1)     = 18772
    doctor_header(2) = "Goode, MD"
    doctor_id(2)     = 18771
    doctor_header(3) = "Scott, PA"
    doctor_id(3)     = 23245

    doctor_number = 3

%>
<!--#include file="schedule_displaydate.inc"-->
<%
end if

if ((request.form("scheduleadd") = "Add") or (request.querystring("starttime") <> "") or (request.querystring("task") = "modify") or (request.querystring("task") = "delete")) then
   response.write "<table>" & vbCrLf
   response.write "  <tr>" & vbCrLf
   response.write "    <td>" & vbCrLf
%>
<!--#include file="schedule_newevent.asp"-->
<% 
   response.write "    </td>" & vbCrLf
   response.write "  </tr>" & vbCrLf
   response.write "</table>" & vbCrLf
end if
%>
<!-- End Schedule List -->