<!--#include file="adovbs.inc"-->
<!--#include file="db.asp"-->
<!--#include file="func_activity.inc"-->
<!--#include file="header.inc"-->

<!-- Start Doctor Add -->
<%
''response.write request.form
''response.end
if request.form("doctorsubmit") = "Delete" then

    ''Doctor Delete Logging
    doctor_id                          = Request.Form("doctor_id")    
	doctor_firstname                   = Request.Form("doctor_firstname")
	doctor_middleinitial               = Request.Form("doctor_middleinitial")
	doctor_lastname                    = Request.Form("doctor_lastname")
    description = "Doctor delete #" & doctor_id & ": " & doctor_lastname & ", " & doctor_firstname
    test = activity(session("username"),description)

    ''MySQL = "DELETE FROM users WHERE uid = " & request.form("doctor_id")
    MySQL = "SELECT * FROM users WHERE uid = " & request.form("doctor_id")
    ''response.write MySQL
    ''delflag = 1 means record has been deleted
    
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

    if Not MyRS.EOF then
      MyRS("delflag") = 1
      MyRS.Update
    end if

    MyRS.close
    set MyRS = nothing

    response.write "Deleted<br />"

else

if request.form("doctorsubmit") = "Cancel" then response.redirect "doctor.asp"

if request.form("doctorsubmit") = "Submit" or request.form("doctorsubmit") = "Update" then  
Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

'displays header on form field validation pages
function displayHeader()
	Response.Write "<html>"
	Response.Write "<head>"
	Response.Write "<body>"
	Response.Write "<p align='center'>"
	Response.Write "<div align = 'center'>"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.Write "<font size='4' color=red face='arial'><strong>Error</strong><br /></p>"
end function

'Validate user entries in form fields and generate error pages
For Each key in Request.Form
		strName = key
		strValue = Request.Form(key)
		Session(strName) = strValue
	Next

    doctor_id                          = Request.Form("doctor_id")    
	doctor_firstname                   = Request.Form("doctor_firstname")
	doctor_middleinitial               = Request.Form("doctor_middleinitial")
	doctor_lastname                    = Request.Form("doctor_lastname")
	doctor_address                     = Request.Form("doctor_address")
	doctor_city                        = Request.Form("doctor_city")
	doctor_state                       = Request.Form("doctor_state")
	doctor_postalcode                  = Request.Form("doctor_postalcode")
	doctor_phonenumber                 = Request.Form("doctor_phonenumber")
	doctor_phonefax                    = Request.Form("doctor_phonefax")

If doctor_firstname = "" OR len(doctor_firstname) <=2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "Please enter the first name." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_firstname) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The first name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_middleinitial) > 1 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The middle initial is too long.  Maximum length is 1 character." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If doctor_lastname = "" OR len(doctor_lastname) <=2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "Please enter the last name." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_lastname) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The last name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_address) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The address is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_city) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The city or town is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_state) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The state or province is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_postalCode) > 10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The Zip/Postal code is too long.  Maximum length is 10 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_phoneNumber) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The phone number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(doctor_phoneFax) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The doctor fax number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if


    MySQL = "SELECT * FROM users"

    intDoctorsID = 0
    intDoctorsHighRecord = 0
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn

    Do While Not MyRS.EOF
      intDoctorsID = intDoctorsID + 1
      if intDoctorsHighRecord < MyRS("uid") then intDoctorsHighRecord = MyRS("uid")

    MyRS.MoveNext 
    Loop 

    if intDoctorsID < 1 then
      intDoctorsID = 1
      intDoctorsHighRecord = 1
    else
      intDoctorsID = intDoctorsID + 1
      intDoctorsHighRecord = intDoctorsHighRecord + 1
    end if

    ''response.write "Current Doctor #: " & intDoctorsID & " High Record #: " & intDoctorsHighRecord

    MyRS.Close
    set MyRS = Nothing
    if request.form("doctorsubmit") = "Submit" then
      ''MySQL = "SELECT * FROM users"
      MySQL = "SELECT users.*, doctors.* FROM users INNER JOIN doctors ON users.uid = doctors.doctorID "
    else 
      ''MySQL = "SELECT * FROM users WHERE uid = " & doctor_id
      MySQL = "SELECT users.uid, users.ufname, users.uminitial, users.ulname, users.upaddress, users.upcity, users.upstate, users.zipcode, "
      MySQL = MySQL & "users.upPhone, doctors.FaxNo FROM users INNER JOIN doctors ON users.uid = doctors.doctorID "
      MySQL = MySQL & "WHERE users.uid=" & doctor_id
    end if
    ''response.write MySQL

    Set rsDoctor = Server.CreateObject("ADODB.Recordset") 
    rsDoctor.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

      if request.form("doctorsubmit") = "Submit" then
        rsDoctor.AddNew
        rsDoctor("uid")          = intDoctorsHighRecord
        rsDoctor("doctorid")     = intDoctorsHighRecord
        rsDoctor("cdate")        = Date & " " & Time
        rsDoctor("usertype")     = 5
        rsDoctor("delflag")      = 0
        ''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
        ''Delflag: 0-Active, 1-Deleted

        ''Doctor Add Logging
        description = "Doctor add #"
      end if

      rsDoctor("ufname")         = doctor_firstname
      if doctor_middleinitial <> "" then rsDoctor("uminitial")      = doctor_middleinitial
      rsDoctor("ulname")         = doctor_lastname
      rsDoctor("upaddress")      = doctor_address
      rsDoctor("upcity")         = doctor_city
      rsDoctor("upstate")        = doctor_state
      rsDoctor("zipcode")        = doctor_postalCode
      rsDoctor("upphone")        = doctor_phoneNumber
      rsDoctor("faxno")          = doctor_phoneFax
    rsDoctor.Update

    rsDoctor.close
    set rsDoctor = nothing

    set MyConn = Nothing

    ''Doctor Add/Modify Logging
    if description = "Doctor add #" then 
      description = "Doctor add #" & intDoctorsHighRecord & ": " & doctor_lastname & ", " & doctor_firstname
    else
      description = "Doctor modify #" & Request.Form("doctor_id") & ": " & doctor_lastname & ", " & doctor_firstname
    end if
    test = activity(session("username"),description)
    description = ""
end if

response.write "Update Successful<br />"

end if ''Delete Doctor IF STATEMENT
if  request.form("doctoradd") <> "Add" then
    response.write "<form action='doctor.asp' method=POST>"
    response.write "Doctor Search<br />"
    response.write "<input type='text' name='doctor' size='30' value=''>&nbsp;&nbsp;"
    response.write "<input type='submit' name='doctorsearch' value='Search'><input type='submit' name='doctoradd' value='Add'><br /></form>"
end if
%>

<!-- End Doctor Add -->

<!--#include file="footer.inc"-->