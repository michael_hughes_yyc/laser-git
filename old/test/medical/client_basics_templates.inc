
            <!-- Start Patient Basics Template -->
            <% 
              varMonth = right("00" & left(date, inStr(date, "/")-1), 2)
              varDay   = right("00" & left(right(date, (len(date) - inStr(date, "/"))), inStr(right(date, (len(date) - inStr(date, "/"))), "/")-1), 2)
              varYear  = right("0000" & date,4)
              varDate  = varMonth & "/" & varDay & "/" & varYear
             %>

            <form action="client_add_templates.asp" method="post">
            <input type="hidden" name="client_id" value="<%= client_id %>">
            <input type="hidden" name="client_lastname" value="<%= client_lastname %>">
            <input type="hidden" name="client_firstname" value="<%= client_firstname %>">
            <input type="hidden" name="client_middleinitial" value="<%= client_middleinitial %>">
            <input type="hidden" name="client_physicianNameRend" value="<%= client_physicianNameRend %>">
            <input type="hidden" name="client_placeofservice" value="<%= client_placeofservice %>">
            <input type="hidden" name="client_physicianNameRef" value="<%= client_physicianNameRef %>">
            <input type="hidden" name="client_insuranceName" value="<%= client_insuranceName %>">
            <input type="hidden" name="client_varDate" value="<%= varDate %>">
            <input type="hidden" name="client_templatename" value="<%= request.querystring("sub") %>">

            <font face="arial">
            <table width="100%" border="0">
              <tr>
                <td colspan="3" class="subheading"><strong>Report Template</strong></td>
              </tr>
			  <tr>
                <td width="30%"><font size="3">Patient: <%= client_lastname %>, <%= client_firstname %>&nbsp;<%= client_middleinitial %></td>
                <td width="5%"> </td>
                <td width="35%" align="right"><input type='submit' name='templateadd' value='Add To Medical Record'></td>
              </tr>
              <tr>
                <td width="30%"><font size="3">Provider: <%= client_physicianNameRend %></td>
              </tr>
              <tr>
                <td width="30%"><font size="3">Primary Insurance: <%= client_insuranceName %></td>
                <td width="5%"> </td>
                <td width="35%"><font size="3">Encounter Date: <%= varDate %>
              </tr>
              <tr>
                <td width="30%"><font size="3">Place of Service: Carson Douglas Pain Care</td>
              </tr>
              <tr>
                <td width="30%"><font size="3">Surgeon: <%= client_physicianNameRef %></td>
                <td width="5%"> </td>
                <td width="30%"><font size="3">Assistant: </td>
              </tr>
            </table>
            <!-- End Patient Basics Template -->
