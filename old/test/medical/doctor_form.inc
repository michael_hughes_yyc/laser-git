
<!-- Start Doctor Form -->
<form action="doctor_add.asp" method="post">
<table width="100%">
  <tr>
    <td width="2%"> </td>
    <td width="96%"> 
            <table width="100%">
            <tr>
            <td colspan="2"  class="subhead"><strong>Doctor Information</strong></td>
			</tr>
			<tr>
                <td>Name: (ID:<%= right("00000" & doctor_id, 5) %>)</td>
                <td>
                <input type="text" name="doctor_firstname" size="25" value="<%= doctor_firstname %>">
                <input type="text" name="doctor_middleinitial" size="5" value="<%= doctor_middleinitial %>">
                <input type="text" name="doctor_lastname" size="25" value="<%= doctor_lastname %>">
                </td>
            </tr>
            <tr>
                <td>Address: </td>
                <td>
                <input type="text" name="doctor_address" size="25" value="<%= doctor_address %>">
                </td>
            </tr>
            <tr>
                <td>City, State/Province, Zip/Postal Code: </td>
                <td>
                <input type="text" name="doctor_city" size="25" value="<%= doctor_city %>">
                <input type="text" name="doctor_state" size="5" value="<%= doctor_state %>">
                <input type="text" name="doctor_postalCode" size="5" value="<%= doctor_postalCode %>">
                </td>
            </tr>
            <tr>
                <td>Telephone: </td>
                <td>
                <input type="text" name="doctor_phoneNumber" size="25" value="<%= doctor_phoneNumber %>">
                </td>
            </tr>
            <tr>
                <td>Fax: </td>
                <td>
                <input type="text" name="doctor_phonefax" size="25" value="<%= doctor_phonefax %>">
                </td>
            </tr>
            <tr>
              <td colspan="2"><hr color="#CCCCCC" size="1" noshade></td>
            </tr>
            <tr>
                <td colspan="2" class="subheading"><strong>&nbsp;</strong></td>
            </tr>
            <tr>
              <td colspan="2"><br />
                <input type="hidden" name="doctor_id" value="<%= doctor_id %>">
                <center>
                  <%
                  if doctor_id = "" then
                    response.write "<input type='submit' name='doctorsubmit' value='Submit'>" & vbCrLF
                    response.write "<input type='submit' name='doctorsubmit' value='Cancel'>" & vbCrLF
                  else
                    response.write "<input type='submit' name='doctorsubmit' value='Update'>" & vbCrLF
                    response.write "<input type='submit' name='doctorsubmit' value='Cancel'>" & vbCrLF
                    response.write "<input type='submit' name='doctorsubmit' value='Delete'>" & vbCrLF
                  end if
                  %>
                </center></form>
              </td>
            </tr>
          </table>
    </td>
    <td width="2%"> </td>
  </tr>
</table>

<!-- End Doctor Form -->
