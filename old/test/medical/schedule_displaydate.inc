<%
'''''''''''''
' Constants '
'''''''''''''
Const firstHour = 9		'33
Const lastHour = 18		'68

numHour = lastHour - firstHour + 1



'''''''''''''
' Functions '
'''''''''''''
Function BuildHtmlContent (rs, today, year, month, day)

      schedule_id            = rs("scheduleid")
      schedule_clientID      = rs("scheduleclientID")
      schedule_client        = rs("scheduleclient")
      schedule_phonenumber   = rs("schedulephone")
      schedule_dateofbirth   = rs("scheduledob")
      schedule_procedure     = rs("schedulenotes")

      result = "<a href='schedule.asp?pid=" & schedule_clientID & "&scheduleid=" & schedule_id 
      result = result & "&scheduledate="& Server.URLEncode(today) & "&year=" & year & "&month=" & month & "&day=" & day
      result = result & "'>" & schedule_client & "</a>: "

    	result = result & "DOB:" & schedule_dateofbirth &  " "
      
      result = result & "Tel:" & schedule_phonenumber & " "
      result = result & schedule_procedure
      
      BuildHtmlContent = result

End Function


Function BuildTextContent (rs)

      schedule_client        = rs("scheduleclient")
      schedule_phonenumber   = rs("schedulephone")
      schedule_dateofbirth   = rs("scheduledob")
      schedule_procedure     = rs("schedulenotes")

      result = schedule_client & ": "

    	result = result & "DOB:" & schedule_dateofbirth &  " "
      
      result = result & "Tel:" & schedule_phonenumber & " "
      result = result & schedule_procedure
      
      BuildTextContent = result

End Function


Function BuildDateString (year, month, day)

	d = CDate(day & " " & MonthName(month) & " " & year)
	s = WeekdayName(Weekday(d, vbMonday), False, vbMonday) & ", " & MonthName(month) & " " & day & ", " & year
	BuildDateString = s

End Function





''''''''''''''''
' Getting data '
''''''''''''''''

' Declarations
Dim rows(3, 196)

ReDim appoContent(3, 284)
ReDim appoAlt(3, 284)
Dim appoStart(3, 284)
Dim appoDuration(3, 284)
Dim appoType(3, 284)
Dim appoStatus(3, 284)

Dim appoPos(3, 284)
Dim actRows(3, 196)

numAppo = 0


' Preparing parameters
If request.querystring("scheduledate") = "" Then
  dispDate = right("00" & Month(Now),2) & "/" & right("00" & Day(Now), 2) & "/" & right("0000" & Year(Now), 4)
  dispYear  = right("0000" & Year(Now), 4)
  dispMonth = right("00" & Month(Now),2)
  dispDay   = right("00" & Day(Now), 2)
Else
  dispDate = right("00" & request.querystring("month"),2) & "/" & right("00" & request.querystring("day"),2) & "/" & right("0000" & request.querystring("year"), 4) 
  dispYear  = right("0000" & request.querystring("year"), 4)
  dispMonth = right("00" & request.querystring("month"),2)
  dispDay   = right("00" & request.querystring("day"),2)
End If

//doctId = 18772		' Now forced


' Accessing db
Set dbConn = Server.CreateObject("ADODB.Connection")
dbConn.ConnectionTimeout = 15
dbConn = DB_SCHstring

For k = 1 To 3
	Set dbRs = Server.CreateObject("ADODB.Recordset") 
	
	dbQuery= "select * from SchEvents where scheduleEmployeeID = '" & doctor_id(k) & "' and scheduleYear = '" & dispYear & "' and scheduleMonth = '" & dispMonth & "' and scheduleDay = '" & dispDay & "' order by scheduleStartTime asc"
	'dbQuery= "select * from SchEvents where scheduleYear = '" & dispYear & "' and scheduleMonth = '" & dispMonth & "' and scheduleDay = '" & dispDay & "' order by scheduleStartTime asc"
	dbRs.Open dbQuery, dbConn
	
	Do While Not dbRs.EOF
	      numAppo = numAppo + 1
	
	      appoContent (k, numAppo) 	= BuildHtmlContent (dbRs, dToday, dispYear, dispMonth, dispDay)
	      appoAlt (k, numAppo) 			= BuildTextContent (dbRs)
				sBeg									 	= dbRs("scheduleStartTime")
				sEnd 										= dbRs("scheduleEndTime")
				appoType (k, numAppo) 			= dbRs("scheduletype")
				appoStatus(k, numAppo) 		= dbRs("schedulestatus")
				'doctorName							= dbRs("scheduleemployee")
				
				hBeg = Left(sBeg, 2)
				mBeg = Right(sBeg, 2)
				hEnd = Left(sEnd, 2)
				mEnd = Right(sEnd, 2)
				
				appoStart (k, numAppo) = hBeg * 4 + (mBeg \ 15) + 1
				appoDuration (k, numAppo) = (hEnd - hBeg) * 4 + (mEnd \ 15) - (mBeg \ 15)
				
				For i = 0 To appoDuration(k, numAppo) - 1
					rows(k, appoStart(k, numAppo) + i) = rows(k, appoStart(k, numAppo) + i) + 1
				Next
	
				dbRs.MoveNext 
	      
	Loop
	
	dbRs.Close
	
	
	
	'''''''''''''''''''''''''
	' Calculating positions '
	'''''''''''''''''''''''''
	For i = 1 To numAppo
	
		actMax = 0
		For j = 0 To appoDuration (k, i)
			If (actRows(k, appoStart(k, i) + j) + 1) > actMax Then
				actMax = actRows(k, appoStart(k, i) + j) + 1
			End If
			actRows(k, appoStart(k, i) + j) = actRows(k, appoStart(k, i) + j) + 1
		Next
		
		appoPos(k, i) = actMax
	Next

Next

Function SlotToHtml (row, height, left, width, color, border, content, alt)

	div = "<div class='slot-box' style='left: " & left & "px; top: " & ((row-1)*19) & "px; height: " & ((height*19)-2) & "px; width: " & width & "px; background-color: " & color & "' title='" & alt & "'>" & vbCrLf
	div = div & "<div class='slot-left-bar' style='background-color: " & border & ";'>" & vbCrLf
	div = div & "</div>" & vbCrLf
	div = div & content & "</div>" & vbCrLf

	SlotToHtml = div

End Function
%>

<style>
	div.slot-line
	{
		position: absolute;
		left: 0px;
		width: 326px;
		height: 18px;
		border-top: solid 1px #8e817b;				
	}

	div.timeline-hour
	{
		position: absolute;
		left: 2px;
		width: 32px;
		height: 70px;
		padding-top: 5px;
		padding-right: 24px;
		border-top: solid 1px #848285;

		text-align: right;
		font-family: Tahoma;
		font-size: 20px;
		font-weight: normal;
	}
		
	div.timeline-quarter
	{
		position: absolute;
		right: 2px;
		width: 20px;
		height: 18px;
		pading-left: 3px;
		border-bottom: solid 1px #848285;
				
		text-align: left;
		font-family: Tahoma;
		font-size: 12px;
		font-weight: normal;
	}

	div.slot-box
	{
		position: absolute;
		overflow: hidden;
		border: solid 1px black;
				
		text-align: center;
		font-size: 10px;
	}
	
	div.slot-left-bar
	{
		float: left;
		width: 6px;
		height: 100%;
		border-right: solid 1px black;
	}
</style>
<div style='position: absolute;'>
	<div style='position: absolute; left: 0px; top: 0px; width: 58px; height: 28px; border-top: solid 1px black; border-left: solid 1px black; padding-top: 2px; padding-left: 2px; background-color: #d6d3ce; font-style: Tahoma; font-size: 16pt;'>
	</div>
	<div style='position: absolute; left: 61px; top: 0px; width: 390px; height: 28px; border-top: solid 1px black; padding-top: 2px; background-color: #d6d3ce; font-style: Tahoma; font-size: 16pt; text-align: center'>
		<%= doctor_header(1) %>
	</div>
	<div style='position: absolute; left: 451px; top: 0px; width: 390px; height: 28px; border-top: solid 1px black; padding-top: 2px; background-color: #d6d3ce; font-style: Tahoma; font-size: 16pt; text-align: center'>
		<%= doctor_header(2) %>
	</div>
	<div style='position: absolute; left: 841px; top: 0px; width: 390px; height: 28px; border-top: solid 1px black; border-right: solid 1px black; padding-top: 2px; background-color: #d6d3ce; font-style: Tahoma; font-size: 16pt; text-align: center'>
		<%= doctor_header(3) %>
	</div>
	<div style='position: absolute; left: 0px; top: 30px; width: 1230px; height: 30px; border-left: solid 1px black; border-right: solid 1px black; background-color: #d6d3ce;'>
		<div style='position: absolute; left: 0px; top: 0px; width: 16px; height: 26px; border: outset white 2px;'>
		</div>
		<div style='position: absolute; left: 20px; top: 0px; width: 1186px; height: 26px; border: outset white 2px; text-align: center; font-style: Tahoma; font-size: 13px;'>
			<span style='line-height: 26px;'><%= BuildDateString (dispYear, dispMonth, dispDay) %></span>
		</div>
		<div style='position: absolute; left: 1210px; top: 0px; width: 16px; height: 26px; border: outset white 2px;'>
		</div>
	</div>
	<div style='position: absolute; left: 0px; top: 60px; width: 1230px; height: <%= numHour*76 %>px; overflow: hidden; border-bottom: solid 1px black; border-left: solid 1px black; border-right: solid 1px black; background-color: red;'>
		<div style='position: absolute; left: 0px; top: 0px; width: 60px; height: <%= numHour*76 %>px; background-color: #c5c3c6;'>
<% for i = firstHour-1 to lastHour-1 %>

			<div class='timeline-hour' style='top: <%= (i-firstHour+1)*76 %>px;'>
	<%= ((i+11) mod 12) + 1 %>
				<div class='timeline-quarter' style='top: 0px;'>
<% if ((i mod 12) = 0) Or (i = firstHour-1) Or (i = lastHour-1) then
     if (i < 12) then
%>
		am
<%    else
%>
		pm
<%
      end if
   else
%>
		00
<%
   end if
%>
				</div>
				<div class='timeline-quarter' style='top: 19px;'></div>
				<div class='timeline-quarter' style='top: 38px;'></div>
			</div>
<% next %>
		</div>
<%		
	For j = 1 To 3
%>		
		<div style='position: absolute; left: <%= 60 + (j-1)*390 %>px; top: 0px; width: 60px; height: <%= numHour*76-4 %>px; border: solid 2px black; background-color: #cdcf64;'>
			<div style='float: left; width: 5px; height: <%= numHour*76-4 %>px; border-right: solid 1px black; background-color: white;'>
			</div>
		</div>
		<div style='position: absolute; left: <%= 124 + (j-1)*390 %>px; top: 0px; width: 326px; height: <%= numHour*76 %>px; background-color: #ffff95; overflow: hidden;'>
<% for i = 0 to numHour*4-1 %>
			<div class='slot-line' style='top: <%= i*19 %>px;'>
			</div>
<% next

''''''''''''''''
' Drawing divs '
''''''''''''''''
For i = 1 To numAppo
	Select Case actRows(j, appoStart(j, i))
		Case 1
			bWidth = 304
			bLeft = 10

		Case 2
			bWidth = 146
			bLeft = 10 + (appoPos(j, i) - 1) * 158

		Case 3
			bWidth = 94
			bLeft = 10 + (appoPos(j, i) - 1) * 106

		Case Else
			bWidth = 67
			bLeft = 10 + (appoPos(j, i) - 1) * 79

	End Select
	
	
	Select Case appoType(j, i)
		Case "Office Visit"
			bColor = "green"

		Case "Procedure"
			bColor = "pink"

		Case "New Patient"
			bColor = "blue"

		Case "Pump"
			bColor = "purple"

		Case "Blocked Out"
			bColor = "yellow"

	End Select
	

	Select Case appoStatus(j, i)
		Case "Confirmed"
			bBorder = "darkgreen"

		Case "Left Message"
			bBorder = "yellow"

		Case "Rescheduled"
			bBorder = "red"

		Case "No Show"
			bBorder = "red"

		Case "Cancelled"
			bBorder = "red"

	End Select
%>	
	
	<%= SlotToHtml (appoStart(j, i) - ((firstHour-1)*4), appoDuration(j, i), bLeft, bWidth, bColor, "#049868", appoContent(j, i), appoAlt(j, i)) %>

<%
Next
%>
		</div>
<%		
	Next
%>		
	</div>
</div>