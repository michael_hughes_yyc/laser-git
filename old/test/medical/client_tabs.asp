
      <!-- Start Client Tabs -->
      <table class="client-tabs">
          <tr>
          <td>
            <h2>Client Tabs</h2>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Quick View</strong>
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=info" >Info</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=insurance" >Insurance</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=authorization" >Authorization</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=misc" >Miscellaneous</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=medlog" >Medication Log Sheet</a>
            
          </td>
        </tr>
        <!-- End Client Tabs -->

        <!-- Start Attachments Tab -->
        <tr>
          <td>
            <br />
          </td>
        </tr>
        <tr>
          <td>
           <strong>Attachments</strong>
          </td>
        </tr>
        <tr>
          <td>
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=attachments&doc_Type=4">Medication Contract</a>
            
          </td>
        </tr>
        <tr>
          <td nowrap>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=attachments&sub=patientquestionaire" >Patient Questionaire Packet</a>
            
          </td>
        </tr>
        <tr>
          <td>
           
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=attachments&sub=dictation" >Dictation</a>
            
          </td>
        </tr>
        <tr>
          <td>
           
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=attachments&sub=lab" >Lab</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=attachments&sub=lab" >Radiology</a>
            
          </td>
        </tr>
        <tr>
          <td>
           
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=attachments&sub=lab" >All Attachments</a>
            
          </td>
        </tr>
        <!-- End Attachments Tab -->

        <%
        if session("username") = "admin" or session("username") = "Goode, Randall" then
        %>
        <!-- Start Templates Tab -->
        <tr>
          <td>
            <br />
          </td>
        </tr>
        <tr>
          <td >
           <strong>Templates</strong>
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_Cervical_MBB" >RG-Cervical MBB</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_IESI" >RG-IESI</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_Interlam_TFESI" >RG-Interlam + TFESI</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_Lumbar_Bil_MBB" >RG-Lumbar Bil MBB</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_LumbRF" >RG-LumbRF</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_MBB" >RG-MBB</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_Procedure" >RG-Procedure</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_SCS" >RG-SCS</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_Stellate" >RG-Stellate</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=rgoode_TFESI" >RG-TFESI</a>
            
          </td>
        </tr>
      
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=_rgoode_CESI" >NEW_RG-CESI</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=_rgoode_CSCFJS" >NEW_RG-CSCFJS</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=_rgoode_Hip" >NEW_RG-Hip</a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=_rgoode_LateralFemoral" >NEW_RG-LateralFemoral<b></b></a>
            
          </td>
        </tr>
        <tr>
          <td>
            
            <a href="client.asp?pid=<%= request.querystring("pid") %>&form=templates&sub=_rgoode_SIjoint" >NEW_RG-SIjoint</a>
            
          </td>
        </tr>
        <%
        end if
        %>
      </table>
      <!-- End Templates Tab -->
