
<!-- Start Calendar -->
<%
	'------------------------------------------------------------
	' This function finds the last date of the given month
	'------------------------------------------------------------
	Function GetLastDay(intMonthNum, intYearNum)
		Dim dNextStart
		If CInt(intMonthNum) = 12 Then
			dNextStart = CDate( "1/1/" & intYearNum)
		Else
			dNextStart = CDate(intMonthNum + 1 & "/1/" & intYearNum)
		End If
		GetLastDay = Day(dNextStart - 1)
	End Function
	
	'-------------------------------------------------------------------------
	' This routine prints the individual table divisions for days of the month
	'-------------------------------------------------------------------------
	Sub Write_TD(sValue, sClass)
		Response.Write "		<TD align='RIGHT' WIDTH=20 HEIGHT=15 Valign='BOTTOM' CLASS='" & sClass & "'> " & sValue & "</TD>" & vbCrLf
	End Sub


	' Constants for the days of the week
	Const cSUN = 1, cMON = 2, cTUE = 3, cWED = 4, cTHU = 5, cFRI = 6, cSAT = 7
	
	' Get the name of this file
	sScript = Request.ServerVariables("SCRIPT_NAME")
	
	' Check for valid month input
	If IsEmpty(Request("MONTH")) OR NOT IsNumeric(Request("MONTH")) Then
	  datToday = Date()
	  intThisMonth = Month(datToday)
	ElseIf CInt(Request("MONTH")) < 1 OR CInt(Request("MONTH")) > 12 Then
	  datToday = Date()
	  intThisMonth = Month(datToday)
	Else
	  intThisMonth = CInt(Request("MONTH"))
	End If
	
	' Check for valid year input
	If IsEmpty(Request("YEAR")) OR NOT IsNumeric(Request("YEAR")) Then
	  datToday = Date()
	  intThisYear = Year(datToday)
	Else
	  intThisYear = CInt(Request("YEAR"))
	End If

	strMonthName = MonthName(intThisMonth)
	datFirstDay = DateSerial(intThisYear, intThisMonth, 1)
	intFirstWeekDay = WeekDay(datFirstDay, vbSunday)
	intLastDay = GetLastDay(intThisMonth, intThisYear)
	
	' Get the previous month and year
	intPrevMonth = intThisMonth - 1
	If intPrevMonth = 0 Then
		intPrevMonth = 12
		intPrevYear = intThisYear - 1
	Else
		intPrevYear = intThisYear	
	End If
	
	' Get the next month and year
	intNextMonth = intThisMonth + 1
	If intNextMonth > 12 Then
		intNextMonth = 1
		intNextYear = intThisYear + 1
	Else
		intNextYear = intThisYear
	End If

	' Get the last day of previous month. Using this, find the sunday of
	' last week of last month
	LastMonthDate = GetLastDay(intLastMonth, intPrevYear) - intFirstWeekDay + 2
	NextMonthDate = 1

	' Initialize the print day to 1  
	intPrintDay = 1
	
	' These dates are used in the SQL
	dFirstDay = intThisMonth & "/1/" & intThisYear
	dLastDay 	= intThisMonth & "/" & intLastDay & "/" & intThisYear

%>

<style TYPE="text/css">
  TD.NON {font-family :Tahoma, Verdana, Arial; font-size :12px; color :#C0C0C0; font-weight :normal;}
  TD.TOP {font-family :Tahoma, Verdana, Arial; font-size :12px; color :#0000FF; font-weight :bold;}
  TD.Some {font-family :Tahoma, Verdana, Arial; font-size :12px; color :#000000; font-weight :normal;}

  A.NOEVENT:Link {font-family :Tahoma, Verdana, Arial; font-size :12px; color :#000000; font-weight :normal; text-decoration: none;}
  A.NOEVENT:Visited {font-family :Tahoma, Verdana, Arial; font-size :12px; color :#000000; font-weight :normal; text-decoration: none;}

  A.xEVENT:Link {font-family :Tahoma, Verdana, Arial; font-size :12px; color :#000000; font-weight :bold; text-decoration: none;}
  A.xEVENT:Visited {font-family :Tahoma, Verdana, Arial; font-size :12px; color :#000000; font-weight :bold; text-decoration: none;}

  A.xNORMAL:Link {font-family :Verdana, Arial; font-size :12px; color :#0000FF; font-weight :normal; text-decoration: none;}
  A.xNORMAL:Visited {font-family :Verdana, Arial; font-size :12px; color :#0000FF; font-weight :normal; text-decoration: none;}
</style>
<table BORDER="1" CELLSPACING="0" CELLPADDING="2"  BORDERCOLOR="Gray">
<tr><td>
	<table width="140" BORDER="0" CELLPADDING="1" CELLSPACING="0" BGCOLOR="#FFFFFF">
		<tr height="18" BGCOLOR="Silver">
			<td width="20" height="18" align="left" valign="middle"><a HREF="<% =sScript%>?month=<% =IntPrevMonth %>&amp;year=<% =IntPrevYear %>"><img SRC="data/prev.gif" width="10" height="18" BORDER="0" ALT="Previous Month"></a></td>
			<td width="120" colspan="5" align="center" valign="middle" class="some"><% = strMonthName & " " & intThisYear %></td>
			<td width="20" height="18" align="right" valign="middle"><a HREF="<% =sScript %>?month=<% =IntNextMonth %>&amp;year=<% =IntNextYear %>"><img SRC="data/next.gif" width="10" height="18" BORDER="0" ALT="Next Month"></a></td>
		</tr>
	  <tr>
			<td align="right" class="some" width="20" height="15" valign="bottom">S</td>
			<td align="right" class="some" width="20" height="15" valign="bottom">M</td>
			<td align="right" class="some" width="20" height="15" valign="bottom">T</td>
			<td align="right" class="some" width="20" height="15" valign="bottom">W</td>
			<td align="right" class="some" width="20" height="15" valign="bottom">T</td>
			<td align="right" class="some" width="20" height="15" valign="bottom">F</td>
			<td align="right" class="some" width="20" height="15" valign="bottom">S</td>
	  </tr>
	  <tr><td height="1" align="middle" colspan="7"><img SRC="data/line.gif" height="1" width="140" BORDER="0"></td></tr>
	  <%
			' Initialize the end of rows flag to false
			EndRows = False
			Response.Write vbCrLf
			
			' Loop until all the rows are exhausted
		 	Do While EndRows = False
				' Start a table row
				Response.Write "	<TR>" & vbCrLf
				' This is the loop for the days in the week
				For intLoopDay = cSUN To cSAT
					' If the first day is not sunday then print the last days of previous month in grayed font
					If intFirstWeekDay > cSUN Then
						Write_TD LastMonthDate, "NON"
						LastMonthDate = LastMonthDate + 1
						intFirstWeekDay = intFirstWeekDay - 1
					' The month starts on a sunday	
					Else
						' If the dates for the month are exhausted, start printing next month's dates
						' in grayed font
						If intPrintDay > intLastDay Then
							Write_TD NextMonthDate, "NON"
							NextMonthDate = NextMonthDate + 1
							EndRows = True 
						Else
							' If last day of the month, flag the end of the row
							If intPrintDay = intLastDay Then
								EndRows = True
							End If
							
							dToday = CDate(intThisMonth & "/" & intPrintDay & "/" & intThisYear)
                            if test = "123" then
								' Set events flag to false. This means the day has no event in it
								bEvents = False	
							End If
							
							' If the event flag is not raise for that day, print it in a plain font
							If bEvents = False Then
								Write_TD "<A HREF=schedule.asp?scheduledate="& Server.URLEncode(dToday) & "&year=" & intThisYear & "&month=" & intThisMonth & "&day=" & intPrintDay & " CLASS='NOEVENT'> " & intPrintDay & "</A>", "SOME"
							End If
						End If 
						
						' Increment the date. Done once in the loop.
						intPrintDay = intPrintDay + 1
					End If
				
				' Move to the next day in the week
				Next
				Response.Write "	</TR>" & vbCrLf
				
			Loop 
			''Rs.Close
			''Set Rs = Nothing
		%>
	</table>
	</td></tr>
</table>
<!-- End Calendar -->
