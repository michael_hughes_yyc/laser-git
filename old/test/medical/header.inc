<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Medical Office Suite</title>

<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="robots" content="index, follow" />

<meta http-equiv="Content-Type" content="text/html" />
<script src="data/rollover.js" type="text/javascript"></script>
<script src="data/scripts.js" type="text/javascript"></script>
 <script src="data/jquery-1.7.2.min.js" type="text/javascript"></script>
  <script type="text/javascript">

</script>
<link href="data/styles.css" rel="stylesheet" type="text/css" />
<link href="data/layout.css" rel="stylesheet" type="text/css" />

 <script>
 $(document).ready(function() {
 
    setMobileStyles();	 
    $("tr:even").addClass("even");  
    $("tr:odd").addClass("odd");
    fixMinWidthForIE();	 
    setSubmits();
});
 </script>
</head>
 
<body>
	<div class="top-tail">
	
	
    <div class="bot-tail">
    
    <div class="nav">
       <ul>
          <li class="home"><a href="index.asp" alt="home">Home</a></li>
          <li class="patient"><a href="client.asp" alt="patient">Patient</a></li>
          <li class="schedule"><a href="schedule.asp" alt="schedule">Schedule</a></li>
          <li class="doctor"><a href="doctor.asp" alt="doctor">Doctor</a></li>
          <li class="pharmacy"><a href="pharmacy.asp" alt="pharmacy">Pharmacy</a></li>
          <li class="insurance"><a href="insurance.asp" alt="insurance">Insurance</a></li>
          <li class="reports"><a href="reports.asp" alt="reports">Reports</a></li>
          <li class="reminders"><a href="reminders.asp" alt="reminders">Reminders</a></li>
          <li class="encounters"><a href="encounters.asp" alt="encounters">Encounters</a></li>
          <li class="help"><a href="help.asp" alt="help">Help</a></li>
          <li class="logout"><a href="login.asp?logout=true" alt="logout">Logout</a></li>
        </ul>  
    </div>
    
    <div id="main">
      


<table width="100%"  >
  <tr>
    <td valign="top">
   
      <table width="100%" border="0" >
        <tr>
          <td colspan="7"> 
            <!--#include file="search.inc"--> 
          </td>
        </tr>
      </table>

      <%
        if request.querystring("pid") <> "" then
      %>

      <!--#include file="client_database_query.inc"-->
      <!--#include file="client_basics.inc"-->

      <%
        end if ''If no records found from database query
      end if
      %>
    </td>
  </tr>
</table>