<!--#include file="adovbs.inc"-->
<!--#include file="db.asp"-->
<!--#include file="header.inc"-->

<!-- Start MedLog Add -->
<%
''response.write request.form
''response.end

if request.form("authlogadd") = "Add" then  
Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

'displays header on form field validation pages
function displayHeader()
	Response.Write "<html>"
	Response.Write "<head>"
	Response.Write "<body>"
	Response.Write "<p align='center'>"
	Response.Write "<div align = 'center'>"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.Write "<font size='4' color=red face='arial'><strong>Error</strong><br /></p>"
end function

'Validate user entries in form fields and generate error pages
For Each key in Request.Form
		strName = key
		strValue = Request.Form(key)
		Session(strName) = strValue
	Next

    client_id                               = Request.Form("client_id")
    authorization_id                        = RandomID
    client_firstname                        = Request.Form("client_firstname")
    client_middleinitial                    = Request.Form("client_middleinitial")
    client_lastname                         = Request.Form("client_lastname")
    client_dateofbirth                      = Request.Form("client_dateofbirth")
    client_phoneNumber                      = Request.Form("client_phoneNumber")
	authorization_date                      = Request.Form("authorization_date")
	if Request.Form("client_insuranceID") <> "0" then client_insuranceID = left(Request.Form("client_insuranceID"),(instr(Request.Form("client_insuranceID"),",")-1))
	client_insuranceName                    = mid( Request.Form("client_insuranceID"), (instr(Request.Form("client_insuranceID"),",")+1), len(Request.Form("client_insuranceID")) - instr(Request.Form("client_insuranceID"),",") )
    authorization_rep                       = Request.Form("authorization_rep")
    authorization_insurance_rep             = Request.Form("authorization_insurance_rep")
    authorization_insurance_referencenumber = Request.Form("authorization_insurance_referencenumber")
    authorization_status                    = Request.Form("authorization_status")
    authorization_comments                  = Request.Form("authorization_comments")

If authorization_date = "" or len(authorization_date) <10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The authorization date is a required field. Please enter as MM/DD/YYYY." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(authorization_date) > 10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The authorization date is too long. Please enter as MM/DD/YYYY." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If client_insuranceID = "" or client_insuranceID = "0" then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If authorization_rep = "" or len(authorization_rep) <3 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The CDPC representative a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(authorization_rep) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The CDPC representative is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If authorization_insurance_rep = ""  or len(authorization_insurance_rep) <3 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance representative is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(authorization_insurance_rep) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance representative is too large.  Maximum amount is 1000." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If authorization_insurance_referencenumber = "" or len(authorization_insurance_referencenumber) <3 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance reference number is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(authorization_insurance_referencenumber) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance reference number is too large.  Maximum amount is 10." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
if authorization_status = "" or len(authorization_status) <3 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The authorization status is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(authorization_status) >50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The authorization status is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(authorization_comments) >250 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The comments are too long.  Maximum length is 250 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if


    MySQL = "SELECT * FROM authlog"

    Set MyAuth = Server.CreateObject("ADODB.Recordset") 
    MyAuth.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

      MyAuth.AddNew
      MyAuth("pid")             = client_id
      MyAuth("ControlNo")       = authorization_id
      MyAuth("ufname")          = client_firstname
      MyAuth("uminitial")       = client_middleinitial
      MyAuth("ulname")          = client_lastname
      MyAuth("dob")             = client_dateofbirth
      MyAuth("upphone")         = client_phoneNumber
      MyAuth("insid")           = client_insuranceID
      MyAuth("insName")         = client_insuranceName
      MyAuth("insPhone")        = client_insurancePhoneNumber
      MyAuth("insFax")          = client_insurancePhoneFax
	  MyAuth("entrydate")       = authorization_date
      MyAuth("entryRepName")    = authorization_rep
	  MyAuth("insRepName")      = authorization_insurance_rep
      MyAuth("insRefNumber")    = authorization_insurance_referencenumber
      MyAuth("status")          = authorization_status
      MyAuth("comments")        = authorization_comments
    MyAuth.Update

    MyAuth.close
    set MyAuth = nothing


    response.write "Update Successful<br />"
end if


if  request.form("clientadd") <> "Add" then
    response.write "<form action='client.asp' method=POST>"
    response.write "<div id='clientauth'>Pabtient Search<br /></div>"
    response.write "<input type='text' name='client' size='30' value=''>&nbsp;&nbsp;"
    response.write "<input type='submit' name='clientsearch' value='Search'><input type='submit' name='clientadd' value='Add'><br /></form>"
end if
%>

<!-- End MedLog Add -->

<!--#include file="footer.inc"-->