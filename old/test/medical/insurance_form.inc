
<!-- Start Insurance Form -->
<form action="insurance_add.asp" method="post">
<table width="100%">
  <tr>
    <td width="2%"> </td>
    <td width="96%"> 
            <table width="100%">
            <tr>
                <td colspan="2" class="subheading"><strong>Insurance Information</strong></td>
			</tr>
			<tr>
                <td>Insurance Name: (ID:<%= right("00000" & insurance_ID, 5) %>)</td>
                <td>
                  <input type="text" name="insurance_name" size="25" value="<%= insurance_name %>">
                </td>
            </tr>
            <tr>
                <td>Address: </td>
                <td>
                <input type="text" name="insurance_address" size="25" value="<%= insurance_address %>">
                </td>
            </tr>
            <tr>
                <td>City, State/Province, Zip/Postal Code: </td>
                <td>
                <input type="text" name="insurance_city" size="25" value="<%= insurance_city %>">
                <input type="text" name="insurance_state" size="5" value="<%= insurance_state %>">
                <input type="text" name="insurance_postalCode" size="5" value="<%= insurance_postalCode %>">
                </td>
            </tr>
            <tr>
                <td>Insurance Telephone: </td>
                <td>
                <input type="text" name="insurance_PhoneNumber" size="25" value="<%= insurance_PhoneNumber %>">
                </td>
            </tr>
            <tr>
                <td>Insurance Fax: </td>
                <td>
                <input type="text" name="insurance_PhoneFax" size="25" value="<%= insurance_PhoneFax %>">
                </td>
            </tr>
            <tr>
              <td colspan="2"><hr color="#CCCCCC" size="1" noshade></td>
            </tr>
            <tr>
                <td colspan="2" class="subheading"><strong>&nbsp;</strong></td>
            </tr>
            <tr>
              <td colspan="2"><br />
                <input type="hidden" name="insurance_id" value="<%= insurance_id %>">
                <center>
                  <%
                  if insurance_id = "" then
                    response.write "<input type='submit' name='insurancesubmit' value='Submit'>" & vbCrLF
                    response.write "<input type='submit' name='insurancesubmit' value='Cancel'>" & vbCrLF
                  else
                    response.write "<input type='submit' name='insurancesubmit' value='Update'>" & vbCrLF
                    response.write "<input type='submit' name='insurancesubmit' value='Cancel'>" & vbCrLF
                    response.write "<input type='submit' name='insurancesubmit' value='Delete'>" & vbCrLF
                  end if
                  %>
                </center></form>
              </td>
            </tr>
          </table>
    </td>
    <td width="2%"> </td>
  </tr>
</table>
<!-- End Insurance Form -->
