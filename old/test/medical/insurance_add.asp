<!--#include file="adovbs.inc"-->
<!--#include file="db.asp"-->
<!--#include file="func_activity.inc"-->
<!--#include file="header.inc"-->

<!-- Start Insurance Add -->
<%
''response.write request.form
''response.end
if request.form("insurancesubmit") = "Delete" then

    ''Insurance Delete Logging
    insurance_id                          = Request.Form("insurance_id")
    insurance_name                        = Request.Form("insurance_name")
    description = "Insurance delete #" & insurance_id & ": " & insurance_name
    test = activity(session("username"),description)

    ''MySQL = "DELETE FROM insurance WHERE insid = " & request.form("insurance_id")
    MySQL = "SELECT * FROM insurance WHERE insid = " & request.form("insurance_id")
    ''response.write MySQL
    ''deleteflag = 1 means record has been deleted
    
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

    if Not MyRS.EOF then
      MyRS("deleteflag") = 1
      MyRS.Update
    end if

    MyRS.close
    set MyRS = nothing

    response.write "Deleted<br />"

else

if request.form("insurancesubmit") = "Cancel" then response.redirect "insurance.asp"

if request.form("insurancesubmit") = "Submit" or request.form("insurancesubmit") = "Update" then  
Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

'displays header on form field validation pages
function displayHeader()
	Response.Write "<html>"
	Response.Write "<head>"
	Response.Write "<body>"
	Response.Write "<p align='center'>"
	Response.Write "<div align = 'center'>"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.Write "<font size='4' color=red face='arial'><strong>Error</strong><br /></p>"
end function

'Validate user entries in form fields and generate error pages
For Each key in Request.Form
		strName = key
		strValue = Request.Form(key)
		Session(strName) = strValue
	Next

    insurance_id                          = Request.Form("insurance_id")
    insurance_name                        = Request.Form("insurance_name")
	insurance_address                     = Request.Form("insurance_address")
	insurance_city                        = Request.Form("insurance_city")
	insurance_state                       = Request.Form("insurance_state")
	insurance_postalcode                  = Request.Form("insurance_postalcode")
    insurance_phoneNumber                 = Request.Form("insurance_phonenumber")
    insurance_phoneFax                    = Request.Form("insurance_phonefax")

If insurance_name = "" OR len(insurance_name) <=2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "Please enter the insurance name." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(insurance_name) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(insurance_address) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The address is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(insurance_city) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The city or town is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(insurance_state) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The state or province is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(insurance_postalCode) > 10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The Zip/Postal code is too long.  Maximum length is 10 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(insurance_phoneNumber) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance phone number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(insurance_phoneFax) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance fax number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if

    MySQL = "SELECT * FROM insurance"

    intInsuranceID = 0
    intInsuranceHighRecord = 0
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn

    Do While Not MyRS.EOF
      intInsuranceID = intInsuranceID + 1
      if intInsuranceHighRecord < MyRS("insid") then intInsuranceHighRecord = MyRS("insid")

    MyRS.MoveNext 
    Loop 

    if intInsuranceID < 1 then
      intInsuranceID = 1
      intInsuranceHighRecord = 1
    else
      intInsuranceID = intInsuranceID + 1
      intInsuranceHighRecord = intInsuranceHighRecord + 1
    end if

    ''response.write "Current Insurance #: " & intInsuranceID & " High Record #: " & intInsuranceHighRecord

    MyRS.Close
    set MyRS = Nothing
    if request.form("insurancesubmit") = "Submit" then
      MySQL = "SELECT * FROM insurance"
    else 
      MySQL = "SELECT * FROM insurance WHERE insid = " & insurance_id
    end if

    Set rsinsurance = Server.CreateObject("ADODB.Recordset") 
    rsinsurance.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

      if request.form("insurancesubmit") = "Submit" then
        rsinsurance.AddNew
        rsinsurance("insid")          = intInsuranceHighRecord
        ''rsinsurance("cdate")        = Date & " " & Time
        rsinsurance("deleteflag")     = 0
        ''Deleteflag: 0-Active, 1-Deleted

        ''Insurance Add Logging
        description = "Insurance add #"
      end if

      rsinsurance("insurancename")    = insurance_name
      rsinsurance("insuranceaddress") = insurance_address
      rsinsurance("insurancecity")    = insurance_city
      rsinsurance("insurancestate")   = insurance_state
      rsinsurance("insurancezip")     = insurance_postalCode
      if insurance_phonenumber <> "" then rsinsurance("insurancephone")   = insurance_phonenumber
      if insurance_phonefax    <> "" then rsinsurance("insurancefax")     = insurance_phonefax
    rsinsurance.Update

    rsinsurance.close
    set rsinsurance = nothing

    set MyConn = Nothing

    ''Insurance Add/Modify Logging
    if description = "Insurance add #" then 
      description = "Insurance add #" & intInsuranceHighRecord & ": " & insurance_name
    else
      description = "Insurance modify #" & Request.Form("insurance_id") & ": " & insurance_name
    end if
    test = activity(session("username"),description)
    description = ""
end if

response.write "Update Successful<br />"

end if ''Delete Insurance IF STATEMENT
if  request.form("insuranceadd") <> "Add" then
    response.write "<form action='insurance.asp' method=POST>"
    response.write "Insurance Search<br />"
    response.write "<input type='text' name='insurance' size='30' value=''>&nbsp;&nbsp;"
    response.write "<input type='submit' name='insurancesearch' value='Search'><input type='submit' name='insuranceadd' value='Add'><br /></form>"
end if
%>

<!-- End Insurance Add -->

<!--#include file="footer.inc"-->