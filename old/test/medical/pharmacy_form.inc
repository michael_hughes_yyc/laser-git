

<!-- Start Pharmacy Form -->
<form action="pharmacy_add.asp" method="post">
<table width="100%">
  <tr>
    <td width="2%"> </td>
    <td width="96%"> 
            <table width="100%">
            <tr>
                <td colspan="2" class="subheading"><strong>Pharmacy Information</strong></td>
			</tr>
			<tr>
                <td>Pharmacy Name: (ID:<%= right("00000" & pharmacy_ID, 5) %>)</td>
                <td>
                  <input type="text" name="pharmacy_name" size="25" value="<%= pharmacy_name %>">
                </td>
            </tr>
            <tr>
                <td>Address: </td>
                <td>
                <input type="text" name="pharmacy_address" size="25" value="<%= pharmacy_address %>">
                </td>
            </tr>
            <tr>
                <td>City, State/Province, Zip/Postal Code: </td>
                <td>
                <input type="text" name="pharmacy_city" size="25" value="<%= pharmacy_city %>">
                <input type="text" name="pharmacy_state" size="5" value="<%= pharmacy_state %>">
                <input type="text" name="pharmacy_postalCode" size="5" value="<%= pharmacy_postalCode %>">
                </td>
            </tr>
            <tr>
                <td>Pharmacy Telephone: </td>
                <td>
                <input type="text" name="pharmacy_PhoneNumber" size="25" value="<%= pharmacy_PhoneNumber %>">
                </td>
            </tr>
            <tr>
                <td>Pharmacy Fax: </td>
                <td>
                <input type="text" name="pharmacy_PhoneFax" size="25" value="<%= pharmacy_PhoneFax %>">
                </td>
            </tr>
            <tr>
              <td colspan="2"><hr color="#CCCCCC" size="1" noshade></td>
            </tr>
            <tr>
                <td colspan="2" class="subheading"><strong>&nbsp;</strong></td>
            </tr>
            <tr>
              <td colspan="2"><br />
                <input type="hidden" name="pharmacy_id" value="<%= pharmacy_id %>">
                <center>
                  <%
                  if pharmacy_id = "" then
                    response.write "<input type='submit' name='pharmacysubmit' value='Submit'>" & vbCrLF
                    response.write "<input type='submit' name='pharmacysubmit' value='Cancel'>" & vbCrLF
                  else
                    response.write "<input type='submit' name='pharmacysubmit' value='Update'>" & vbCrLF
                    response.write "<input type='submit' name='pharmacysubmit' value='Cancel'>" & vbCrLF
                    response.write "<input type='submit' name='pharmacysubmit' value='Delete'>" & vbCrLF
                  end if
                  %>
                </center></form>
              </td>
            </tr>
          </table>
    </td>
    <td width="2%"> </td>
  </tr>
</table>
<!-- End Pharmacy Form -->
