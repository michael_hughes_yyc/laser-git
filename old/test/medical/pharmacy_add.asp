<!--#include file="adovbs.inc"-->
<!--#include file="db.asp"-->
<!--#include file="func_activity.inc"-->
<!--#include file="header.inc"-->

<!-- Start Pharmacy Add -->
<%
''response.write request.form
''response.end
if request.form("pharmacysubmit") = "Delete" then

    ''Pharmacy Delete Logging
    pharmacy_id                          = Request.Form("pharmacy_id")
    pharmacy_name                        = Request.Form("pharmacy_name")
    description = "Pharmacy delete #" & pharmacy_id & ": " & pharmacy_name
    test = activity(session("username"),description)

    ''MySQL = "DELETE FROM pharmacy WHERE pmcid = " & request.form("pharmacy_id")
    MySQL = "SELECT * FROM pharmacy WHERE pmcid = " & request.form("pharmacy_id")
    ''response.write MySQL
    ''delflag = 1 means record has been deleted
    
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

    if Not MyRS.EOF then
      MyRS("delflag") = 1
      MyRS.Update
    end if

    MyRS.close
    set MyRS = nothing

    response.write "Deleted<br />"

else

if request.form("pharmacysubmit") = "Cancel" then response.redirect "pharmacy.asp"

if request.form("pharmacysubmit") = "Submit" or request.form("pharmacysubmit") = "Update" then  
Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

'displays header on form field validation pages
function displayHeader()
	Response.Write "<html>"
	Response.Write "<head>"
	Response.Write "<body>"
	Response.Write "<p align='center'>"
	Response.Write "<div align = 'center'>"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.Write "<font size='4' color=red face='arial'><strong>Error</strong><br /></p>"
end function

'Validate user entries in form fields and generate error pages
For Each key in Request.Form
		strName = key
		strValue = Request.Form(key)
		Session(strName) = strValue
	Next

    pharmacy_id                          = Request.Form("pharmacy_id")
    pharmacy_name                        = Request.Form("pharmacy_name")
	pharmacy_address                     = Request.Form("pharmacy_address")
	pharmacy_city                        = Request.Form("pharmacy_city")
	pharmacy_state                       = Request.Form("pharmacy_state")
	pharmacy_postalcode                  = Request.Form("pharmacy_postalcode")
    pharmacy_phoneNumber                 = Request.Form("pharmacy_phonenumber")
    pharmacy_phoneFax                    = Request.Form("pharmacy_phonefax")

If pharmacy_name = "" OR len(pharmacy_name) <=2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "Please enter the pharmacy name." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(pharmacy_name) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The pharmacy name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(pharmacy_address) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The address is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(pharmacy_city) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The city or town is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(pharmacy_state) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The state or province is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(pharmacy_postalCode) > 10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The Zip/Postal code is too long.  Maximum length is 10 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(pharmacy_phoneNumber) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The pharmacy phone number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(pharmacy_phoneFax) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The pharmacy fax number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if

    MySQL = "SELECT * FROM pharmacy"

    intPharmacyID = 0
    intPharmacyHighRecord = 0
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn

    Do While Not MyRS.EOF
      intPharmacyID = intPharmacyID + 1
      if intPharmacyHighRecord < MyRS("pmcid") then intPharmacyHighRecord = MyRS("pmcid")

    MyRS.MoveNext 
    Loop 

    if intPharmacyID < 1 then
      intPharmacyID = 1
      intPharmacyHighRecord = 1
    else
      intPharmacyID = intPharmacyID + 1
      intPharmacyHighRecord = intPharmacyHighRecord + 1
    end if

    ''response.write "Current Pharmacy #: " & intPharmacyID & " High Record #: " & intPharmacyHighRecord

    MyRS.Close
    set MyRS = Nothing
    if request.form("pharmacysubmit") = "Submit" then
      MySQL = "SELECT * FROM pharmacy"
    else 
      MySQL = "SELECT * FROM pharmacy WHERE pmcid = " & pharmacy_id
    end if

    Set rsPharmacy = Server.CreateObject("ADODB.Recordset") 
    rsPharmacy.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

      if request.form("pharmacysubmit") = "Submit" then
        rsPharmacy.AddNew
        rsPharmacy("pmcid")         = intPharmacyHighRecord
        ''rsPharmacy("cdate")       = Date & " " & Time
        rsPharmacy("delflag")       = 0
        ''Delflag: 0-Active, 1-Deleted

        ''Pharmacy Add Logging
        description = "Pharmacy add #"
      end if

      rsPharmacy("pharmacyname")    = pharmacy_name
      rsPharmacy("pharmacyaddress") = pharmacy_address
      rsPharmacy("pharmacycity")    = pharmacy_city
      rsPharmacy("pharmacystate")   = pharmacy_state
      rsPharmacy("pharmacyzip")     = pharmacy_postalCode
      if pharmacy_phonenumber <> "" then rsPharmacy("pharmacyphone")   = pharmacy_phonenumber
      if pharmacy_phonefax    <> "" then rsPharmacy("pharmacyfax")     = pharmacy_phonefax
    rsPharmacy.Update

    rsPharmacy.close
    set rsPharmacy = nothing

    set MyConn = Nothing

    ''Pharmacy Add/Modify Logging
    if description = "Pharmacy add #" then 
      description = "Pharmacy add #" & intPharmacyHighRecord & ": " & pharmacy_name
    else
      description = "Pharmacy modify #" & Request.Form("pharmacy_id") & ": " & pharmacy_name
    end if
    test = activity(session("username"),description)
    description = ""
end if

response.write "Update Successful<br />"

end if ''Delete Pharmacy IF STATEMENT
if  request.form("pharmacyadd") <> "Add" then
    response.write "<form action='pharmacy.asp' method=POST>"
    response.write "Pharmacy Search<br />"
    response.write "<input type='text' name='pharmacy' size='30' value=''>&nbsp;&nbsp;"
    response.write "<input type='submit' name='pharmacysearch' value='Search'><input type='submit' name='pharmacyadd' value='Add'><br /></form>"
end if
%>

<!-- End Pharmacy Add -->

<!--#include file="footer.inc"-->