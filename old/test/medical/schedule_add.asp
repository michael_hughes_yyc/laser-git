<!--#include file="db.asp"-->

<%
' Prevent Caching
Response.Expires = 0
Response.ExpiresAbsolute = now - 1
Response.CacheControl = "no-cache"
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

if Request.Item("scheduletask")
Response.Write "<html><head><title>Updating Event</title></head><body><center>"

set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_String

usertype_id = 1   ''Script modified to include Admin and Employee ID so Mike Scotts name is found
delflag_id = 0
MySQL = "SELECT * FROM users WHERE uid = " & Request.Item("scheduleEmployeeID") & " and ((usertype = " & usertype_id & ") or (usertype = 2)) and (delflag = " & delflag_id & ")"
''response.write MySQL
''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
''delflag: 0-Active, 1-Deleted

Set MyRS = Server.CreateObject("ADODB.Recordset") 
MyRS.Open MySQL, MyConn
if Not MyRS.EOF then scheduleEmployee = MyRS("ulname") & ", " & MyRS("ufname") & " " & MyRS("uminitial")
MyRS.close
set MyRS = Nothing 

usertype_id = 3
delflag_id = 0
MySQL = "SELECT * FROM users WHERE uid = " & Request.Item("scheduleClientID") & " and (usertype = " & usertype_id & ") and (delflag = " & delflag_id & ")"
Set MyRS = Server.CreateObject("ADODB.Recordset") 
''response.write MySQL
''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
''delflag: 0-Active, 1-Deleted
MyRS.Open MySQL, MyConn
if Not MyRS.EOF then 
  scheduleClient         = MyRS("ulname") & ", " & MyRS("ufname") & " " & MyRS("uminitial")
  schedule_dateofbirth   = MyRS("dob")
  schedule_phonenumber   = MyRS("upPhone")
end if
MyRS.close
set MyRS = Nothing

''ERROR CHECKING
if len(Request.Item("scheduleMonth")) <> 2 then
  response.write "Error: Month"
  response.end
end if
if len(Request.Item("scheduleYear")) <> 4 then
  response.write "Error: Year"
  response.end
end if
if len(Request.Item("scheduleDay")) <> 2 then
  response.write "Error: Day"
  response.end
end if
if len(Request.Item("scheduleStartTime")) <> 4 then
  response.write "Error: Start Time"
  response.end
end if
if len(Request.Item("scheduleEndTime")) <> 4 then
  response.write "Error: End Time"
  response.end
end if
if len(scheduleEmployee) < 5 then
  response.write "Error: Employee"
  response.end
end if
if len(Request.Item("scheduleEmployeeID")) <> 5 then
  response.write "Error: Employee ID"
  response.end
end if
if len(scheduleClient) < 5 then
  response.write "Error: Client"
  response.end
end if
if ((len(schedule_dateofbirth) < 8) or (len(schedule_dateofbirth) > 10)) then
  response.write "Error: Date of Birth"
  response.end
end if
if ((len(schedule_phonenumber) < 10) or (len(schedule_phonenumber) > 12)) then
  response.write "Error: Phone Number"
  response.end
end if
if len(Request.Item("scheduleClientID")) <> 5 then
  response.write "Error: Client ID"
  response.end
end if
if len(Request.Item("schedulestatus")) <> 0 then
  response.write "Error: Status"
  response.end
end if
if len(Request.Item("scheduletype")) < 4 then
  response.write "Error: Schedule Type"
  response.end
end if
if len(Request.Item("scheduleNotes")) > 50 then
  response.write "Error: Notes"
  response.end
end if

Response.Write "Saving New Event....."
Response.Flush

set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_SCHstring

set CONNECT = server.CreateObject("ADODB.Connection")
Connect.Open DB_SCHstring

SQL = "Insert Into schEvents "
SQL = SQL & "(scheduleID, scheduleMonth, scheduleYear, scheduleDay, scheduleStartTime, scheduleEndTime, scheduleEmployee, scheduleEmployeeID, scheduleClient, scheduleDOB, schedulePhone, scheduleStaff, scheduleClientID, schedulestatus, scheduletype, scheduleNotes) values ("
SQL = SQL & "'" & RandomID & "', "
SQL = SQL & "'" & Request.Item("scheduleMonth") & "', "
SQL = SQL & "'" & Request.Item("scheduleYear") & "', "
SQL = SQL & "'" & Request.Item("scheduleDay") & "', "
SQL = SQL & "'" & Replace(Request.Item("scheduleStartTime"), "'", "''") & "', "
SQL = SQL & "'" & Replace(Request.Item("scheduleEndTime"), "'", "''") & "', "
SQL = SQL & "'" & scheduleEmployee & "', "
SQL = SQL & "'" & Replace(Replace(Request.Item("scheduleEmployeeID"), "'", "''"),"<br />", vbCrLf) & "', "
SQL = SQL & "'" & scheduleClient & "', "
SQL = SQL & "'" & schedule_dateofbirth & "', "
SQL = SQL & "'" & schedule_phonenumber & "', "
SQL = SQL & "'" & session("username") & "', "
SQL = SQL & "'" & Replace(Replace(Request.Item("scheduleClientID"), "'", "''"),"<br />", vbCrLf) & "', "
SQL = SQL & "'" & Replace(Replace(Request.Item("schedulestatus"), "'", "''"),"<br />", vbCrLf) & "', "
SQL = SQL & "'" & Replace(Replace(Request.Item("scheduletype"), "'", "''"),"<br />", vbCrLf) & "', "
SQL = SQL & "'" & Replace(Replace(Request.Item("scheduleNotes"), "'", "''"),"<br />", vbCrLf) & "'"
SQL = SQL & ");"
''response.write SQL
set mData = Connect.Execute(SQL)
%>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("window.close()",1500);
</SCRIPT>
</body></html>