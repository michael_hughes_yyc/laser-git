<!--#include file="db.asp"-->

<%

' Prevent Caching
Response.Expires = 0
Response.ExpiresAbsolute = now - 1
Response.CacheControl = "no-cache"
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

Response.Write "<html><head><title>Deleting Bulk Event</title></head><body><center>"
Response.Write "Deleting Bulk Event....."
Response.Flush

Function WriteEvent(scheduleID, scheduleMonth, scheduleYear, scheduleDay, scheduleStartTime, scheduleEndTime, scheduleEmployee, scheduleEmployeeID, scheduleClient, scheduleClientID, scheduleNotes)
  set CONNECT = server.CreateObject("ADODB.Connection")
  Connect.Open DB_SCHstring
  SQL = "delete from schEvents where "
  SQL = SQL & "scheduleMonth = '" & scheduleMonth & "' and "
  SQL = SQL & "scheduleYear = '" & scheduleYear & "' and "
  SQL = SQL & "scheduleDay = '" & scheduleDay & "' and "
  SQL = SQL & "scheduleEmployeeID = '" & Replace(Replace(scheduleEmployeeID, "'", "''"),"<br />", vbCrLf) & "' and "
  SQL = SQL & "scheduleClientID = '" & Replace(Replace(scheduleClientID, "'", "''"),"<br />", vbCrLf) & "'"
  set mData = Connect.Execute(SQL)
  ''response.write "<td>" & SQL & "</td>"
  set CONNECT = nothing
End Function

test = "ABC"
if test="123" then
  month_start     = "09"
  day_start       = "01"
  year_start      = "2007"
 
  month_end       = "12"
  day_end         = "10"
  year_end        = "2007"
  
  sunday_start    = "..."
  sunnday_end     = "..."
  monday_start    = "..."
  monday_end      = "..."
  tuesday_start   = "..."
  tuesday_end     = "..."
  wednesday_start = "..."
  wednesday_end   = "..."
  thursday_start  = "..."
  thursday_end    = "..."
  friday_start    = "0800"
  friday_end      = "1300"
  saturday_start  = "..."
  saturday_end    = "..."

  scheduleEmployee   = "Ada M. Rankins evenings ASAP"
  scheduleEmployeeID = "WBYKOGVTSM20070531091017"
  scheduleClient     = "Mary Britz CLOSED 8/30"
  scheduleClientID   = "NVQNRRNWUY20070816193810"
  scheduleNotes      = "This is a bulk test..."

else

  ''response.write request.querystring
  month_start     = request.querystring("scheduleMonth_start")
  day_start       = request.querystring("scheduleDay_start")
  year_start      = request.querystring("scheduleYear_start")

  month_end       = request.querystring("scheduleMonth_end")
  day_end         = request.querystring("scheduleDay_end")
  year_end        = request.querystring("scheduleYear_end")

  sunday_start    = request.querystring("SunStartTime")
  sunday_end      = request.querystring("SunEndTime")
  monday_start    = request.querystring("MonStartTime")
  monday_end      = request.querystring("MonEndTime")
  tuesday_start   = request.querystring("TueStartTime")
  tuesday_end     = request.querystring("TueEndTime")
  wednesday_start = request.querystring("WedStartTime")
  wednesday_end   = request.querystring("WedEndTime")
  thursday_start  = request.querystring("ThuStartTime")
  thursday_end    = request.querystring("ThuEndTime")
  friday_start    = request.querystring("FriStartTime")
  friday_end      = request.querystring("FriEndTime")
  saturday_start  = request.querystring("SatStartTime")
  saturday_end    = request.querystring("SatEndTime")

  scheduleEmployee   = scheduleEmployee
  scheduleEmployeeID = Request.Item("scheduleEmployeeID")
  scheduleClient     = scheduleClient
  scheduleClientID   = Request.Item("scheduleClientID")
  scheduleNotes      = Request.Item("scheduleNotes")


  set MyConn = Server.CreateObject("ADODB.Connection")
  MyConn.ConnectionTimeout = 15
  MyConn = DB_String

  MySQL = "SELECT * FROM employees WHERE employee_ID = '" & Request.Item("scheduleEmployeeID") & "'"
  Set MyRS = Server.CreateObject("ADODB.Recordset") 
  ''response.write MySQL
  MyRS.Open MySQL, MyConn
  if Not MyRS.EOF then scheduleEmployee = MyRS("employee_name")
  MyRS.close
  set MyRS = Nothing 
 
  MySQL = "SELECT * FROM clients WHERE clientID = '" & Request.Item("scheduleClientID") & "'"
  Set MyRS = Server.CreateObject("ADODB.Recordset") 
  ''response.write MySQL
  MyRS.Open MySQL, MyConn
  if Not MyRS.EOF then scheduleClient = MyRS("client_name")
  MyRS.close
  set MyRS = Nothing

  set MyConn = Server.CreateObject("ADODB.Connection")
  MyConn.ConnectionTimeout = 15
  MyConn = DB_SCHstring
end if

response.write "<table border='1'>"
response.write "<tr>"
for count_year = year_start to year_end
  if count_year = cInt(year_end) then
    count_month_end = month_end
  else
    month_start = "01"
    count_month_end = "12"
  end if
  for count_month = month_start to count_month_end
    varMonth = count_month
    select case varMonth
      case 9, 4, 6, 11
	 enddate = 30
      case 2
        if count_year mod 4 = 0 then	'Leap-year calc.
          enddate = 29
        else
          enddate = 28
        end if
      case else
        enddate = 31
    end select
    if count_year = cInt(year_end) and count_month = cInt(month_end) then
      if cInt(day_end) > cInt(enddate) then
        count_day_end = enddate
      else
        count_day_end = day_end
      end if
    else
      count_day_end = enddate
    end if
    'response.write count_day_end
    for count_day = day_start to count_day_end
    temp = trim(right("00" & count_month, 2) & "/" & right("00" & count_day,2) & "/" & right("0000" & count_year,4))
    dayofweek_start = ""
    dayofweek_end = ""
    select case WeekdayName(Weekday(temp),False)
      case "Sunday"
        dayofweek_start = sunday_start 
        dayofweek_end   = sunday_end
      case "Monday"
        dayofweek_start = monday_start 
        dayofweek_end   = monday_end
      case "Tuesday"
        dayofweek_start = tuesday_start 
        dayofweek_end   = tuesday_end
      case "Wednesday"
        dayofweek_start = wednesday_start 
        dayofweek_end   = wednesday_end
      case "Thursday"
        dayofweek_start = thursday_start 
        dayofweek_end   = thursday_end
      case "Friday"
        dayofweek_start = friday_start 
        dayofweek_end   = friday_end
      case "Saturday"
        dayofweek_start = saturday_start 
        dayofweek_end   = saturday_end
    end select
    ''if dayofweek_start <> "..." and dayofweek_end <> "..." then
      response.write "<td>" & temp  & "</td>" & "<td>" & WeekdayName(Weekday(temp),False)  & "</td><td>" & dayofweek_start & "</td><td>" & dayofweek_end & "</td>"
      scheduleID         = RandomID
      scheduleMonth      = right("00" & count_month,2)
      scheduleYear       = right("0000" & count_year,4)
      scheduleDay        = right("00" & count_day, 2)
      scheduleStartTime  = dayofweek_start
      scheduleEndTime    = dayofweek_end
      test = WriteEvent(scheduleID, scheduleMonth, scheduleYear, scheduleDay, scheduleStartTime, scheduleEndTime, scheduleEmployee, scheduleEmployeeID, scheduleClient, scheduleClientID, scheduleNotes)
    ''else
      ''response.write "<td>" & WeekdayName(Weekday(temp),False)  & "</td><td>&nbsp;</td><td>&nbsp;</td>"
    ''end if
    response.write "</tr>"
    next
  next
month_start = "01"
next
response.write "</table>"
%>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("window.close()",1500);
</SCRIPT>
</body></html>