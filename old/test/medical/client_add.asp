<!--#include file="adovbs.inc"-->
<!--#include file="db.asp"-->
<!--#include file="func_activity.inc"-->
<!--#include file="header.inc"-->

<!-- Start Client Add -->
<%
''response.write request.form
''response.end
if request.form("clientsubmit") = "Delete" then

    ''Client Delete Logging
    client_id                          = Request.Form("client_id")
	client_firstname                   = Request.Form("client_firstname")
	client_lastname                    = Request.Form("client_lastname")
    description = "Client delete #" & client_id & ": " & client_lastname & ", " & client_firstname
    test = activity(session("username"),description)

    set MyConn = Server.CreateObject("ADODB.Connection")
    MyConn.ConnectionTimeout = 15
    MyConn = DB_String

    ''MySQL = "DELETE FROM users WHERE uid = " & request.form("client_id")
    MySQL = "SELECT * FROM users WHERE uid = " & request.form("client_id")
    ''response.write MySQL
    ''delflag = 1 means record has been deleted
    
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

    if Not MyRS.EOF then
      MyRS("delflag") = 1
      MyRS.Update
    end if

    MyRS.close
    set MyRS = nothing

    response.write "Deleted<br />"

else

if request.form("clientsubmit") = "Cancel" then response.redirect "client.asp"
if request.form("attachmentsubmit") = "Cancel" then response.redirect "client.asp?pid=" & request.form("attachment_pid")

if request.form("clientsubmit") = "Submit" or request.form("clientsubmit") = "Update" then  
Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

'displays header on form field validation pages
function displayHeader()
	Response.Write "<html>"
	Response.Write "<head>"
	Response.Write "<body>"
	Response.Write "<p align='center'>"
	Response.Write "<div align = 'center'>"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.Write "<font size='4' color=red face='arial'><strong>Error</strong><br /></p>"
end function

'Validate user entries in form fields and generate error pages
For Each key in Request.Form
		strName = key
		strValue = Request.Form(key)
		Session(strName) = strValue
	Next

    if form = "insurance" then 
      client_id = Request.Form("inspid") 
    else
      client_id = Request.Form("client_id") 
    end if   
    client_action                      = Request.Form("client_action")
    client_form                        = Request.Form("client_form")
	client_firstname                   = Request.Form("client_firstname")
	client_middleinitial               = Request.Form("client_middleinitial")
	client_lastname                    = Request.Form("client_lastname")
	client_address                     = Request.Form("client_address")
	client_city                        = Request.Form("client_city")
	client_state                       = Request.Form("client_state")
	client_postalcode                  = Request.Form("client_postalcode")
	client_phonenumber                 = Request.Form("client_phonenumber")
	client_phonecellular               = Request.Form("client_phonecellular")
	client_dateofbirth                 = Request.Form("client_dateofbirth")
	client_gender                      = Request.Form("client_gender")
    client_ssn                         = Request.Form("client_ssn")
	client_age                         = Request.Form("client_age")
    client_physicianID                 = Request.Form("client_physicianID")
    client_physicianName               = Request.Form("client_physicianName")
    client_physicianIDRef              = Request.Form("client_physicianIDRef")
    client_physicianNameRef            = Request.Form("client_physicianNameRef")
    client_physicianIDRend             = Request.Form("client_physicianIDRend")
    client_physicianNameRend           = Request.Form("client_physicianNameRend")
    client_pharmacyID                  = Request.Form("client_pharmacyID")
    client_pharmacyName                = Request.Form("client_pharmacyname")
    client_pharmacyPhoneNumber         = Request.Form("client_pharmacyphone")
    client_pharmacyPhoneFax            = Request.Form("client_pharmacyfax")
	client_insuranceID                 = Request.Form("client_insuranceID")
	client_insuranceName               = Request.Form("client_insuranceName")
	client_insurancePolicyNumber       = Request.Form("client_insurancePolicyNumber")
	client_insuranceIDSecond           = Request.Form("client_insuranceIDSecond")
	client_insuranceNameSecond         = Request.Form("client_insuranceNameSecond")
	client_insurancePolicyNumberSecond = Request.Form("client_insurancePolicyNumberSecond")
	client_insuranceIDThird            = Request.Form("client_insuranceIDThird")
	client_insuranceNameThird          = Request.Form("client_insuranceNameThird")
	client_insurancePolicyNumberThird  = Request.Form("client_insurancePolicyNumberThird")

<!-- Start Patient Information Validation -->

if client_form = "info" or client_action = "Add" then
  If client_firstname = "" OR len(client_firstname) <=2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "Please enter the first name." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_firstname) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The first name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_middleinitial) > 1 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The middle initial is too long.  Maximum length is 1 character." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If client_lastname = "" OR len(client_lastname) <=2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The last name is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_lastname) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The last name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_address) = "" or len(client_address) <=2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The address is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_address) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The address is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_city) = "" or len(client_city) <=2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The city or town is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_city) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The city or town is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_state) = "" or len(client_state) <2 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The state or province is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_state) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The state or province is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_postalCode) = "" or len(client_postalCode) <5 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The Zip/Postal code is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_postalCode) > 10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The Zip/Postal code is too long.  Maximum length is 10 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_phoneNumber) = "" or len(client_phoneNumber) <12 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The phone number is a required field. Please enter as ###-###-####." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_phoneNumber) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The phone number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_phoneCellular) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The cellular phone number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_dateofbirth) = "" or len(client_dateofbirth) <10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The date of birth is a required field. Please enter as MM/DD/YYYY." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_dateofbirth) > 10 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The date of birth is too long. Please enter as MM/DD/YYYY." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_age) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The age is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If client_age = "" OR len(client_age) > 3 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The age is incorrect.  Maximum length is 3 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_ssn) <> 11 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The social security number format is incorrect.  Please enter as ###-##-####." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_physicianName) > 50 then
 	displayHeader()
	Response.Write "<font size='4'><b>" & "The primary care physician name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If client_physicianIDRef = "" then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The referring physician name is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_physicianNameRef) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The referring physician name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If client_physicianIDRend = "" then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The rendering physician name is a required field." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_physicianNameRend) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The refendering physician name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If client_pharmacyID = "" then
    client_pharmacyID = 0
  end if
  If len(client_pharmacyName) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The pharmacy name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_pharmacyPhoneNumber) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The pharmacy phone number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If len(client_pharmacyPhoneFax) > 20 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The pharmacy fax number is too long.  Maximum length is 20 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
  If client_gender = "" then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The gender is not selected." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
  end if
end if
<!-- End Patient Information Validation -->

<!-- Start Patient Insurance Validation -->
If len(client_insuranceName) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(client_insurancePolicyNumber) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The insurance policy number is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(client_insuranceNameSecond) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The second insurance name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(client_insurancePolicyNumberSecond) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The second insurance policy number is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(client_insuranceNameThird) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The third insurance name is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if
If len(client_insurancePolicyNumberThird) > 50 then
	displayHeader()
	Response.Write "<font size='4'><b>" & "The third insurance policy number is too long.  Maximum length is 50 characters." & "</b><br /><br />" & "<font size='2'>" & "Press your browser's " & "<b>back button</b> " & "to return to form." & "<br /><br />"
	Response.Write "<hr color='#cccccc' size='5' noshade>"
	Response.End
end if

if client_form = "info" or client_action = "Add" then
    MySQL = "SELECT * FROM users"

    intClientsID = 0
    intClientsHighRecord = 0
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn

    Do While Not MyRS.EOF
      intClientsID = intClientsID + 1
      if intClientsHighRecord < MyRS("uid") then intClientsHighRecord = MyRS("uid")

    MyRS.MoveNext 
    Loop 

    if intClientsID < 1 then
      intClientsID = 1
      intClientsHighRecord = 1
    else
      intClientsID = intClientsID + 1
      intClientsHighRecord = intClientsHighRecord + 1
    end if

    ''response.write "Current Client #: " & intClientsID & " High Record #: " & intClientsHighRecord

    MyRS.Close
    set MyRS = Nothing
    if request.form("clientsubmit") = "Submit" then
      MySQL = "SELECT users.*, patients.pid, patients.pmcId, patients.doctorid, patients.refprid, patients.rendprid FROM users INNER JOIN patients ON users.uid = patients.pid"
    else 
      MySQL = "SELECT users.*, patients.pid, patients.pmcId, patients.doctorid, patients.refprid, patients.rendprid FROM users INNER JOIN patients ON users.uid = patients.pid where users.uid = " & client_id
    end if

    Set rsClient = Server.CreateObject("ADODB.Recordset") 
    rsClient.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

      if request.form("clientsubmit") = "Submit" then
        rsClient.AddNew
        rsClient("uid")          = intClientsHighRecord
        rsClient("cdate")        = Date & " " & Time
        client_id                = intClientsHighRecord
        rsClient("usertype")     = 3
        rsClient("delflag")      = 0
        ''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
        ''Delflag: 0-Active, 1-Deleted

        ''Client Add Logging
        description = "Client add #"
      end if

      rsClient("ufname")         = client_firstname
      rsClient("uminitial")      = client_middleinitial
      rsClient("ulname")         = client_lastname
      rsClient("upaddress")      = client_address
      rsClient("upcity")         = client_city
      rsClient("upstate")        = client_state
      rsClient("zipcode")        = client_postalCode
      rsClient("upphone")        = client_phoneNumber
      if client_phoneCellular <> "" then rsClient("umobileno")      = client_phoneCellular
      rsClient("dob")            = client_dateofbirth
      rsClient("sex")            = client_gender
      rsClient("ssn")            = client_ssn
      rsClient("pid")            = client_id                        ''Part of Patients Table
      rsClient("doctorid")       = client_physicianID               ''Part of Patients Table
      rsClient("RefPrID")        = client_physicianIDRef            ''Part of Patients Table
      rsClient("RendPrID")       = client_physicianIDRend           ''Part of Patients Table
      rsClient("pmcid")          = client_pharmacyID                ''Part of Patients Table
    rsClient.Update

    rsClient.close
    set rsClient = nothing

    ''Client Add/Modify Logging
    if description = "Client add #" then 
      description = "Client add #" & intClientsHighRecord & ": " & client_lastname & ", " & client_firstname
    else
      description = "Client modify #" & Request.Form("client_id") & ": " & client_lastname & ", " & client_firstname
    end if
    test = activity(session("username"),description)
    description = ""
end if

debugging = false

    MySQL = "SELECT DISTINCTROW insurancedetail.insid, insurance.insuranceName, insurancedetail.subscriberno, insurancedetail.pid  FROM insurancedetail LEFT JOIN insurance ON insurancedetail.insid=insurance.insId WHERE (((insurancedetail.pid)=" & client_id & "))"
    if debugging then response.write "MySQL: " & MySQL & "<br />"
    if debugging then response.write "Request.Form: " & request.form & "<br />"
    if debugging then response.write "client_insuranceID / Policy: " & client_insuranceID & " / " & client_insurancePolicyNumber & "<br />" 
    if debugging then response.write "client_insuranceIDSecond / Policy: " & client_insuranceIDSecond & " / " & client_insurancePolicyNumberSecond & "<br />" 
    if debugging then response.write "client_insuranceIDThird / Policy: " & client_insuranceIDThird & " / " & client_insurancePolicyNumberThird & "<br />" 

    Set MyINS = Server.CreateObject("ADODB.Recordset") 
    MyINS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

    for intClientINS = 1 to 3
    if debugging then response.write "intClientINS: " & intClientINS & "<br />"    
      if intClientINS = 1 then
        if debugging then response.write "intClient=1<br />" 
        if client_insuranceID <> "" then 
          if debugging then response.write "client_insuranceID is not blank:" & client_insuranceID & "<br />" 
          if MyINS.EOF then
            if debugging then response.write "MyINS.EOF = true -- adding new record<br />" 
            MyINS.AddNew
            MyINS("pid") = client_id
            MyINS("insid") = client_insuranceID
            MyINS("subscriberno") = client_insurancePolicyNumber
            MyINS.Update
          else
            if debugging then response.write "MyINS.EOF = false<br />" 
            insuranceID = MyINS("insid")
            insurancePolicyNumber = MyINS("subscriberno")
            if debugging then response.write "insuranceID: " & insuranceID & "<br />"
            if debugging then response.write "insurancePolicyNumber: " & insurancePolicyNumber & "<br />" 
            if client_insuranceID <> insuranceID then 
              if debugging then response.write " client_insuranceID <> insuranceID: " & client_insuranceID & " / " & insuranceID & "<br />"
              if client_insuranceID = "None" then
                if debugging then response.write "client_insuranceID = None<br />"
                MySQL = "delete * from insurancedetail where ((insurancedetail.pid=" & client_id & ") and (insurancedetail.insid = " & insuranceID & "))"
                Set MyINSdelete = Server.CreateObject("ADODB.Recordset") 
                MyINSdelete.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText
                if debugging then response.write "Deleted Record: " & MySQL & "<br />"
              else
                MyINS("insid") = client_insuranceID
                MyINS.Update
                if debugging then response.write "insuranceID Updated: " & client_insuranceID & "<br />"
              end if
            end if
            if client_insurancePolicyNumber <> insurancePolicyNumber then 
              if debugging then response.write "client_insurancePolicyNumber <> insurancePolicyNumber: " & client_insurancePolicyNumber & insurancePolicyNumber & "<br />"
              if client_insurancePolicyNumber <> "" then
                if debugging then response.write "client_insurancePolicyNumber <> Blank: " & client_insurancePolicyNumber & "<br />"
                MyINS("subscriberno") = client_insurancePolicyNumber
                MyINS.Update
                if debugging then response.write "client_insurancePolicyNumber Updated: " & client_insurancePolicyNumber & "<br />"
              end if
            end if
          end if
        end if
      end if
      if intClientINS = 2 then
        if debugging then response.write "intClient=2<br />" 
        if client_insuranceIDSecond <> "" then 
          if debugging then response.write "client_insuranceIDSecond is not blank:" & client_insuranceIDSecond & "<br />" 
          if MyINS.EOF then
            if debugging then response.write "MyINS.EOF = true -- adding new record<br />"
            MyINS.AddNew
            MyINS("pid") = client_id
            MyINS("insid") = client_insuranceIDSecond
            MyINS("subscriberno") = client_insurancePolicyNumberSecond
            MyINS.Update
          else
            if debugging then response.write "MyINS.EOF = false<br />"
            insuranceIDSecond = MyINS("insid")
            insurancePolicyNumberSecond = MyINS("subscriberno")
            if debugging then response.write "insuranceIDSecond: " & insuranceIDSecond & "<br />"
            if debugging then response.write "insurancePolicyNumberSecond: " & insurancePolicyNumberSecond & "<br />" 
            if client_insuranceIDSecond <> insuranceIDSecond then
              if debugging then response.write " client_insuranceIDSecond <> insuranceIDSecond: " & client_insuranceIDSecond & " / " & insuranceIDSecond & "<br />"
              if client_insuranceIDSecond = "None" then
                if debugging then response.write "client_insuranceIDSecond = None<br />"
                MySQL = "delete * from insurancedetail where ((insurancedetail.pid=" & client_id & ") and (insurancedetail.insid = " & insuranceIDSecond & "))"
                Set MyINSdelete = Server.CreateObject("ADODB.Recordset") 
                MyINSdelete.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText
                if debugging then response.write "Deleted Record: " & MySQL & "<br />"
              else
                MyINS("insid") = client_insuranceIDSecond
                MyINS.Update
                if debugging then response.write "insuranceID Updated: " & client_insuranceIDSecond & "<br />"
              end if
            end if
            if client_insurancePolicyNumberSecond <> insurancePolicyNumberSecond then 
              if debugging then response.write "client_insurancePolicyNumberSecond <> insurancePolicyNumberSecond: " & client_insurancePolicyNumberSecond & insurancePolicyNumberSecond & "<br />"
              if client_insurancePolicyNumberSecond <> "" then
                if debugging then response.write "client_insurancePolicyNumberSecond <> Blank: " & client_insurancePolicyNumberSecond & "<br />"
                MyINS("subscriberno") = client_insurancePolicyNumberSecond
                MyINS.Update
                if debugging then response.write "client_insurancePolicyNumberSecond Updated: " & client_insurancePolicyNumberSecond & "<br />"
              end if
            end if
          end if
        end if
      end if
      if intClientINS = 3 then
        if debugging then response.write "intClient=3<br />" 
        if client_insuranceIDThird <> "" then 
          if debugging then response.write "client_insuranceIDThird is not blank:" & client_insuranceIDThird & "<br />" 
          if MyINS.EOF then
            if debugging then response.write "MyINS.EOF = true -- adding new record<br />"
            MyINS.AddNew
            MyINS("pid") = client_id
            MyINS("insid") = client_insuranceIDThird
            if client_insurancePolicyNumberThird <> "" then MyINS("subscriberno") = client_insurancePolicyNumberThird
            MyINS.Update
          else
            if debugging then response.write "MyINS.EOF = false<br />"
            insuranceIDThird = MyINS("insid")
            insurancePolicyNumberThird = MyINS("subscriberno")
            if debugging then response.write "insuranceIDThird: " & insuranceIDThird & "<br />"
            if debugging then response.write "insurancePolicyNumberThird: " & insurancePolicyNumberThird & "<br />"
            if client_insuranceIDThird <> insuranceIDThird then
              if debugging then response.write " client_insuranceIDThird <> insuranceIDThird: " & client_insuranceIDThird & " / " & insuranceIDThird & "<br />"
              if client_insuranceIDThird = "None" then
                if debugging then response.write "client_insuranceIDThird = None<br />"
                MySQL = "delete * from insurancedetail where ((insurancedetail.pid=" & client_id & ") and (insurancedetail.insid = " & insuranceIDThird & "))"
                Set MyINSdelete = Server.CreateObject("ADODB.Recordset") 
                MyINSdelete.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText
                if debugging then response.write "Deleted Record: " & MySQL & "<br />"
              else
                MyINS("insid") = client_insuranceIDThird
                MyINS.Update
                if debugging then response.write "insuranceID Updated: " & client_insuranceIDThird & "<br />"
              end if
            end if
            if client_insurancePolicyNumberThird <> insurancePolicyNumberThird then 
              if debugging then response.write "client_insurancePolicyNumberThird <> insurancePolicyNumberThird: " & client_insurancePolicyNumberThird & insurancePolicyNumberThird & "<br />"
              if client_insurancePolicyNumberThird <> "" then
                if debugging then response.write "client_insurancePolicyNumberThird <> Blank: " & client_insurancePolicyNumberThird & "<br />"
                MyINS("subscriberno") = client_insurancePolicyNumberThird
                MyINS.Update
                if debugging then response.write "client_insurancePolicyNumberThird Updated: " & client_insurancePolicyNumberThird & "<br />"
              end if
            end if
          end if
        end if
      end if
    if NOT MyINS.EOF then MyINS.MoveNext 
    Next

    MyINS.close
    set MyINS = nothing

    set MyConn = Nothing

    response.write "Update Successful<br />"
    if debugging then response.write "Response.End"
    if debugging then response.end
end if

end if ''Delete Client IF STATEMENT

if request.form("attachmentsubmit") = "Delete" then

    ''Attachment Delete Logging
    attachment_id                      = Request.Form("attachment_id")    
	attachment_rid                     = Request.Form("attachment_rid")
    description = "Attachment delete #" & attachment_id & ": " & attachment_rid
    test = activity(session("username"),description)

    MySQL = "SELECT * FROM document WHERE docID = " & request.form("attachment_id") & " and FileName= '" & request.form("attachment_rid") & "'"
    ''response.write MySQL
    ''delflag = 1 means record has been deleted
    
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

    if Not MyRS.EOF then
      MyRS("delflag") = 1
      MyRS.Update
    end if

    MyRS.close
    set MyRS = nothing

    response.write "Attachment Deleted<br />"
end if

if request.form("attachmentsubmit") = "Update" then

    ''Attachment Modify Logging
    attachment_id                      = Request.Form("attachment_id")    
	attachment_rid                     = Request.Form("attachment_rid")
    description = "Attachment modify #" & attachment_id & ": " & attachment_rid
    test = activity(session("username"),description)

    MySQL = "SELECT * FROM document WHERE docID = " & request.form("attachment_id") & " and FileName= '" & request.form("attachment_rid") & "'"
    ''response.write MySQL
    ''delflag = 1 means record has been deleted
    
    Set MyRS = Server.CreateObject("ADODB.Recordset") 
    MyRS.Open MySQL, MyConn, adOpenKeyset, adLockPessimistic, adCmdText

    if Not MyRS.EOF then
      MyRS("doc_Type") = request.form("itemtype")
      if ((len(request.form("description")) < 50) and (request.form("description") <> "")) then MyRS("customName") = request.form("description")
      MyRS.Update
    end if

    MyRS.close
    set MyRS = nothing

    response.write "Attachment Updated<br />"
end if

if  request.form("clientadd") <> "Add" then
    response.write "<form action='client.asp' method=POST>"
    response.write "<div id='clientadd'>Paatient Search<br /></div>"
    response.write "<input type='text' name='client' size='30' value='' />&nbsp;&nbsp;"
    response.write "<input type='submit' name='clientsearch' value='Search' /><input type='submit' name='clientadd' value='Add'><br /></form>"
end if
%>

<!-- End Client Add -->

<!--#include file="footer.inc"-->