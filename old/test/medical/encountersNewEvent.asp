<!--#include File = "CalendarInclude.asp"-->
<!--#include file="db.asp"-->

<%
' Prevent Caching
Response.Expires = 0
Response.ExpiresAbsolute = now - 1
Response.CacheControl = "no-cache"
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_String

response.write "<head><title>Encounter Addition</title>"
response.write "<script language=javascript>"
  Response.Write "function SubmitMe()"
  Response.Write "{"
  Response.Write "	myRegExp = new RegExp(""\n"", ""g"");"
  Response.Write "	mcalNotes = document.ThisForm.encounterNotes.value.replace(""&"",""and"");"
  Response.Write "	mcalNotes = mcalNotes.replace(myRegExp,""<br />"");"
  Response.Write "window.showModalDialog('encounters_addmodifydone.asp?"
  Response.Write "encounterMonth=' + document.ThisForm.encounterMonth.value + "
  Response.Write "'&encounterDay=' + document.ThisForm.encounterDay.value + "
  Response.Write "'&encounterYear=' + document.ThisForm.encounterYear.value + "
  Response.Write "'&encounterClientID=' + document.ThisForm.encounterClientID.value + "
  Response.Write "'&encounterStaff=' + document.ThisForm.encounterStaff.value + "
  Response.Write "'&encounterStaffID=' + document.ThisForm.encounterStaffID.value + "
  Response.Write "'&encounterAssignedTo=' + document.ThisForm.encounterAssignedTo.value + "
  ''Response.Write "'&encounterAssignedToID=' + document.ThisForm.encounterAssignedToID.value + "
  Response.Write "'&encounterStatus=' + document.ThisForm.encounterStatus.value + "
  Response.Write "'&encountertask=' + document.ThisForm.encountertask.value + "
  Response.Write "'&encounterNotes=' + mcalNotes,"
  Response.Write "'Updating', 'scroll: yes help: yes; status: yes; dialogWidth: 500px; dialogHeight: 250px;');"
  Response.Write "}"
  Response.Write "function UpdateMe()"
  Response.Write "{"
  Response.Write "	myRegExp = new RegExp(""\n"", ""g"");"
  Response.Write "	mcalNotes = document.ThisForm.encounterNotes.value.replace(""&"",""and"");"
  Response.Write "	mcalNotes = mcalNotes.replace(myRegExp,""<br />"");"
  Response.Write "window.showModalDialog('encounters_addmodifydone.asp?"
  Response.Write "encounterMonth=' + document.ThisForm.encounterMonth.value + "
  Response.Write "'&encounterDay=' + document.ThisForm.encounterDay.value + "
  Response.Write "'&encounterYear=' + document.ThisForm.encounterYear.value + "
  Response.Write "'&encounterClientID=' + document.ThisForm.encounterClientID.value + "
  Response.Write "'&encounterStaff=' + document.ThisForm.encounterStaff.value + "
  Response.Write "'&encounterStaffID=' + document.ThisForm.encounterStaffID.value + "
  Response.Write "'&encounterAssignedTo=' + document.ThisForm.encounterAssignedTo.value + "
  Response.Write "'&encounterStatus=' + document.ThisForm.encounterStatus.value + "
  Response.Write "'&encountertask=' + document.ThisForm.encountertask.value + "
  Response.Write "'&encounterid=' + document.ThisForm.encounterid.value + "
  Response.Write "'&encounterNotes=' + mcalNotes,"
  Response.Write "'Updating', 'scroll: yes help: yes; status: yes; dialogWidth: 500px; dialogHeight: 250px;');"
  Response.Write "}"
  Response.Write "function CancelMe()"
  Response.Write "{"
  Response.Write "window.location = " & chr(34) & "encounters.asp" & chr(34) & ";"
  Response.Write "}"
response.write "</script>"
response.write "</head>"
response.write "<body>"

if request.querystring("encounterid") <> "" then
  MySQL = "SELECT * FROM encounters WHERE encounterid = '" & request.querystring("encounterid") & "'"
  ''response.write MySQL
  MyConn2 = DB_SCHstring
  Set MyRS = Server.CreateObject("ADODB.Recordset") 
  MyRS.Open MySQL, MyConn2

  if MyRS.EOF then
    response.write "No Matches"
    response.end

  else

    ''Start Encounters List Database Mapping
    encounter_id            = MyRS("encounterid")
    encounter_clientID      = MyRS("encounterclientID")
    encounter_client        = MyRS("encounterclient")
    encounter_month         = MyRS("encounterMonth")
    encounter_day           = MyRS("encounterDay")
    encounter_year          = MyRS("encounterYear")
    encounter_notes         = MyRS("encounternotes")
    encounter_status        = MyRS("encounterstatus")
    ''End Encounters List Database Mapping
  end if

  MyRS.Close
  set MyRS = Nothing

end if

mMonth = Request.Item("calMonth")
if mMonth = "" then mMonth = Month(date)
mDay = Request.Item("calDay")
if mDay = "" then mDay = Day(date)
mYear = Request.Item("calYear")
if mYear = "" then mYear = Year(date)
response.write "<form ID=ThisForm Name=ThisForm>"
response.write "<input type=hidden name=calDay value=" & mDay & ">"
response.write "<input type=hidden name=calMonth value=" & mMonth & ">"
response.write "<input type=hidden name=calYear value=" & mYear & ">"
response.write "<input type='hidden' name='encounterStaff' value='" & session("username") & "'>"
response.write "<input type='hidden' name='encounterStaffID' value='" & session("userid")  & "'>"
response.write "<table width='100%' height='100%' border=1 bgcolor=lightgrey>"
response.write "<tr><td height='1%'>"
response.write "<table width='100%'>"
response.write "<tr><td><b>Encounter</b></td><tr>"
response.write "<tr><td><b>Date</b></td>"
response.write "<td>"

function selected_month(data)
  if encounter_month = data then 
    selected_month = " selected='selected'"
  else
    if mMonth = data then 
      selected_month = " selected='selected'"
    end if
  end if
end function
response.write "<select name='encounterMonth'>"
response.write "  <option value='01'" & selected_month("01") & ">01 - January</option>"
response.write "  <option value='02'" & selected_month("02") & ">02 - February</option>"
response.write "  <option value='03'" & selected_month("03") & ">03 - March</option>"
response.write "  <option value='04'" & selected_month("04") & ">04 - April</option>"
response.write "  <option value='05'" & selected_month("05") & ">05 - May</option>"
response.write "  <option value='06'" & selected_month("06") & ">06 - June</option>"
response.write "  <option value='07'" & selected_month("07") & ">07 - July</option>"
response.write "  <option value='08'" & selected_month("08") & ">08 - August</option>"
response.write "  <option value='09'" & selected_month("09") & ">09 - September</option>"
response.write "  <option value='10'" & selected_month("10") & ">10 - October</option>"
response.write "  <option value='11'" & selected_month("11") & ">11 - November</option>"
response.write "  <option value='12'" & selected_month("12") & ">12 - December</option>"
response.write "</select>"

function selected_day(data)
  if encounter_day = data then 
    selected_day = " selected='selected'"
  else
    if mDay = data then 
      selected_day = " selected='selected'"
    end if
  end if
end function
response.write "<select name='encounterDay'>" 
response.write "  <option value='01'" & selected_day("01") & ">01</option>"
response.write "  <option value='02'" & selected_day("02") & ">02</option>"
response.write "  <option value='03'" & selected_day("03") & ">03</option>"
response.write "  <option value='04'" & selected_day("04") & ">04</option>"
response.write "  <option value='05'" & selected_day("05") & ">05</option>"
response.write "  <option value='06'" & selected_day("06") & ">06</option>"
response.write "  <option value='07'" & selected_day("07") & ">07</option>"
response.write "  <option value='08'" & selected_day("08") & ">08</option>"
response.write "  <option value='09'" & selected_day("09") & ">09</option>"
response.write "  <option value='10'" & selected_day("10") & ">10</option>"
response.write "  <option value='11'" & selected_day("11") & ">11</option>"
response.write "  <option value='12'" & selected_day("12") & ">12</option>"
response.write "  <option value='13'" & selected_day("13") & ">13</option>"
response.write "  <option value='14'" & selected_day("14") & ">14</option>"
response.write "  <option value='15'" & selected_day("15") & ">15</option>"
response.write "  <option value='16'" & selected_day("16") & ">16</option>"
response.write "  <option value='17'" & selected_day("17") & ">17</option>"
response.write "  <option value='18'" & selected_day("18") & ">18</option>"
response.write "  <option value='19'" & selected_day("19") & ">19</option>"
response.write "  <option value='20'" & selected_day("20") & ">20</option>"
response.write "  <option value='21'" & selected_day("21") & ">21</option>"
response.write "  <option value='22'" & selected_day("22") & ">22</option>"
response.write "  <option value='23'" & selected_day("23") & ">23</option>"
response.write "  <option value='24'" & selected_day("24") & ">24</option>"
response.write "  <option value='25'" & selected_day("25") & ">25</option>"
response.write "  <option value='26'" & selected_day("26") & ">26</option>"
response.write "  <option value='27'" & selected_day("27") & ">27</option>"
response.write "  <option value='28'" & selected_day("28") & ">28</option>"
response.write "  <option value='29'" & selected_day("29") & ">29</option>"
response.write "  <option value='30'" & selected_day("30") & ">30</option>"
response.write "  <option value='31'" & selected_day("31") & ">31</option>"
response.write "</select>"

function selected_year(data)
  if encounter_year = data then 
    selected_year = " selected='selected'"
  else
    if mYear = data then 
      selected_year = " selected='selected'"
    end if
  end if
end function
response.write "<select name='encounterYear'>"
response.write "  <option value='2010'" & selected_year("2010") & ">2010</option>"
response.write "  <option value='2011'" & selected_year("2011") & ">2011</option>"
response.write "  <option value='2012'" & selected_year("2012") & ">2012</option>"
response.write "</select>"
response.write "</td>"

function selected_status(data)
  if request.querystring("task") = "done" then
   if data = "Done" then selected_status = " selected='selected'"
  else
    if encounter_status = data then 
      selected_status = " selected='selected'"
    end if
  end if
end function

response.write "<td align=right><b>Status: </b>"
response.write "<select name='encounterStatus'>"
response.write "  <option value='Open'" & selected_status("Open") & ">Open</option>"
response.write "  <option value='Done'" & selected_status("Done") & ">Done</option>"
response.write "</select>"
response.write "</td>"
response.write "</tr>"           


response.write "<tr><td><b>Client:</b></td>"
response.write "<td><select name='encounterClientID'>" & vbCrLF
response.write "<option value='0'>None</option>"

   usertype_id = 3
   delflag_id = 0
   MySQL = "SELECT * FROM users WHERE (usertype = " & usertype_id & ") and (delflag = " & delflag_id & ") ORDER BY ulname, ufname"
   ''response.write MySQL
   ''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
   field_acctno   = true
   field_name     = true
   field_dob      = true
   field_phone    = true
   field_location = true

   intClientID = 0
   Set MyRS = Server.CreateObject("ADODB.Recordset") 
   MyRS.Open MySQL, MyConn

   if MyRS.EOF then
     response.write ""
   end if
  
   Do While Not MyRS.EOF

     ''Start Client List Database Mapping
     client_id            = MyRS("uid")
     client_firstname     = MyRS("ufname")
     client_middleinitial = MyRS("uminitial")
     client_lastname      = MyRS("ulname")
     client_address       = MyRS("upaddress")
     client_city          = MyRS("upcity")
     client_state         = MyRS("upstate")
     client_postalcode    = MyRS("zipcode")
     client_phonenumber   = MyRS("upPhone")
     client_dateofbirth   = MyRS("dob")
     client_gender        = MyRS("sex")
     client_ssn           = MyRS("ssn")
     ''End Client List Database Mapping

     response.write "<option value='" & client_id
     if right("00000" & encounter_clientID, 5) = right("00000" & client_id, 5) then response.write "' selected='selected"
     response.write "'>" & client_lastname & ", " & client_firstname & " " & client_middleinitial & "</option>" & vbCrLf


   MyRS.MoveNext 
   Loop 

   MyRS.Close
   set MyRS = Nothing
response.write "</select>"
response.write "</td>"

function selected_assignedto(data)
  if request.querystring("assignedto") = "Boggs, Jennifer" then
   if data = "Done" then selected_assignedto = " selected='selected'"
  else
    if encounter_assignedto = data then 
      selected_assignedto = " selected='selected'"
    end if
  end if
end function

response.write "<td align=right><b>Assigned To: </b>"
response.write "<select name='encounterAssignedTo'>"
response.write "  <option value='Boggs, Jennifer'" & selected_assignedto("Boggs, Jennifer") & ">Jenn</option>"
response.write "  <option value='Hermann, Carrie'" & selected_assignedto("Hermann, Carrie") & ">Carrie</option>"
response.write "  <option value='Dublanc, Sharon'" & selected_assignedto("Dublanc, Sharon") & ">Sharon</option>"
response.write "  <option value='Scelsi, Michelle'" & selected_assignedto("Scelsi, Michelle") & ">Michelle</option>"
response.write "  <option value='Sullivan, James'" & selected_assignedto("Sullivan, James") & ">Sullivan, MD</option>"
response.write "  <option value='Goode, Randall'" & selected_assignedto("Goode, Randall") & ">Goode, MD</option>"
response.write "  <option value='Scott, Mike'" & selected_assignedto("Scott, Mike") & ">Scott, PA</option>"
response.write "</select>"
response.write "</td>"

response.write "</tr>"
response.write "<tr><td colspan=3 align=left><b>Notes:</b><br />"
response.write "<textarea name=encounterNotes cols=65 rows=10>"
if encounter_notes <> "" then response.write encounter_notes
response.write "</textarea></td></tr>"
response.write "</table></td></tr>"
response.write "<tr><td valign=top align=middle><input type=button value='Cancel' onClick='CancelMe()'> "
if encounter_id = "" then
  response.write "<input type='hidden' name='encountertask' value='add'>"
  response.write "<input type='hidden' name='encounterstatus' value=''>"
  response.write "<input type='button' value='Add to Encounters' onClick='SubmitMe()'></td></tr></table>"
  ''response.write "<input type='submit'>"
else
  if request.querystring("task") = "done" then
    response.write "<input type='hidden' name='encountertask' value='done'>"
    response.write "<input type='hidden' name='encounterid' value='" & encounter_id & "'>"
    response.write "<input type='button' value='Save Changes' onClick='UpdateMe()'></td></tr></table>"
  else
    if request.querystring("task") = "modify" then  
      response.write "<input type='hidden' name='encountertask' value='modify'>"
      response.write "<input type='hidden' name='encounterid' value='" & encounter_id & "'>"
      response.write "<input type='button' value='Save Changes' onClick='UpdateMe()'></td></tr></table>"
    end if
  end if
end if
%>