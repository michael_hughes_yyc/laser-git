<!--#include file="db.asp"-->
<!--#include File="CalendarInclude.asp"-->


<%
' Prevent Caching
Response.Expires = 0
Response.ExpiresAbsolute = now - 1
Response.CacheControl = "no-cache"
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

' Event Calendar Application
set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_String

set CONNECT = server.CreateObject("ADODB.Connection")
Connect.Open = DB_SCHstring
varEventID = Request.Item("EventID")

SQL = "Select * from schEvents where scheduleID = '" & varEventID & "'"
''response.write SQL
set mDATA=Connect.Execute(SQL)

if mDATA.EOF then
  response.write "No Matches"
  response.end
else
  Response.Write "<head><title>Event Details</title>"
  Response.Write "<script language=javascript>"
  Response.Write "function SubmitMe()"
  Response.Write "{"
  Response.Write "	myRegExp = new RegExp(""\n"", ""g"");"
  Response.Write "	mcalScheduleID = document.ThisForm.scheduleID.value.replace(""&"",""and"");"
  Response.Write "	mcalScheduleID = mcalScheduleID.replace(myRegExp,""<br />"");"

  Response.Write "	mcalClientID = document.ThisForm.scheduleClientID.value.replace(""&"",""and"");"
  Response.Write "	mcalClientID = mcalClientID.replace(myRegExp,""<br />"");"
  Response.Write "	mcalNotes = document.ThisForm.scheduleNotes.value.replace(""&"",""and"");"
  Response.Write "	mcalNotes = mcalNotes.replace(myRegExp,""<br />"");"
  Response.Write "window.showModalDialog('ScheduleUpdateDetails.asp?scheduleID=' + document.ThisForm.scheduleID.value +"
  Response.Write "'&scheduleStartTime=' + document.ThisForm.scheduleStartTime.value + "
  Response.Write "'&scheduleEndTime=' + document.ThisForm.scheduleEndTime.value + "
  Response.Write "'&scheduleClientID=' + document.ThisForm.scheduleClientID.value + "
  Response.Write "'&scheduleNotes=' + mcalNotes,"
  Response.Write "'Updating', 'scroll: no help: no; status: no; dialogWidth: 205px; dialogHeight: 100px;');"
  Response.Write "	window.close();"
  Response.Write "}"
  Response.Write "function DeleteMe(ID)"
  Response.Write "{"
  Response.Write "	if(confirm(""Are you sure you want to delete this event?""))"
  Response.Write "	{"
  Response.Write "		window.showModalDialog('ScheduleDeleteEvent.asp?scheduleID=' + ID,'Deleting', 'scroll: no; help: no; status: no; dialogWidth: 205px; dialogHeight: 100px;');"
  Response.Write "		window.close();"
  Response.Write "	}"
  Response.Write "}"
  Response.Write "function PrintMe(ID)"
  Response.Write "{"
  Response.Write "	window.print();"
  Response.Write "}"
  Response.Write "</script>"
  Response.Write "</head>"
  Response.Write "<body>"

  Response.Write "<form ID=ThisForm Name=ThisForm action=CalendarUpdateDetails.asp method=post>"
  Response.Write "<input type='hidden' name='scheduleID' value='" & mData("scheduleID") & "'>"
  Response.Write "<table width=500 height='100%' border=1 bgcolor=lightblue>"
  Response.Write "<tr><td height='1%'><table width='100%'>"
  Response.Write "<tr><td><b>Date:</td>"
  Response.Write "<td><b>" & MonthName(mData("scheduleMonth")) & " " & mData("scheduleDay") & ", " & mData("scheduleYear") & "</b></td></tr>"
  Response.Write "<tr><td align=left><b>Client: </b></td>"
  
  Response.Write "<td>"
  Response.Write "<select name='scheduleClientID'>" & vbCrLF
  if not isNull(mDATA("scheduleClientID")) then
    MySQL2 = "SELECT * FROM clients WHERE clientID = '" & mDATA("scheduleClientID") & "'"
    Set MyRS2 = Server.CreateObject("ADODB.Recordset") 
    MyRS2.Open MySQL2, MyConn
    if MyRS2.EOF then
      response.write "<option value=''>...</option>"  & vbCrLF
    else  
      response.write  "<option value='" & MyRS2("clientID") & "'>" & MyRS2("client_name") & "</option>" & vbCrLF
    end if
    MyRS2.Close
    set MyRS2 = Nothing
  else
    response.write "<option value=''>...</option>"  & vbCrLF
  end if


  MySQL = "SELECT DISTINCT clientID, client_name FROM clients ORDER BY client_name"
  Set MyRS = Server.CreateObject("ADODB.Recordset") 
  MyRS.Open MySQL, MyConn
  if MyRS.EOF then
    response.write "<option value=''>...</option>"  & vbCrLF
  else
    Do While Not MyRS.EOF
      if MyRS("clientID") <> mDATA("scheduleClientID") then
        MySQL2 = "SELECT * FROM clients WHERE clientID = '" & MyRS("clientID") & "'"
        Set MyRS2 = Server.CreateObject("ADODB.Recordset") 
        MyRS2.Open MySQL2, MyConn
        response.write  "<option value='" & MyRS2("clientID") & "'>" & MyRS2("client_name") & "</option>" & vbCrLF
        MyRS2.Close
        set MyRS2 = Nothing
      end if
    MyRS.MoveNext 
    Loop
  end if 
  response.write "</select>"
  response.write "</td></tr>"

  Response.Write "<tr><td align=left NOWRAP><b>Time:</b></td>"
  Response.Write "<td><select name=scheduleStartTime>"
  Response.Write Replace(mDateList, mData("scheduleStartTime"), mData("scheduleStartTime") & " selected")
  Response.Write "</select> to "
  Response.Write "<Select name=scheduleEndTime>"
  Response.Write Replace(mDateList, mData("scheduleEndTime"), mData("scheduleEndTime") & " selected")
  Response.Write "</select></td></tr>"
  Response.Write "<tr><td colspan=2 align=left><b>Notes:</b><br />"
  Response.Write "<textarea name='scheduleNotes' cols=57 rows=10>" & mData("scheduleNotes") & "</textarea></td></tr>"
  Response.Write "</table></td></tr>"
  Response.Write "<tr><td align=middle valign=top><input type=button value='Close' onClick='window.close()'> "
  Response.Write " <input type=button value='Save Changes' onClick='SubmitMe()'>"
  Response.Write " <input type=button value='Delete Event' onclick='DeleteMe(" & chr(34) & mData("scheduleID") & chr(34) & ")'>"
  Response.Write " <input type=button value='Print' onClick='PrintMe(" & chr(34) & mData("scheduleID") & chr(34) & ")'></td></tr></table>"
end if
%>	