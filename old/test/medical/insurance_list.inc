<!-- Start Insurance List -->
<%
if (request.form("insuranceadd") <> "Add") and (request.form("insurancesearch") = "Search" or request.form("insurance") <> "") then

   search_id = request.form("insurance")
   deleteflag_id = 0
   MySQL = "SELECT * FROM insurance WHERE ((insurancename like '" & search_id & "%' or insurancecity like '%" & search_id & "%' or insurancestate like '%" & search_id & "%') and (deleteflag = " & deleteflag_id & ")) ORDER BY insurancename"
   ''response.write MySQL
   ''deleteflag: 0-Active, 1-Deleted
   field_acctno   = true
   field_name     = true
   field_address  = true
   field_location = true
   field_phone    = true
   field_fax      = true

   intInsuranceID = 0
   Set MyRS = Server.CreateObject("ADODB.Recordset") 
   MyRS.Open MySQL, MyConn

   if MyRS.EOF then
     response.write "No Matches"
     response.end
   end if
  
   response.write "<table class='insurance'>" & vbCrLf
   response.write "  <tr>" & vbCrLf
   if field_acctno   then response.write "    <td class='smaller'><b>AcctNo</b></td>" & vbCrLf
   if field_name     then response.write "    <td width='100'><b>Name</b></td>" & vbCrLf
   if field_address  then response.write "    <td width='100'><b>Address</b></td>" & vbCrLf
   if field_location then response.write "    <td width='100'><b>Location</b></td>" & vbCrLf
   if field_phone    then response.write "    <td width='100'><b>Phone</b></td>" & vbCrLf
   if field_fax      then response.write "    <td width='100'><b>Fax</b></td>" & vbCrLf 
   response.write "  </tr>" & vbCrLf

   Do While Not MyRS.EOF

     ''Start Insurance List Database Mapping
     insurance_id            = MyRS("insid")
     insurance_name          = MyRS("insurancename")
     insurance_address       = MyRS("insuranceaddress")
     insurance_city          = MyRS("insurancecity")
     insurance_state         = MyRS("insurancestate")
     insurance_postalcode    = MyRS("insurancezip")
     insurance_phonenumber   = MyRS("insurancephone")
     insurance_phonefax      = MyRS("insurancefax")
     ''End Insurance List Database Mapping

     intInsuranceID = intInsuranceID + 1
     response.write "  <tr>" & vbCrLf
     if field_acctno   then response.write "    <td  class='smaller'><a href='insurance.asp?id=" & insurance_id & "' class='textlinks'>" & right("00000" & insurance_id, 5) & "</a></td>" & vbCrLf
     if field_name     then response.write "    <td>" & insurance_name & "</td>"  & vbCrLf
     if field_address  then response.write "    <td>" & insurance_address & "</td>" & vbCrLf
     if field_location then
       if (len(insurance_city) > 1) and (len(insurance_state) > 1) then
         response.write "    <td>" & insurance_city & ", " & insurance_state & "</td>" & vbCrLf
       else
         response.write "    <td> </td>"
       end if
     end if
     if field_phone    then response.write "    <td>" & insurance_phonenumber & "</td>" & vbCrLf
     if field_fax      then response.write "    <td>" & insurance_phonefax &  "</td>" & vbCrLf
     response.write "  </tr>" & vbCrLf

   MyRS.MoveNext 
   Loop 

   response.write "  <tr>" & vbCrLf
   response.write "    <td colspan='2'>" & right("00000" & intInsuranceID, 5) & " Insurance Providers Found</td>" & vbCrLf
   response.write "  </tr>" & vbCrLf

   response.write "</table>" & vbCrLf

   if intInsuranceID < 1 then
     intInsuranceID = 1
   else
     intInsuranceID = intInsuranceID + 1
   end if

   ''response.write "Current Application #: " & intInsuranceID

   MyRS.Close
   set MyRS = Nothing
end if
%>
<!-- End Insurance List -->