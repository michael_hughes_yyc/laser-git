<% 
''option explicit 
''Response.Expires = -1
''Server.ScriptTimeout = 600
%>
<!--#include file="func_aspupload.inc" -->
<%
dim uploadsDirVar, pid, iCtr, sChar, sID, activityfilename
uploadsDirVar = "f:\mobiledoc\" 

Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

function OutputForm()
%>
    <form name="frmSend" method="POST" enctype="multipart/form-data" action="upload.asp" onSubmit="return onSubmitForm();">
    <input type="hidden" name="pid" value="<%= pid %>">
    <input type="hidden" name="rid" value="<%= RandomID %>">
    <input type="hidden" name="userid" value="<%= userid %>">
    File:<br />
    <input type="file" name="attach1" size=35><br />
<!--
    File 2: <input name="attach2" type="file" size=35><br />
    File 3: <input name="attach3" type="file" size=35><br />
    File 4: <input name="attach4" type="file" size=35><br />
-->
    <br /> 
    <input type=submit value="Upload">
    </form>
<%
end function

function TestEnvironment()
    Dim fso, fileName, testFile, streamTest
    TestEnvironment = ""
    Set fso = Server.CreateObject("Scripting.FileSystemObject")
    if not fso.FolderExists(uploadsDirVar) then
        TestEnvironment = "<B>Folder " & uploadsDirVar & " does not exist.</B><br />The value of your uploadsDirVar is incorrect. Open uploadTester.asp in an editor and change the value of uploadsDirVar to the pathname of a directory with write permissions."
        exit function
    end if
    fileName = uploadsDirVar & "\test.txt"
    on error resume next
    Set testFile = fso.CreateTextFile(fileName, true)
    If Err.Number<>0 then
        TestEnvironment = "<B>Folder " & uploadsDirVar & " does not have write permissions.</B><br />The value of your uploadsDirVar is incorrect. Open uploadTester.asp in an editor and change the value of uploadsDirVar to the pathname of a directory with write permissions."
        exit function
    end if
    Err.Clear
    testFile.Close
    fso.DeleteFile(fileName)
    If Err.Number<>0 then
        TestEnvironment = "<B>Folder " & uploadsDirVar & " does not have delete permissions</B>, although it does have write permissions.<br />Change the permissions for IUSR_<I>computername</I> on this folder."
        exit function
    end if
    Err.Clear
    Set streamTest = Server.CreateObject("ADODB.Stream")
    If Err.Number<>0 then
        TestEnvironment = "<B>The ADODB object <I>Stream</I> is not available in your server.</B><br />Check the Requirements page for information about upgrading your ADODB libraries."
        exit function
    end if
    Set streamTest = Nothing
end function

function SaveFiles
    Dim Upload, fileName, fileSize, ks, i, fileKey, completefilename

    Set Upload = New FreeASPUpload
    fileName = Upload.Form("pid") & "_" & Upload.Form("rid") & "_" & fileName
    Upload.Save(uploadsDirVar)

	' If something fails inside the script, but the exception is handled
	If Err.Number<>0 then Exit function

    SaveFiles = ""
    ks = Upload.UploadedFiles.keys
    if (UBound(ks) <> -1) then
        SaveFiles = "<B>Files uploaded:</B> "
        for each fileKey in Upload.UploadedFiles.keys
            completefilename = Upload.Form("pid") & "_" & Upload.Form("rid") & "_" & Upload.UploadedFiles(fileKey).FileName
            activityfilename = completefilename 
            pid = Upload.Form("pid")

            %>
            <!--#include file="client_upload_database_addition.inc"-->
            <%

            SaveFiles = SaveFiles & Upload.UploadedFiles(fileKey).FileName & " (" & Upload.UploadedFiles(fileKey).Length & "B) "
        next
    else
        SaveFiles = "The file name specified in the upload form does not correspond to a valid file in the system."
    end if
end function
%>

<script>
function onSubmitForm() {
    var formDOMObj = document.frmSend;
    if (formDOMObj.attach1.value == "" && formDOMObj.attach2.value == "" && formDOMObj.attach3.value == "" && formDOMObj.attach4.value == "" )
        alert("Please press the Browse button and pick a file.")
    else
        return true;
    return false;
}
</script>
<!--#include file="func_activity.inc"-->

<%
Dim diagnostics
if Request.ServerVariables("REQUEST_METHOD") <> "POST" then
    diagnostics = TestEnvironment()
    if diagnostics<>"" then
        response.write diagnostics
        response.write "After you correct this problem, reload the page."
    else
        OutputForm()
    end if
else
    OutputForm()
    response.write SaveFiles()
    ''Attachment Add Logging
    description = "Attachment add - client #" & pid & ": " & activityfileName
    test = activity(session("username"),description)
    description = ""
    response.redirect "client.asp?pid=" & pid
end if

%>