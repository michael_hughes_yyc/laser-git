            <!-- Start Patient Information Form -->
            <table width="100%" >
            <tr>
                <td colspan="2"><strong>Client Medication</strong></td>
			</tr>
			<tr>
                <td width="35%">Name: (ID:<%= right("00000" & client_id, 5) %>)</td>
                <td width="65%">
                <input type="text" name="client_firstname" size="25" value="<%= client_firstname %>">
                <input type="text" name="client_middleinitial" size="5" value="<%= client_middleinitial %>">
                <input type="text" name="client_lastname" size="25" value="<%= client_lastname %>">
                </td>
            </tr>
            <tr>
                <td>Telephone: </td>
                <td>
                <input type="text" name="client_phoneNumber" size="25" value="<%= client_phoneNumber %>">
                </td>
            </tr>
            <tr>
                <td>Date of Birth: </td>
                <td>
                <input type="text" name="client_dateofbirth" size="25" value="<%= client_dateofbirth %>">
                </td>
            </tr>
            </table>
            </form>
            <!-- End Patient Information Form -->

            <!-- Start Patient Authorization -->

            <form action="client_authlog_add.asp" method="post">
            <table width="100%" >
            <tr>
                <td colspan="7"><strong>Authorization</strong></td>
			</tr>
			<tr>
                <td width="10%">Entry Date</td>
                <td width="25%">Insurance</td>
                <td width="15%">CDPC Rep</td>
                <td width="10%">Ins Rep Name</td>
                <td width="10%">Ref #</td>
                <td width="10%">Status</td>
                <td width="30%">Comments</td>
            </tr>

              <!--#include file="client_authlog_view.inc"-->
              <% 
                varMonth = right("00" & left(date, inStr(date, "/")-1), 2)
                varDay   = right("00" & left(right(date, (len(date) - inStr(date, "/"))), inStr(right(date, (len(date) - inStr(date, "/"))), "/")-1), 2)
                varYear  = right("0000" & date,4)
                varDate  = varMonth & "/" & varDay & "/" & varYear
               %>

			<tr>
                <td width="10%"><input type="text" name="authorization_date" size="10" value="<%= varDate %>"></td>
                <td width="25%">
                  <select name="client_insuranceID">
                  <option value='<%= client_insuranceID %>,<%= client_insuranceName %>' selected='selected'><%= client_insuranceName %></option>
                  <option value='0,None'>None</option>
                  <!--#include file="insurance_select_auth.inc"-->
                  </select>
                </td>
                <td width="15%"><input type="text" name="authorization_rep" size="15" value="<%= session("username") %>"></td>
                <td width="10%"><input type="text" name="authorization_insurance_rep" size="15" value=""></td>
                <td width="10%"><input type="text" name="authorization_insurance_referencenumber" size="15" value=""></td>
                <td width="10%">
                  <select name="authorization_status">
                  <option value='Pending' selected='selected'>Pending</option>
                  <option value='Authorized'>Authorized</option>
                  </select>
                </td>
                <td width="30%"><input type="text" name="authorization_comments" size="15" value=""><input type='submit' name='authlogadd' value='Add'></td>
            </tr>

            </table>
            <input type="hidden" name="client_id"                  value="<%= client_id %>">
            <input type="hidden" name="client_firstname"           value="<%= client_firstname %>">
            <input type="hidden" name="client_middleinitial"       value="<%= client_middleinitial %>">
            <input type="hidden" name="client_lastname"            value="<%= client_lastname %>">
            <input type="hidden" name="client_dateofbirth"         value="<%= client_dateofbirth %>">
            <input type="hidden" name="client_phoneNumber"         value="<%= client_phoneNumber %>">
            </form>
            <!-- End Patient Authorization -->
