<!--#include file="db.asp"-->

<%
' Prevent Caching
Response.Expires = 0
Response.ExpiresAbsolute = now - 1
Response.CacheControl = "no-cache"
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

Function RandomID()
    Randomize 
    for iCtr = 1 to 10
        sChar = Chr(Int((90 - 65 + 1) * Rnd) + 65)
          sID = sID & sChar
    Next
  
    sID = sID & Year(Now) & right("00" & Month(Now),2) & right("00" & Day(Now), 2) & right("00" & Hour(Now),2) & right("00" & Minute(Now),2) & right("00" & Second(Now),2) 
    RandomID = sID
End Function

Response.Write "<html><head><title>Updating Encounter</title></head><body><center>"

set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_String

usertype_id = 3
delflag_id = 0
MySQL = "SELECT * FROM users WHERE uid = " & Request.Item("encounterClientID") & " and (usertype = " & usertype_id & ") and (delflag = " & delflag_id & ")"
Set MyRS = Server.CreateObject("ADODB.Recordset") 
''response.write MySQL
''UserType: 1-Admin, 2-Employee, 3-Patient, 4-Unknown, 5-Doctor
''delflag: 0-Active, 1-Deleted
MyRS.Open MySQL, MyConn
if Not MyRS.EOF then 
  encounterClient         = MyRS("ulname") & ", " & MyRS("ufname") & " " & MyRS("uminitial")
end if
MyRS.close
set MyRS = Nothing


set MyConn = Server.CreateObject("ADODB.Connection")
MyConn.ConnectionTimeout = 15
MyConn = DB_SCHstring

if Request.Item("encountertask") = "modify" or Request.Item("encountertask") = "done" then
  Response.Write "Modifying Item....."
  Response.Flush

  SQL = "Update encounters set "
  SQL = SQL & " encounterId='" & Replace(Request.Item("encounterID"), "'", "''") & "', "
  SQL = SQL & " encounterMonth='" & Request.Item("encounterMonth") & "',"
  SQL = SQL & " encounterYear='" & Request.Item("encounterYear") & "',"
  SQL = SQL & " encounterDay='" & Request.Item("encounterDay") & "',"
  SQL = SQL & " encounterClient='" & encounterClient & "', "
  SQL = SQL & " encounterClientID='" & Request.Item("encounterClientID") & "',"
  SQL = SQL & " encounterStaff='" & Request.Item("encounterStaff") & "', "
  SQL = SQL & " encounterStaffID='" & Request.Item("encounterStaffID") & "', "
  SQL = SQL & " encounterStatus='" & Request.Item("encounterStatus") & "',"
  SQL = SQL & " encounterNotes='" & Replace(Request.Item("encounterNotes"), "'", "''") & "'"
  SQL = SQL & " Where encounterID = '" & Request.Item("encounterID") & "'"
  
  set CONNECT = server.CreateObject("ADODB.Connection")
  Connect.Open DB_SCHstring
  
  ''response.write SQL
  ''response.end
  set mData = Connect.Execute(SQL)
  set MyConn = Nothing
else
  Response.Write "Saving Item....."
  Response.Flush

  SQL = "Insert Into encounters "
  SQL = SQL & "(encounterID, encounterMonth, encounterYear, encounterDay, encounterClient, encounterClientID, encounterStaff, encounterStaffID, encounterAssignedTo, encounterStatus, encounterNotes) values ("
  SQL = SQL & "'" & RandomID & "', "
  SQL = SQL & "'" & Request.Item("encounterMonth") & "', "
  SQL = SQL & "'" & Request.Item("encounterYear") & "', "
  SQL = SQL & "'" & Request.Item("encounterDay") & "', "
  SQL = SQL & "'" & encounterClient & "', "
  SQL = SQL & "'" & Replace(Replace(Request.Item("encounterClientID"), "'", "''"),"<br />", vbCrLf) & "', "
  SQL = SQL & "'" & Request.Item("encounterStaff") & "', "
  SQL = SQL & "'" & Request.Item("encounterStaffID") & "', "
  SQL = SQL & "'" & Request.Item("encounterAssignedTo") & "', "
  SQL = SQL & "'" & Request.Item("encounterStatus") & "', "
  SQL = SQL & "'" & Replace(Replace(Request.Item("encounterNotes"), "'", "''"),"<br />", vbCrLf) & "'"
  SQL = SQL & ");"

  set CONNECT = server.CreateObject("ADODB.Connection")
  Connect.Open DB_SCHstring
  
  response.write SQL
  ''response.end
  set mData = Connect.Execute(SQL)
end if

%>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("window.close()",1500);
</SCRIPT>
</body></html>