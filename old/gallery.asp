<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Laser Equation - Industrial Cutting Solutions</title>
<link href="includes/styles.css" rel="stylesheet" type="text/css" />
<meta name="description" content="Laser Equation Ltd. provides material cutting services for a variety of industries." />
<meta name="keywords" content="Laser cutting, precision cutting, carbon steel, stainless steel, wood, plastics, water jet, waterjet, speed cutting, clean cut edges" />
<meta name="robots" content="index, follow" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

   <link rel="stylesheet" media="screen" type="text/css" href="includes/layout.css" />
   <link rel="stylesheet" media="screen" type="text/css" href="includes/spacegallery.css" />
    <script type="text/javascript" src="includes/jquery.js"></script>
    <script type="text/javascript" src="includes/eye.js"></script>
    <script type="text/javascript" src="includes/utils.js"></script>
    <script type="text/javascript" src="includes/spacegallery.js"></script>
    <script type="text/javascript" src="includes/layout.js"></script>  
    
    <script type="text/javascript">
    // preload gallery images
    if (document.images)
{
  pic1= new Image(150,200); 
  pic1.src="images/gallery-laser1.jpg"; 

  pic2= new Image(267,200); 
  pic2.src="images/gallery-laser2.jpg"; 

  pic3= new Image(669,200); 
  pic3.src="images/gallery-laser3.jpg"; 
  
  pic4= new Image(300,200); 
  pic4.src="images/gallery-laser4.jpg"; 
  
  pic5= new Image(230,200); 
  pic5.src="images/gallery-laser5.jpg"; 
  
  pic6= new Image(253,200); 
  pic6.src="images/gallery-laser6.jpg"; 

  pic7= new Image(492,200); 
  pic7.src="images/gallery-laser7.jpg"; 

  pic8= new Image(400,160); 
  pic8.src="images/gallery-laser8.jpg"; 
  
  pic9= new Image(367,300); 
  pic9.src="images/gallery-laser9.jpg"; 
  
  pic10= new Image(308,300); 
  pic10.src="images/gallery-laser10.jpg"; 
  
  pic11= new Image(225,300); 
  pic11.src="images/gallery-laser11.jpg"; 

  pic12= new Image(225,300); 
  pic12.src="images/gallery-laser12.jpg"; 

  pic13= new Image(225,300); 
  pic13.src="images/gallery-laser13.jpg"; 
  
  pic14= new Image(600,180); 
  pic14.src="images/gallery-laser14.jpg"; 
  
  pic15= new Image(359,300); 
  pic15.src="images/gallery-laser15.jpg"; 
  
  pic16= new Image(400,300); 
  pic16.src="images/gallery-laser16.jpg"; 

  pic17= new Image(400,300); 
  pic17.src="images/gallery-laser17.jpg"; 

  pic18= new Image(400,300); 
  pic18.src="images/gallery-laser18.jpg"; 
  

}
</script>
</head>

   <body>
<div id="header">

<ul>
		<li><a href="index.asp" >Home</a></li>
		<li><a href="about-us.asp">About Us</a></li>
		<li><a href="laser-cutting.asp">Laser Cutting</a></li>
		<li><a href="water-cutting.asp">Water Cutting</a></li>
		<li><a href="#" class="active">Gallery</a></li>
		<li><a href="contact-us.asp">Contact Us</a></li>
	</ul>

<!-- <p id="layoutdims">Measure columns in: <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-pixels.htm">Pixel widths</a> | <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-ems.htm">Em widths</a> | <strong>Percentage widths</strong></p> -->

</div>

   <div class="colmask fullpage">
  
	<div class="col1">   
	    <p style="margin-left:115px; margin-top:10px; ">(click on the images below to cycle through and see them)</p>
	       
				<div id="gallery" class="spacegallery">
					<img src="images/gallery-laser1.jpg" alt="" />
					<img src="images/gallery-laser2.jpg" alt="" />
					<img src="images/gallery-laser3.jpg" alt="" />
					<img src="images/gallery-laser4.jpg" alt="" />
					<img src="images/gallery-laser5.jpg" alt="" />
					<img src="images/gallery-laser6.jpg" alt="" />
					<img src="images/gallery-laser7.jpg" alt="" />
					<img src="images/gallery-laser8.jpg" alt="" />
					<img src="images/gallery-laser9.jpg" alt="" />
					<img src="images/gallery-laser10.jpg" alt="" />
					<img src="images/gallery-laser11.jpg" alt="" />
					<img src="images/gallery-laser12.jpg" alt="" />					
					<img src="images/gallery-laser14.jpg" alt="" />
					<img src="images/gallery-laser15.jpg" alt="" />
					<img src="images/gallery-laser16.jpg" alt="" />
					<img src="images/gallery-laser17.jpg" alt="" />
					<img src="images/gallery-laser18.jpg" alt="" />
					<img src="images/gallery-laser19.jpg" alt="" />
					<img src="images/gallery-laser20.jpg" alt="" />
					<img src="images/gallery-laser21.jpg" alt="" />
					<img src="images/gallery-laser22.jpg" alt="" />					
				</div>
          </div>
          
        
          </div>
          
         
       <div id="video"><object width="309" height="250" type="application/x-shockwave-flash" data="http://www.youtube.com/v/Ww_yc5LwfIo?hl=en&amp;fs=1" ><param name="movie" value="http://www.youtube.com/v/Ww_yc5LwfIo?hl=en&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param></object></div>
<div id="footer">

<p>For a custom quote contact us at 403.250.2603 or toll free 1.888.534.1141</p>
</div>

<!--#include file="includes/logo.asp"-->
</body>
</html>
