<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Laser Equation - Industrial Cutting Solutions</title>
<link href="includes/styles.css" rel="stylesheet" type="text/css" />
<meta name="description" content="Laser Equation Ltd. provides material cutting services for a variety of industries." />
<meta name="keywords" content="Laser cutting, precision cutting, carbon steel, stainless steel, wood, plastics, water jet, waterjet, speed cutting, clean cut edges" />
<meta name="robots" content="index, follow" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>

   <body>
<div id="header">

<ul>
		<li><a href="index.asp" >Home</a></li>
		<li><a href="about-us.asp">About Us</a></li>
		<li><a href="laser-cutting.asp">Laser Cutting</a></li>
		<li><a href="water-cutting.asp" >Water Cutting</a></li>
        <li><a href="gallery.asp" >Gallery</a></li> 
		<li><a href="#" class="active">Contact Us</a></li>
	</ul>

<!-- <p id="layoutdims">Measure columns in: <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-pixels.htm">Pixel widths</a> | <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-ems.htm">Em widths</a> | <strong>Percentage widths</strong></p> -->

</div>
<div class="colmask fullpage">
	<div class="col1">
	
<p>Please feel free to call or email us, and we will be happy to engineer a quote for you.</p>

<p>Please phone<br />
fax, or email<br />
for quotes:<br />
Telephone: (403) 250-2603<br />
Toll Free: 1-888-534-1141<br />
Fax: (403) 735-5123<br />
Email:lasereq@telus.net <br />
</p>

</div>

<div id="map"><iframe width="350" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=2018+41+Avenue+Northeast,+Calgary,+Alberta&amp;sll=51.090057,-114.01094&amp;sspn=0.007871,0.026071&amp;doflg=ptk&amp;ie=UTF8&amp;hq=&amp;hnear=2018+41+Ave+NE,+Calgary,+Division+No.+6,+Alberta+T2E+8Z7&amp;t=h&amp;ll=51.090046,-114.010963&amp;spn=0.016011,0.036564&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.ca/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=2018+41+Avenue+Northeast,+Calgary,+Alberta&amp;sll=51.090057,-114.01094&amp;sspn=0.007871,0.026071&amp;doflg=ptk&amp;ie=UTF8&amp;hq=&amp;hnear=2018+41+Ave+NE,+Calgary,+Division+No.+6,+Alberta+T2E+8Z7&amp;t=h&amp;ll=51.090046,-114.010963&amp;spn=0.016011,0.036564&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small></div>
</div>
<div id="footer">

<p>For a custom quote contact us at 403.250.2603 or toll free 1.888.534.1141</p>
</div>
<!--#include file="includes/logo.asp"-->
</body>
</html>
