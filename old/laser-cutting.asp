<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Laser Equation - Industrial Cutting Solutions</title>
<link href="includes/styles.css" rel="stylesheet" type="text/css" />
<meta name="description" content="Laser Equation Ltd. provides material cutting services for a variety of industries." />
<meta name="keywords" content="Laser cutting, precision cutting, carbon steel, stainless steel, wood, plastics, water jet, waterjet, speed cutting, clean cut edges" />
<meta name="robots" content="index, follow" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>

   <body>
<div id="header">

<ul>
		<li><a href="index.asp" >Home</a></li>
		<li><a href="about-us.asp">About Us</a></li>
		<li><a href="#" class="active">Laser Cutting</a></li>
		<li><a href="water-cutting.asp">Water Cutting</a></li>
		<li><a href="gallery.asp">Gallery</a></li>
		<li><a href="contact-us.asp">Contact Us</a></li>
	</ul>

<!-- <p id="layoutdims">Measure columns in: <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-pixels.htm">Pixel widths</a> | <a href="http://matthewjamestaylor.com/blog/ultimate-1-column-full-page-ems.htm">Em widths</a> | <strong>Percentage widths</strong></p> -->

</div>
<div class="colmask fullpage">
	<div class="col1">
	
	<p>Laser Equation Ltd owns 4 lasers with the lastest being a Mitsubishi capable of cutting 1" of steel.</p>
	
	<p>Laser cutting is a precision cutting process whereby tolerances of 0.12MM (0.005") generally will be held with closer tolerances if requested. </p>

	<p>Some of the materials that have been cut include carbon steel (A-36 or 44W), stainless steel, QT-400 and QT-100, EN30B, spring steel, along with wood and plastics. </p>

	<p>Parts that have been cut include any number of machine parts, sprockets, shims, signs and logos. </p>

	<p>Thickness are from 0.002" to 0.625 for carbon steels (see waterjet for thicker material's) Stainless steel from 0.002" to 0.187 for clean cut edges or 0.500" for O2 cut edges. </p>
    
	<p>Laser Equation Ltd operates several lasers including a high speed linear drive Cincinnati 707.  Other machines are capable of cutting shapes in 2-D on tubing, electrical box cut-outs plus steel plate to 1.00 inch thick. </p>

</div>
</div>
<div id="footer">

<p>For a custom quote contact us at 403.250.2603 or toll free 1.888.534.1141</p>
</div>
<!--#include file="includes/logo.asp"-->
</body>
</html>
