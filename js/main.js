$('ul.tabs').each(function(){
  // For each set of tabs, we want to keep track of
  // which tab is active and it's associated content
  var $active, $content, $links = $(this).find('a');

  // If the location.hash matches one of the links, use that as the active tab.
  // If no match is found, use the first link as the initial active tab.
  $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
  $active.addClass('active');
  $content = $($active.attr('href'));

  // Hide the remaining content
  $links.not($active).each(function () {
    $($(this).attr('href')).hide();
  });

  // Bind the click event handler
  $(this).on('click', 'a', function(e){
    // Make the old tab inactive.
    $active.removeClass('active');
    $content.hide();

    // Update the variables with the new link and content
    $active = $(this);
    $content = $($(this).attr('href'));

    // Make the tab active.
    $active.addClass('active');
    $content.fadeIn();

    // Prevent the anchor's default click action
    e.preventDefault();
  });
});

function changePage(event) {
    if($(event.target).hasClass('external')) {
      window.open( $(event.target).attr('href'));
        return;
    }

  }
    

$(document).ready(function(){

  activateSlider ();

  activateNav ();

  $(".fancybox").fancybox();

  $("a.fbvideo").click(function() {
    $.fancybox({
      'padding'       : 0,
      'autoScale'     : false,
      'transitionIn'  : 'none',
      'transitionOut' : 'none',
      'title'         : this.title,
      'width'     : 680,
      'height'        : 495,
      'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
      'type'          : 'swf',
      'swf'           : {
        'wmode'        : 'transparent',
        'allowfullscreen'   : 'true'
      }
    });


    return false;
  });


 $('.nav li').click( changePage );


  $(window).scroll(function() {
    headerResize();
  });


  $( window ).resize(function() {
    headerResize();
  });

  headerResize();

  autoPlayYouTubeModal();

});



//FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
function autoPlayYouTubeModal() {
  var trigger = $("body").find('[data-toggle="modal"]');
  trigger.click(function () {
    var theModal = $(this).data("target"),
        videoSRC = $(this).attr("data-theVideo"),
        videoSRCauto = videoSRC + "?autoplay=1&start=16";

    if ( theModal == '#videoModal2') {
      videoSRCauto = videoSRC + "?autoplay=1&start=33";
    } else if (theModal == '#videoModal3') {
      videoSRCauto = videoSRC + "?autoplay=1";
    }

    $(theModal + ' iframe').attr('src', videoSRCauto);
    $(theModal + ' button.close').click(function () {
      $(theModal + ' iframe').attr('src', videoSRC);
    });
    $('.modal').click(function () {
      $(theModal + ' iframe').attr('src', videoSRC);
    });
  });
}




// michael to decide on classes here and add an overlay feature
$('.image').mouseover(function()  {
  $(".overlay", this).stop(true, true).fadeIn();
});

$('.image').mouseout(function()
{
  $(".overlay", this).stop(true, true).fadeOut();
});


$(document).ready(function() {
  $('form#contactForm').submit(function() {
    $('form#contactForm .error').remove();
    var hasError = false;
    $('.requiredField').each(function() {
      if($.trim($(this).val()) == '') {
        var labelText = $(this).prev('label').text();
        $(this).parent().append('<span class="error">You forgot to enter your '+labelText+'.</span>');
        $(this).addClass('inputError');
        hasError = true;
      } else if($(this).hasClass('email')) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(!emailReg.test($.trim($(this).val()))) {
          var labelText = $(this).prev('label').text();
          $(this).parent().append('<span class="error">Sorry! You\'ve entered an invalid '+labelText+'.</span>');
          $(this).addClass('inputError');
          hasError = true;
        }
      }
    });
    if(!hasError) {
      var formInput = $(this).serialize();
      $.post($(this).attr('action'),formInput, function(data){
        $('form#contactForm').slideUp("fast", function() {
          $(this).before('<p class="tick"><strong>Thank you. Your message has been delivered.</strong> </p>');
        });
      });
    }

    return false;
  });
});

function activateSlider () {

  $('.slides').fadeIn().bxSlider({
    auto: true,
    pager: false,
    nextSelector: '#slider-next',
    prevSelector: '#slider-prev',

    // triggers slider animations on slide change
    onSlideBefore: function(){
      $('.jumbotron img').addClass("fadeInSlow");
      $('.jumbotron h1').addClass("fadeInSlow");
      $('.jumbotron p').addClass("fadeInSlow");
      $('.jumbotron .button').addClass("fadeInSlow");
      $('.jumbotron .buttonSmall').addClass("fadeInSlow");
      $('#emailInputSlider').addClass("fadeInSlow");

      setTimeout (function(){
        $('.jumbotron img').removeClass("fadeInSlow");
        $('.jumbotron h1').removeClass("fadeInSlow");
        $('.jumbotron p').removeClass("fadeInSlow");
        $('.jumbotron .button').removeClass("fadeInSlow");
        $('.jumbotron .buttonSmall').removeClass("fadeInSlow");
        $('#emailInputSlider').removeClass("fadeInSlow");
      }, 1400);

      /*
      setTimeout (function(){
        $('.jumbotron img').removeClass("fadeInReallyFast");
        $('.jumbotron h1').removeClass("fadeInFast");
        $('.jumbotron p').removeClass("fadeInMed");
        $('.jumbotron .button').removeClass("fadeInSlow");
        $('.jumbotron .buttonSmall').removeClass("fadeInSlow");
        $('#emailInputSlider').removeClass("fadeInSlow");
      }, 1400);
      */
    }
  });

  //Triggers slider animations on page load
  $(document).ready(function() {
    $('.jumbotron img').toggleClass("fadeInSlow");
    $('.jumbotron h1').toggleClass("fadeInSlow");
    $('.jumbotron p').toggleClass("fadeInSlow");
    $('.jumbotron .button').toggleClass("fadeInSlow");
    $('.jumbotron .buttonSmall').toggleClass("fadeInSlow");
    $('#emailInputSlider').toggleClass("fadeInSlow");

    setTimeout (function(){
      $('.jumbotron img').removeClass("fadeInSlow");
      $('.jumbotron h1').removeClass("fadeInSlow");
      $('.jumbotron p').removeClass("fadeInSlow");
      $('.jumbotron .button').removeClass("fadeInSlow");
      $('.jumbotron .buttonSmall').removeClass("fadeInSlow");
      $('#emailInputSlider').removeClass("fadeInSlow");
    }, 7000);
  });

  //activate second bxslider (for testimonials section)
  $('.slides2').bxSlider({
    auto: true,
    controls: false
  });
}

function activateNav () {
  $('.nav.navbar-nav.navbar-right').setNav();
}



function headerResize() {


  var winWidth = $(window).width();

  var small={width: "89px",height: "30px"};
  var large={width: "267px",height: "89px"};

  if (winWidth >= 1000) {

    if ($(document).scrollTop() > 50) {

      $("header").addClass("shrink");
      $(".navbar-brand img").stop().animate( small, 500);


    } else {

      $("header").removeClass("shrink");
      $(".navbar-brand img").stop().animate( large, 500);
    }
  }
  else {

    $("header").addClass("shrink");
    $(".navbar-brand img").stop().animate( small, 500);

  }
}
