<?php

$firstname =   $_POST["firstname"];
$lastname = $_POST["lastname"];
$company = $_POST["company"];
$phone = $_POST["phone"];
$email = $_POST["email"];
$message = $_POST["message"];

$emailmessage = "Hi,<br /><br />The contact form was filled out.<br /><br />";

$emailmessage .= "Name: " . $firstname . " " . $lastname . "<br /><br />";
$emailmessage .= "Company: " . $company  . "<br /><br />";
$emailmessage .= "Phone: " . $phone  . "<br /><br />";
$emailmessage .= "Email: " . $email  . "<br /><br />";
$emailmessage .= "Message: " . $message  . "<br /><br />";


$textemailmessage = "Hi,\n\nThe contact form was filled out.\n\n";

$textemailmessage .= "Name: " . $firstname . " " . $lastname . "\n\n";
$textemailmessage .= "Company: " . $company  . "\n\n";
$textemailmessage .= "Phone: " . $phone  . "\n\n";
$textemailmessage .= "Email: " . $email  . "\n\n";
$textemailmessage .= "Message: " . $message  . "\n\n";



/**
 * This example shows making an SMTP connection with authentication.
 */

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require 'PHPMailerAutoload.php';



//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
//Set the hostname of the mail server
$mail->Host = 'mail.laserequation.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "quotes@laserequation.com";

//Password to use for SMTP authentication
$mail->Password = "laser2016";//Set who the message is to be sent from
$mail->setFrom('quotes@laserequation.com', 'Quotes @ LaserEquation');
//Set an alternative reply-to address
$mail->addReplyTo('quotes@laserequation.com', 'Laser Equation');
//Set who the message is to be sent to
$mail->addAddress('quotes@laserequation.com', 'Laser Equation');
//Set the subject line
$mail->Subject = 'Email from Contact Form on Website';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->msgHTML($emailmessage);
//Replace the plain text body with one created manually
$mail->AltBody = $textemailmessage;
//Attach an image file
// $mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
 echo "Mailer Error: " . $mail->ErrorInfo;
} else {
 echo "Message sent!";
}